---
documentclass: article
fontsize: 11pt
# lang: fr-FR
title: |
    | Techniques de compilation
    | Compilateur HEPIAL
author: Claudio Sousa, David Gonzalez
date: Juin 2018
geometry: margin=2cm
numbersections: true
header-includes: |
    \usepackage{fancyhdr}
    \usepackage{dirtree}
    \usepackage{graphicx}
    \usepackage{float}
    \usepackage{lastpage}

    \graphicspath{{./img/}}
    \pagestyle{fancy}
    \setlength{\headheight}{14pt}
    \renewcommand{\headrulewidth}{0.5pt}
    \renewcommand{\footrulewidth}{0.5pt}

    \fancyhead[LO,LE]{Techniques de compilation}
    \fancyhead[CO,CE]{Compilateur HEPIAL}
    \fancyhead[RO,RE]{Claudio Sousa, David Gonzalez}
    \fancyfoot[LO,LE]{17/05/2018}
    \fancyfoot[CO,CE]{}
    \fancyfoot[RE,RO]{\thepage /\pageref{LastPage}}

    \setcounter{tocdepth}{3}
---

\thispagestyle{empty}
\newpage

\tableofcontents
\newpage

# Introduction

Pour ce travail pratique de ce deuxième semestre en *Techniques de compilation*,
un compilateur doit être créé pour un langage maison nommé HEPIAL.

## Composants

Les composants du compilateur qui devront être implémentés sont les suivants:

* analyse lexicale;
* analyse syntaxique;
* table des symboles;
* arbre syntaxique abstrait;
* optimiseur;
* générateur de code.

L'analyse lexicale a été faite avec JFlex\footnote{\url{http://jflex.de/}} et
l'analyse syntaxique a été faite avec CUP\footnote{\url{http://www2.cs.tum.edu/projects/cup/}}.
Le code du compilateur a été implémenté dans en Java dans le programme `Hepialc`.

Le code généré est de l'assembleur Jasmin\footnote{\url{http://jasmin.sourceforge.net/}} convertible directement en byte-code exécutable par la JVM.

\newpage

## Language

HEPIAL est un langage fait maison, en français, et prend en charge les fonctionnalités suivantes:

* types de base:
    * nombre entier;
    * booléen;
    * tableau de dimension multiple avec plage d'index;
* variables;
* constantes;
* fonctions globales (déclaration et appel);
    * avec portée locale des variables;
* expressions arithmétiques;
* block de condition (`si`);
* blocks de boucle (`tantque`, `pour`);
* instructions de lecture et d'écriture sur la console;

Exemple complet d'un code HEPIAL:

~~~~ {.pascal .numberLines}
programme hepial

entier n;
entier result;

entier facto( entier n)
debutfonc
	si n == 0 alors
		retourne(1);
	sinon
		retourne(n*facto(n-1));
	finsi
finfonc

debutprg
	lire n;

	si n<15 alors
		result = facto(n);
	sinon
		ecrire " votre nombre est trop grand ! ";
	finsi
	ecrire "factorielle de ";
	ecrire n;
	ecrire " est égale à : ";
	ecrire result;
finprg
~~~~

\newpage

## Structure du code source

Voici la structure du code source:

\dirtree{%
    .1 src \dotfill racine du code source.
    .2 bin \dotfill binaires utiles pour la compilation.
    .3 jflex \dotfill permet de compiler le fichier JFlex.
    .2 lib \dotfill librairies java pour la compilation et l'exécution.
    .3 java-cup-11a.jar \dotfill librairie de compilation de CUP.
    .3 java-cup-11a-runtime.jar \dotfill librairie d'exécution de CUP.
    .3 jflex-1.6.1.jar \dotfill librairie de compilation de JFlex.
    .3 jasmin.jar \dotfill convertisseur assembleur Jasmin en bytecode.
    .2 tds \dotfill dossier des sources pour la TDS.
        .3 *.java \dotfill classes pour la table des symboles.
    .2 error\_manager \dotfill dossier des sources pour le gestionnaire d'erreur.
        .3 *.java \dotfill classes pour le gestionnaire d'erreur.
    .2 ast \dotfill dossier des sources pour l'AST.
        .3 *.java \dotfill classes pour l'arbre syntaxique abstrait.
    .2 ast\_visitors \dotfill dossier des visiteurs de l'AST.
        .3 ASTVisitor.java \dotfill interface décrivant les méthodes du visiteur de l'AST.
        .3 SourceCodeGenerator.java \dotfill générateur du code source depuis l'AST.
        .3 AnalyseurSemantique.java \dotfill vérificateur des règles sémantiques.
        .3 ConstantExpressionOptimizer.java \dotfill optimiseur des expressions constantes.
        .3 ByteCodeGenerator.java \dotfill générateur du code assembleur Jasmin.
    .2 tests \dotfill ensemble des fichiers de tests.
        .3 source\_code\_gen \dotfill tests de l'AST.
            .4 hepial* \dotfill fichiers de tests.
            .4 output*.txt \dotfill sortie attendue pour chacun des fichier de tests.
            .4 run\_tests.sh \dotfill script qui exécute et compare la sortie des tests.
        .3 source\_code\_check \dotfill tests sur l'analyseur sémantique.
            .4 hepial* \dotfill fichiers de tests.
            .4 output*.txt \dotfill sortie attendue pour chacun des fichier de tests.
            .4 run\_tests.sh \dotfill script qui exécute et compare la sortie des tests.
        .3 source\_code\_optimise \dotfill tests sur l'optimiseur.
            .4 hepial* \dotfill fichiers de tests.
            .4 output*.txt \dotfill sortie attendue pour chacun des fichier de tests.
            .4 run\_tests.sh \dotfill script qui exécute et compare la sortie des tests.
        .3 byte\_code\_gen \dotfill tests du générateur assembleur.
            .4 test\_*.input \dotfill fichiers de tests.
            .4 test\_*.bytecode \dotfill sortie jasmin attendu pour chaque test.
            .4 test\_*.output \dotfill sortie attendue pour chaque test.
            .4 test\_*.stdin \dotfill entrée nécéssaire au script de test.
            .4 run\_tests.sh \dotfill script qui exécute et compare les sorties des tests.
    .2 Makefile \dotfill commandes de compilation, testes et d'exécution.
    .2 hepial.flex \dotfill définition du lexer.
    .2 hepial.cup \dotfill définition et code du parser.
    .2 Hepialc.java \dotfill classe principale.
}

Tout fichier non-listé ci-dessus est à considérer comme temporaire.

\newpage

# Implémentation

## Analyse lexicale

Le lexer a pour but de prendre le code source en entrée et de découper le contenu
en lexèmes acceptables pour le language *HEPIAL*.

Ces lexèmes seront par la suite traités par le parser durant l'analyse syntaxique.

Le language prend en charge les lexèmes suivants:

* mot-clés:
    * programme, debutprg, finprg,
    * entier, booleen, constante,
    * debutfonc, finfonc, retourne,
    * lire, ecrire,
    * si, alors, sinon, finsi,
    * tantque, faire, fintantque,
    * pour, allantde, a, finpour,
    * vrai, faux;
* ponctuation:
    * point-virgule (;), virgule (,), deux-points (..),
    * parenthèses ('(', ')'), crochets ('[', ']');
* opérateurs:
    * assignation (=),
    * plus (+), moins (-), multiplication (*), division entière (/),
    * comparaison (==), différentation (<>),
    * inférieur (<), inférieur ou égal (<=), supérieur (>), supérieur ou égal (>=),
    * et logique (et), ou logique (ou), inversion logique (non),
    * inversion bit à bit (\textasciitilde);
* littéraux:
    * nombres, chaînes de caractères;
* identificateurs;
* commentaires.

Les 4 derniers éléments, c'est-à-dire les *nombres*,
les *chaînes de caractères*, les *identificateurs* et les *commentaires*,
sont les seuls à avoir une forme suffisamment complexe pour avoir besoin d'une expression régulière.

Un nombre `[0-9]+` est une simple suite de chiffres, de 0 à 9, au minimum un dans la suite.

Une chaîne de caractères `\"([^\"]|\"\")*\"` commence par un guillemet,
suivi de n'importe quel suite de caractères, puis se termine par un autre guillemet.
Il est possible d'insérer des guillemets en doublant le caractère `"`.

Un identificateur `[a-zA-Z][A-Za-z0-9]*` est simplement un mot commençant par une lettre,
suivi d'un nombre indéfini de lettres et/ou de chiffres.

Un commentaire `\/\/.*` commence par deux slashes `/`, continue avec une suite de caractères indéfinis et
se termine en fin de ligne.

\newpage

## Analyse syntaxique

### Règles

Le parser a pour but de prendre en entrée la liste des lexèmes du lexer et
de valider la syntaxe, c'est-à-dire qu'une séquence de lexèmes donnée correspond bien à
une phrase valide du language.

Pendant cette validation, le parser génère la table des symboles ainsi que l'arbre syntaxique abstrait,
nécessaire aux étapes suivantes de la compilation, c'est-à-dire l'analyse sémantique ainsi que la génération de code.

Le parser accepte la hiérarchie syntaxique suivante (symboles non-terminaux):

\label{ruletree}
\dirtree{%
    .1 axiome \dotfill racine syntaxique.
    .2 program \dotfill décrit un programme complet.
    .3 header \dotfill entête de programme, donnant son nom.
    .3 declar\_lst \dotfill racine commune pour la liste de déclarations divers.
    .4 declars* \dotfill liste de déclarations divers.
    .5 declar \dotfill une déclaration, de l'un des 3 types donnés.
    .6 declar\_var \dotfill déclaration de variable.
    .7 type \dotfill type de la variable.
    .8 typebase \dotfill soit un type de base (entier ou booléen).
    .8 array \dotfill soit un tableau.
    .9 -> typebase \dotfill type des éléments du tableau.
    .9 range* \dotfill défini le nombre de dimension et leur taille.
    .7 l\_ident \dotfill liste d'identificateurs.
    .6 declar\_const \dotfill déclaration d'une constante.
    .7 -> type \dotfill type de la constante.
    .7 -> expr \dotfill valeur de la constante.
    .6 declar\_funct \dotfill déclaration d'une fonction.
    .7 -> type \dotfill type de retour de la fonction.
    .7 param\_lst \dotfill racine commune pour la liste des paramètres.
    .8 param* \dotfill liste des paramètres.
    .9 -> type \dotfill type du paramètre.
    .7 -> declar\_lst \dotfill liste des variables locales de la fonction.
    .7 -> body \dotfill instructions de la fonction.
}

Note: suite sur la page suivante.

\newpage

\dirtree{%
    .1 axiome \dotfill racine syntaxique.
    .2 program \dotfill décrit un programme complet.
    .3 body \dotfill racine commune pour la liste d'instructions.
    .4 instr\_lst* \dotfill liste d'instructions.
    .5 instr \dotfill une instructions divers.
    .6 assign \dotfill une assignation de variable.
    .7 access \dotfill désigne une variable de destination.
    .8 index+ \dotfill indexation multidimensionnelle pour les tableaux.
    .9 -> expr \dotfill indice d'indexation.
    .7 expr \dotfill ensemble d'opérations resultant à une valeur.
    .8 op\_bin \dotfill opération mathématique avec deux opérandes.
    .9 -> expr \dotfill premier opérande.
    .9 -> expr \dotfill deuxième opérande.
    .8 op\_una \dotfill opération mathématique avec un opérande.
    .9 -> expr \dotfill opérande.
    .8 -> expr \dotfill expression parenthésée.
    .8 operand \dotfill représente une variable.
    .9 -> access \dotfill variable ou tableau.
    .9 func\_call \dotfill appel de fonction.
    .10 func\_param* \dotfill liste des paramètres.
    .11 -> expr \dotfill opération effective.
    .6 write\_instr \dotfill instruction d'écriture sur la console.
    .7 -> expr \dotfill chaîne de caractère à écrire.
    .6 read\_instr \dotfill instruction de lecture sur la console.
    .6 cond\_instr \dotfill bloc d'instructions conditionnel.
    .7 -> expr \dotfill condition à remplir.
    .7 -> body \dotfill instructions si vrai.
    .7 -> body? \dotfill instructions si faux.
    .6 return\_instr \dotfill instruction de retour de fonction.
    .7 -> expr \dotfill valeur de retour.
    .6 while\_instr \dotfill bloc d'instructions répétées.
    .7 -> expr \dotfill condition à remplir.
    .7 -> body \dotfill instructions à répéter.
    .6 for\_instr \dotfill bloc d'instructions répétées.
    .7 -> expr \dotfill condition de départ.
    .7 -> expr \dotfill condition d'arrivée.
    .7 -> body \dotfill instructions à répéter.
}

Légende:

* `->`: lien vers une règle déjà étendue;
* `*`: l'élément est répété 0, 1 ou n fois;
* `+`: l'élément est répété 1 ou n fois;
* `?`: l'élément est optionnel.

Pour l'explication détaillée de comment le parser génère la table des symboles ainsi que
l'arbre syntaxique abstrait, veuillez vous référer aux deux sections suivantes.

\newpage

### Table des symboles

La table des symboles est la structure de données qui permet de stocker les informations sur les variables,
c'est-à-dire leur type ainsi que leur portée.

Voici son diagramme de classes:

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=0.8\textwidth]{classes_tds}
    \end{center}
    \caption{Diagramme de classes de la table des symboles}
    \label{Diagramme de classes de la table des symboles}
\end{figure}

TDS est la classe principal. C'est un singleton global.
Cette structure de données est construite autour d'une table de hashage à deux niveau ainsi que d'une pile.

Le premier niveau de la table de hashage est simplement une *Entree*, en l'occurrence,
un simple nom de variable.
Pour chacun de ces noms de variable, le deuxième niveau est constitué d'un numéro de bloc ainsi que du *Symbole*.
Ce *Symbole* défini le type de l'entrée pour le niveau de bloc correspondant.

Le numéro de bloc défini la porté de la variable.
À chaque fois que le parser rentre dans une nouvelle fonction,
un numéro de bloc est assigné pour les variables qui sont déclarées à l'intérieur.
Le language accepte les déclarations de variables ou de constantes seulement en début de programme ou dans une fonction,
alors seules les règles *axiome* et *declar_funct* font appel à `TDS.entreeBloc()` lors
du début du traitement de la règle et `TDS.sortieBloc()` lorsque la règle a fini d'être traitée.

La fonction `TDS.entreeBloc()` incrémente le numéro de bloc courant et le met sur la pile.
Ainsi, toutes les variables déclarées dans ce bloc hériteront de ce numéro lors de leur insertion dans la table,
avec la fonction `TDS.ajouter()`.
La fonction `TDS.sortieBloc()` se contente de retirer le numéro de bloc ajouté précédemment,
retournant au bloc au dessus.

Comme dit précédemment, les déclarations ne sont possibles qu'en début de programme ou de fonction,
cela implique qu'il n'y a que 3 scénarios possibles:

* en début de programme, comme variables ou constantes locales à la fonction principale;
* en début de fonctions, comme variables ou constantes locales;
* en tant que paramètre de fonction, comme variables locales.

Ainsi, la fonction `TDS.ajouter()`, appelé au travers d'une fonction `addSymbol` du parser,
n'est utilisée que dans les règles *declar_var*, *declar_const* et *declar_funct*.

\newpage

### Arbre syntaxique abstrait

L'arbre syntaxique abstrait est un ensemble de classes qui permet de représenter sous forme d'arbre
l'entièreté d'un programme, permettant son parcours de manière simple et formel.

Le diagramme ci-dessous montre la hiérarchie des classes pour le language *HEPIAL*:

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=0.8\textwidth]{classes_ast}
    \end{center}
    \caption{Diagramme de classes de l'arbre syntaxique abstrait}
    \label{Diagramme de classes de l'arbre syntaxique abstrait}
\end{figure}

Ici, il n'y a pas de classe *principale*.
Le parser créé lui-même l'arbre durant le traitement.

En effet, si on reprend l'arbre des règles (voir: \ref{ruletree}),
le parser ne fait rien (pour l'AST) tant que celui-ci n'arrive pas à une feuille.
Lorsque c'est le cas, la classe correspondante est instanciée et retournée par la règle.
Si plusieurs feuilles sont présentes pour une règle parente,
elles seront traitées de la même manière avant la parente.
La règle parente va ensuite récupérer ces retours et
instancier sa propre classe à l'aide des retours et retourner celle-ci.
Ce processus est appliqué récursivement, au fur et à mesure que le parser descend et remonte dans l'arbre,
finissant dans la règle *axiome* avec un seul élément à la racine, qui est `DeclarationProgramme` et
contient l'arbre complet du programme traité.

Cette technique permet d'utiliser le potentiel du parser tout en évitant l'utilisation d'une pile intermédiaire,
pouvant parfois rendre le débogage difficile.

Par ailleurs, dans le diagramme d'héritage, seules les classes feuilles sont instanciées.
Les classes parentes, y compris la racine, ne servent que pour le polymorphisme.

\newpage

#### Design visiteur
\label{visitor}

L'arbre syntaxique abstrait est accompagné d'un design pattern *Visiteur*.
Cette architecture permet de créer différentes classes qui parcourent l'arbre sans
devoir modifier les classes de l'arbre en question.

Voici son diagramme de classes:

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=0.6\textwidth]{classes_visitor}
    \end{center}
    \caption{Diagramme de classes du visiteur de l'arbre syntaxique abstrait}
    \label{Diagramme de classes du visiteur de l'arbre syntaxique abstrait}
\end{figure}

Chaque nœud de l'arbre implémente une méthode nommée `accept(ASTVisitor)` avec
comme paramètre le visiteur qui souhaite parcourir l'arbre.
Le code de cette méthode est très simple: elle se contente d'appeler `visit(ASTNode*)` avec
en paramètre le nœud de l'arbre en question,
permettant au visiteur d'exécuter du code pour le type du nœud en question.

En effet, la classe *ASTVisitor* implémente une méthode `visit` par type de nœud que l'on souhaite parcourir.
C'est pour cette raison qu'un astérisque est présent à côté de `ASTNode` dans la définition de `visit`,
il indique qu'il existe une méthode par type.
En théorie, tous les types de nœud doivent être implémentés pour que cela fonctionne.

Pour qu'un parcours de l'arbre soit possible, il faut que la méthode `visit` appelle `accept`
des nœuds sous-jacents visités.

\newpage

## Analyse sémantique
\label{Analyse_semantique}

L'analyseur sémantique suit le design *visiteur* (voir: \ref{visitor}).
Il est implémenté dans une seule classe `AnalyseurSemantique` qui
implémente `visit(ASTNode*)` pour tous les types instanciables de l'arbre.

L'analyseur effectue deux tâches:

* vérifier la porté des variables;
* vérifier la compatibilité des types.

La porté des variable est vérifiée pour les nœuds de type `Idf`.
A chaque fois qu'un identifieur est rencontré,
l'analyseur va vérifier que celui-ci est dans la table des symboles.

Pour pouvoir implémenter la vérification des types,
toutes les méthodes `visit` avec un type de nœud qui héritent de `Expression` retourne
un object de type `Type` de la table des symboles.
Ainsi, les méthodes `visit` avec les types de nœud suivants retournent:

* `Appel`: type de retour de la fonction ou `null` si la fonction n'existe pas;
* `Binaire` et descendants: type de l'expression de gauche si la vérification passe, `null` autrement;
* `Chaine`: `TypeChaine`;
* `Idf`: retourne le type de la variable dans la table des symboles;
* `Indice`: type de l'identifieur avec une dimension en moins;
* `Non`: `TypeBoolean`;
* `Parentheses`: type de l'expression parenthesée;
* `Relation` et descendants: `TypeBoolean`;
* `Retour`: type de l'expression retourné;
* `Unaire` et descendants: type de l'unique opérande.

Cela permet par la suite de faire les vérifications suivantes:

* `Affectation`:
    * vérification de la compatibilité entre la source et la destination;
    * vérification que la destination ne soit pas constant;
* `Appel`:
    * vérification de la compatibilité entre l'appel et la signature de la fonction appelée;
    * vérification que l'identifieur est une fonction;
* `Binaire`: vérification de la compatibilité entre l'expression de droite et celle de gauche;
* `Condition`: vérification que l'expression de la condition retourne un booléen;
* `DeclarationConstant`: idem que `Affectation`;
* `Indice`:
    * vérification que l'expression d'indexation retourne un entier;
    * vérification que l'identifieur de l'indexation est un tableau;
* `Pour`:
    * vérification que la variable itérateur est un entier;
    * vérification que l'expression de départ retourne un entier;
    * vérification que l'expression de fin retourne un entier;
* `Tantque`: idem que `Condition`;
* `Relation`: idem que `Binaire`.

Aucun cast n'est implicite.

\newpage

## Optimiseur de code

Un simple optimiseur a été implémenté.
Son rôle est de reduire les expressions constantes.

Cet optimiseur suit le design *visiteur* (voir: \ref{visitor}).
Il est implémenté dans une seule classe `ConstantExpressionOptimizer` qui
implémente `visit(ASTNode*)` pour tous les types instanciables de l'arbre.

Une expression constante est une expression dont tous les opérandes sont constants.
Un opérande constant peut être de 3 types différents: `Nombre`, `Faux` ou `Vrai`.

Lorsqu'un nœud de type expression (ou un type descendant) est rencontré durant le parcours,
l'optimiseur détermine si celui-ci est constant:

* si c'est le cas, alors l'optimiseur crée un opérande constant pour remplacer l'expression et le retourne au nœud parent;
* sinon, `null` est retourné au nœud parent.

Si le nœud parent reçois en retour un nœud valide non-null,
alors il remplace l'expression en question par le nœud reçu en retour.

Voici la liste des nœuds et leurs expressions optimisées:

* `Affectation`: source de l'affectation;
* `Appel`: tous les paramètres;
* `Binaire`: opérande de gauche et de droite;
* `Condition`: expression de condition;
* `Ecrire`: valeur à écrire;
* `Indice`: expression d'indexation;
* `Parentheses`: expression parenthésée;
* `Pour`: expression de départ et d'arrivée;
* `Retour`: expression retournée;
* `Tantque`: expression de condition;
* `Unaire`: l'opérande.

\newpage

## Générateur de bytecode

Le générateur de bytecode suit le design *visiteur* (voir: \ref{visitor}).
Il est implémenté dans une seule classe `ByteCodeGenerator` qui
implémente `visit(ASTNode*)` pour tous les types instanciables de l'arbre.

Cette classe va convertir chaque noeud de l'AST dans du code assembleur Jasmin convertissable
directement en byte-code exécutable par la JVM.

La JVM utilise un context isolé pour chaque fonction appelée, nommé *stack frame*.
Dans chaque *stack frame*, il existe:

* des variables locales à la fonction;
* une pile (*stack*) sur laquelle sont executées les diférentes instructions.

Toutes les opérations sont appliquées sur des valeurs en haut de la pile.
La JVM a quelques particularités intéressantes: elle comprend le concept d'object ainsi que celui des fonctions,
elle offre l'accès à toute la librairie standard JAVA et est capable de gérer un nombre de variables arbitraire.

### Constantes

Les valeurs constantes utilisées dans un programmes sont copiées dans la pile d'exécution afin
d'être consommées par les opérations qui les utilisent.
L'instruction utilisée pour mettre la valeur constante dans la pile est `ldc`,
suivie de la valeur à mettre sur la pile.
Les constantes HEPIAL `vrai` et `faux` sont symbolisées dans la pile par `1` et `0` respectivement.

### Simples instructions

Bien d'instruction de notre langange HEPIAL sont de simples instructions qui s'appliquent sur 1 ou 2 valeurs.
Des exemples sont les opérateurs binaires et unaires.
Ces instructions sont converties en assembleur Jasmin en deux simples étapes:

#. on met sur la pile le ou les opérandes sur lesquels s'appliquent l'instruction.
    * si l'opérateur est binaire, alors on ajoute d'abord l'opérande de gauche et ensuite celui de droite.
#. on met sur la pile l'instruction de l'opération qui va s'appliquer sur les valeurs du haut de la pile.

Après l'exécution des étapes ci-dessus, la pile ne contient qu'un seul élément: le résultat de l'opération.

![Exemple d'opération binaire d'addition sur la pile](img/pile_example.png){width=30%}

Voici le code Jasmin correspondant:

~~~~ {.gnuassembler .numberLines}
; Code jasmin pour l'expression HEPIAL 2 + 3
ldc 3 ; met l'opérande de gauche dans la pile
ldc 2 ; met l'opérande de droite dans la pile
iadd ; applique l'addition sur les deux valeurs en haut de la pile
~~~~

Le tableau suivant donne la correspondance entre l'instruction HEPIAL et l'instruction Jasmin:

Instruction HEPIAL    | Instruction Jasmin
----------------------|--------------------
Addition              | `iadd`
Division              | `idiv`
Produit               | `imul`
Soustraction          | `isub`
Moins                 | `ineg`
Et                    | `iand`
Ou                    | `ior`
Tilda*                | `ixor`

\*Le *tilda* est un cas particulier. Bien qu'il s'agisse d'un opérateur unaire dans le language HEPIAL,
on utilise l'opérateur binaire Jasmin.
Dans ce cas, on mettra dans la pile la valeur constante `-1` avant d'ajouter l'instruction `ixor`.

L'instruction binaire `non` fut implémentée avec une méthode différente,
expliquée plus en détail dans le chapitre suivant.

Les parenthèses ne font l'object d'aucun traitement dans la génération de l'assembleur.
Leurs présences dans un code HEPIAL conditionnent la construction de l'AST,
de manière à ce que l'évaluation des expressions respecte l'ordre qu'elles imposent.

### Comparaisons binaires

Dans ce chapitre, l'implémentation des comparateurs est décrite.
Le résultat de l'exécution d'une de ces instructions laisse dans la pile la valeur booléenne `0` ou `1`.

Toutes les comparaisons se font selon le modèle de code ci-dessous.
On suppose que les valeurs à comparer se trouvent déjà dans la pile.

~~~~ {.gnuassembler .numberLines}
; Exemple de code testant l'égalité de deux valeurs en haut de la pile
if_icmpeq label_0 ; si vrai, saute vers label_0 (ligne 5)
iconst_0 ; condition fausse, met 'faux' dans la pile
goto label_1 ; saute vers la fin de la condition (ligne 7)
label_0:
iconst_1 ; condition vraie, met 'vrai' dans la pile
label_1: ; fin de la condition
~~~~

L'instruction de comparaison, ci-dessus `if_icmpeq`, va s'appliquer sur les valeurs de la pile et,
si vrai, on va sauter (`goto`) vers le label spécifié,
sinon on continue dans la prochaine ligne.
Si le saut a lieu, alors on charge sur la pile la valeur `1` (*vrai*), sinon on charge sur la pile la valeur `0` (*faux*).

Dans d'autres languages, cela correspond à l'opérateur ternaire:

~~~~ {.python}
# example en python
1 if comparaison else 0
~~~~

Les labels utilisés (dans l'exemple `label_0` et `label_1`) sont des valeurs arbitraires
données par une séquence qui assure leur unicité.
Le modèle ci-dessus pour la comparaison binaire est réutilisé pour tous les opérateurs.

Le tableau suivant donne la correspondance entre l'opérateur HEPIAL et l'instruction Jasmin:

Comparaison HEPIAL | Instruction Jasmin
-------------------|--------------------
Egal `==`          | `if_icmpeq`
Diff `!=`          | `if_icmpne`
Superieur `>`      | `if_icmpgt`
SupEgal `>=`       | `if_icmpge`
Inferieur `<`      | `if_icmplt`
InfEgal `<=`       | `if_icmple`
Non* `non`         | `ifeq`

\*Le `non` n'est pas une une instruction de comparaison mais son implémentation l'est.

### Conditions

L'instruction `si [cond] alors [instructions] sinon [instructions] finsi` a été implémentée de
manière similaire aux comparaisons du chapitre précédent.

On suppose que la condition (`[cond]`) a déjà été évaluée et
que la pile contient au sommet la valeur `1` ou `0` selon le résultat de la condition.
L'implémentation de l'instruction `si` est faite selon le modèle suivant:

~~~~ {.gnuassembler .numberLines}
; Exemple d'exécution de code conditionel
ifeq label_0 ; si la condition (valeur sur la pile) est fausse, va vers label_0
; ici liste d'instruction du le bloc 'alors'
goto label_1 ; saute vers la fin de la condition (ligne 7)
label_0:
; ici liste d'instruction du bloc 'sinon'
label_1: ; fin de la condition
~~~~

### Boucles

Les boucles implémentées utilisent des instructions `goto` pour boucler,
tout en réévaluant la condition de sortie à chaque tour.

#### Tantque

Exemple de boucle `tantque` en langage HEPIAL:

~~~~ {.pascal .numberLines}
tantque [cond] faire
    // [instructions]
fintantque
~~~~

Ce qui donne les instructions Jasmin suivantes:

~~~~ {.gnuassembler .numberLines}
label_loop:
; instructions de condition ([cond])
ifeq goto label_end ; si condition fausse (== 0) alors sortie
; corps de la boucle ([instructions])
goto label_loop ; on recommance
label_end: ; fin de la boucle
~~~~

#### Pour

La boucle `pour` à la particuliarité de maintenir une variable compteur.

Exemple de boucle `pour` en langage HEPIAL:

~~~~ {.pascal .numberLines}
pour x allantde 0 a 10 faire
    // [instructions]
finpour
~~~~

Ce qui donne les instructions Jasmin suivantes:

~~~~ {.gnuassembler .numberLines}
ldc 0 ; on met sur la pile la valeur initial du compteur
istore 0 ; qu'on assigne à la variable du compteur
label_loop: ; début de la boucle
ldc 10 ; on met sur la pile la valeur final du compteur
iload 0 ; ; ainsi que la valeur du compteur
if_icmplt label_end ; si égalité, on finit la boucle
; corps de la boucle ([instructions])
iload 0 ; on met le compteur en mémoire
ldc 1 ; ainsi que la constante 1
iadd ; et on additione le deux
istore 0 ; et on met à jour la valeur du compteur
goto label_loop ; et on recomence
label_end: ; fin de la boucle
~~~~

### Variables

#### Déclaration

Toutes les variables dans le language HEPIAL sont déclarées comme des variables privées.

Les variables déclarées au niveau du programme sont
considérées comme des variables privées de la méthode principale du programme (cf \ref{dec_programme}).

Voici un exemple de code HEPIAL:

~~~~ {.pascal .numberLines}
entier nb;
booleen b;
~~~~

Et voici le code Jasmin correspondant:

~~~~ {.gnuassembler .numberLines}
.var 0 is nb I ; déclare la variable 'nb' à l'index 0
.var 1 is b Z  ; déclare la variable 'b' à l'index 1
~~~~

Chaque déclaration d'une variable commence par l'instruction `.var`, suivie de l'index de la variable,
l'instruction `is`, le nom de la variable et le type (`I` pour des entiers et `Z` pour des booléens).

Les instructions Jasmin pour lire et écrire des variables référencent la variable par son index.
L'index des variables commence à 0 et les paramètres de la fonction prennent les premiers index.

Toutes les variables scalaires locales sont initialisées car la JVM nous empêche de lire une variable non initialisée.
Toutes les variables, entières et booléennes,
sont initialisées à *0* (*false*) sauf pour les constantes qui sont initialisées à leur valeur.

#### Lecture

Pour charger sur la pile la valeur d'une variable locale de type entier ou booléen,
nous utilisons l'instruction `iload` suivie de l'index de la variable concernée.

Pour charger des variables de référence, nous utilisons `astore`.

Exemple:

~~~~ {.gnuassembler .numberLines}
iload 0 ; met la valeur de la variable 'nb' sur la pile
~~~~

#### Affectation

Pour affecter la valeur du sommet de pile sur une variable locale,
nous utilisons l'instruction `istore` pour des entiers ou des booléens ou
`astore` pour les références, suivie de l'index de la variable concernée.

Exemple:

~~~~ {.gnuassembler .numberLines}
istore 0 ; affectation de la valeur sur la pile à la variable 'nb'
~~~~

### La déclaration de fonctions
\label{dec_fonctions}

Comme dans Java, le code exécutable du programme Jasmin doit se trouver dans des méthodes de classe.
Pour des raisons pragmatiques, le choix a été fait de n'utiliser que des fonctions statiques.

Regardons un exemple en détail:

~~~~ {.pascal .numberLines}
// fonction HEPIAL
entier carre(entier i, entier y, entier z)
	entier test;
	debutfonc
		// [instructions]
		retourne i;
    finfonc
~~~~

Donne le code Jasmin suivant:

~~~~ {.gnuassembler .numberLines}
.method public static carre(III)I
.limit stack 200
.limit locals 5
.var 3 is test I
ldc 0
istore 3
; [instructions]
iload 0
exit_label:
ireturn
.end method
~~~~

Regardons les lignes en détail:

#. Contient la signature de la fonction.
La partie initiale (`public static carre`) donne le nom et qualifiants de la fonction.
Chaque caractère entre parenthèses correspond à un paramètre de la méthode et
le dernier caractère de la ligne correpond au type de retour de la fonction.
#. Définit la taille de la pile de la fonction.
Pour des raisons pragmatiques, nous avons fixé la valeur à 20000,
mais il est probablement possible de faire une analyse des instructions assembleur de
la fonction afin d'estimer une taille maximale de pile plus précisément.
#. Spécifie le nombre de variables.
La valeur est égale à *nombre de paramètres* + *variables locales* + 1.
Dans notre exemple, nous avons 3 paramètres et 1 variable locale, ce qui donne la valeur 5.
On rajoute *+1* car on se réserve une variable temporaire interne pour certaines opérations telles que l'affectation à des tableaux.
#. Suit une liste des variables locales avec leur index.
Notre variable locale a l'index 3 car les paramètres prennent les premiers index.
Chaque déclaration de variable à un index (*3*), un nom (*test*) et leur type (*I*).
#. Première étape de l'initialisation de la variable: on met sur la pile la valeur initiale de *0*
#. La valeur de *0* dans la pile est stockée dans la variable *test* (index *3*)
#. Suit le corps de la fonction
#. La valeur de retour est chargée sur la pile
#. Ce label, rajouté manuellement, est utilisé pour sortir prématurément de la fonction car
il ne peut y avoir qu'une seule intruction "return" dans une fonction et elle doit se retrouver à la fin de la fonction
#. Instructon de *return*, de type entier (***i***return)
#. Marqueur de fin de fonction

### Déclaration de programme
\label{dec_programme}

La déclaration d'un programme prend la forme d'une classe,
où les instructions de la fonction principale HEPIAL sont mises dans une méthode statique de la classe appelée `main`.

Regardons un exemple:

~~~~ {.pascal .numberLines}
programme Program

debutprg
// [instructions]
finprg
~~~~

Donne le code Jasmin suivant:

~~~~ {.gnuassembler .numberLines}
.class public Program
.super java/lang/Object
.method public static main([Ljava/lang/String;)V
.limit stack 200
.limit locals 1
return
.end method
~~~~

La ligne 1 déclare la classe, nommée selon le nom du programme HEPIAL.
La deuxième ligne déclare la classe mère de notre classe.
Les lignes suivantes déclarent la fonction `main`,
contruite comme toutes les autres fonctions décrites dans le chapitre précédent.

### Appel de fonction

Nos fonctions HEPIAL sont toujours implémentées comme des méthodes statiques de classe.

L'appel particulier de ce type de méthode se fait à l'aide de l'instruction `invokestatic`,
suivi du nom de la classe et de la signature de la méthode appelée.

Voici l'exemple pour l'appel de la fonction exemplifié dans le chapitre \ref{dec_fonctions}.

~~~~ {.gnuassembler .numberLines}
invokestatic Program/carre(III)I
~~~~

### Tableaux

#### Déclaration

La déclaration de tableaux se fait à l'aide de l'instruction `multianewarray`.

Cette instruction attend en paramètre autant de crochets ouvrants qu'il y a de dimensions,
suivi du type (`I` pour entier, `Z` pour booléen), et du nombre de dimensions.

Des valeurs sont ensuite dépilées de la pile pour définir la taille de chaque dimension du tableau.

Voici un exemple à 1 dimension:

~~~~ {.pascal .numberLines}
entier[1..5] arr; // tableau d'entiers à une dimention de taille 5
~~~~

Le code Jasmin correspondant est le suivant:

~~~~ {.gnuassembler .numberLines}
ldc 5 ; on met sur la pile la taille du tableau
multianewarray [I 1 ; crée un tableau d'entier à 1 dimension
astore 0 ; met la référence du tableau sur la variable d'index 0
~~~~

Et voici un exemple à 2 dimensions:

~~~~ {.pascal .numberLines}
entier[3..5, 2..5] arr2; // tableau d'entiers à 2 dimentions de tailles 3 et 4
~~~~

Le code Jasmin correspondant est le suivant:

~~~~ {.gnuassembler .numberLines}
ldc 3 ; on met sur la pile la taille de la première dimension
ldc 4 ; ainsi que la taille de la deuxième dimension
multianewarray [[I 2 ; crée un tableau d'entiers à 2 dimensions
astore 1 ; met la référence du tableau sur la variable d'index 1
~~~~

A noter que le language HEPIAL supporte la définition de tableaux dont l'index ne commence pas à 0 contrairement à la JVM.
Ce décalage doit être géré à chaque accès au tableau.

#### Lecture
\label{array_read}

Pour lire une valeur dans un tableau, on doit avoir sur la pile, de bas en haut:

#. l'adresse du tableau
#. l'indice de la dimension du tableau
#. si d'autres dimensions existent:
    #. l'adresse de la dimension suivante avec `aaload`
    #. on recommence à l'étape 2

Ensuite nous utilisons l'intruction `iaload` pour faire l'affectation.

Voici un exemple à 1 dimension:

~~~~ {.pascal .numberLines}
a = arr[2] // on lit la valeur tableau pour assigner à une variable
~~~~

Le code Jasmin correspondant est le suivant:

~~~~ {.gnuassembler .numberLines}
aload 0 ; on met sur la pile l'adresse du tableau
ldc 2 ;  on met sur la pile l'index spécifié dans le code
ldc 1 ;  ainsi que l'index de début de la dimension
isub ; on soustrait pour calculer l'index à 0
iaload ; et on met sur la pile la valeur du tableau à la position spécifiée

~~~~

Et voici un exemple à 2 dimensions:

~~~~ {.pascal .numberLines}
a = arr2[4][5] //
~~~~

Le code Jasmin correspondant est le suivant:

~~~~ {.gnuassembler .numberLines}
aload 1 ; on met sur la pile l'adresse du tableau
ldc 4 ;  on met sur la pile l'index spécifié dans le code
ldc 3 ;  ainsi que l'index de début de la dimension
isub ; on soustrait pour calculer l'index à 0
aaload ; on charge sur la pile l'adresse de la prochain dimension
ldc 5 ; on met sur la pile l'index spécifié dans le code
ldc 2 ; ainsi que l'index de début de la dimension
isub ; on soustrait pour calculer l'index à 0
iaload ; et on met sur la pile la valeur du tableau à la position spécifiée
~~~~

#### Affectation

Pour affecter une valeur dans un tableau,
la pile doit contenir les mêmes éléments que durant la lecture (voir section \ref{array_read}),
avec un élément supplémentaire qui est la valeur à affecter.

Ensuite nous utilisons l'intruction `iastore` pour faire l'affectation.

Le méchanisme générique de parcours de l'AST met la valeur à assigner d'abord sur la pile,
ce qui est contraire à l'ordre des valeurs de la pile attendus enumérées ci-dessus.
Pour corriger ceci, nous utilisons une variable temporaire locale afin de
remmetre la valeur d'assignation dans la pile au bon moment.

Voici un exemple à 1 dimension:

~~~~ {.pascal .numberLines}
arr[1] = 2;
~~~~

Le code Jasmin correspondant est le suivant:

~~~~ {.gnuassembler .numberLines}
ldc 2 ; valeur d'assignation est mise dans la pile
istore 2 ; on assigne cette valeur dans une variable temporaire
aload 0 ; on met sur la pile l'adresse du tableau
ldc 1 ; on met sur la pile l'index spécifié dans le code
ldc 1 ; ainsi que l'index de début de la dimension
isub ; on soustrait pour  calculer l'index à 0
iload 2; on remet sur la pile la valeur de la variable temporaire
iastore ; et on l'affecte au tableau
~~~~

Et voici un exemple à 2 dimensions:

~~~~ {.pascal .numberLines}
arr2[4][5] = 4;
~~~~

Le code Jasmin correspondant est le suivant:

~~~~ {.gnuassembler .numberLines}
ldc 4 ; valeur d'assignation est mise dans la pile
istore 2 ; on assigne cette valeur dans une variable temporaire
aload 1 ; on met sur la pile l'adresse du tableau
ldc 4 ; on met sur la pile l'index spécifié dans le code
ldc 3 ; ainsi que l'index de début de la dimension
isub ; on soustrait pour calculer l'index à 0
aaload ; on charge sur la pile l'adresse de la prochain dimension
ldc 5 ; on met sur la pile l'index spécifié dans le code
ldc 2 ; ainsi que l'index de début de la dimension
isub ; on soustrait pour calculer l'index à 0
iload 2; on remet sur la pile la valeur de la variable temporaire
iastore ; et on l'affecte au tableau
~~~~

### Appels à la libraire standard

Une des avantages de la JVM est de pouvoir accéder à toutes les fonctionnalités de la librairie standard.
Nous utilisons ces fonctionalités dans l'implémentation des instructions `ecrire` et `lire`, décrites ci-dessous.

#### Ecrire

Pour `ecrire`, nous utilisons les méthodes java suivantes:

~~~~{.java .numberLines}
System.out.println(Integer):void // si l'on veut écrire un numéro
System.out.println(String):void  // si l'on veut écrire une chaine
~~~~

*Pour les booléens, on écrit les chaines `vrai` ou `faux`.*

Voici le modèle du code Jasmin pour l'instruction `ecrire` dans le cas on l'on désire écrire une chaine de caractères:

~~~~ {.gnuassembler .numberLines}
; Ecriture d'une chaine sur la console
getstatic java/lang/System/out Ljava/io/PrintStream
ldc "test" ; met sur la pile la chaine d'exemple "test"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
~~~~

L'instruction `getstatic`, à la ligne 2,
met sur la pile la propriété statique `System.out` qui est de type `java.io.PrintStream`.

L'instruction `invokevirtual`, à la ligne 4,
va exécuter la fonction `println`, de la classe `java.io.PrintStream`,
qui accepte en paramètre une chaine de caractères.
En haut de la pile se trouve le paramètre et en dessus l'instance de `java.io.PrintStream` sur laquelle appeler la fonction.

#### Lire

Pour la lecture, nous utilisons la classe `java.util.Scanner`, selon l'exemple en java:

~~~~{.java .numberLines}
java.util.Scanner scanner = new java.util.Scanner(System.in);
var nb = scanner.nextInt()
~~~~

En assembleur Jasmin, nous avons:

~~~~ {.gnuassembler .numberLines}
new java/util/Scanner ; instanciation de java.util.Scanner
dup ; duplication du pointeur de l'instance sur la pile
getstatic java/lang/System/in Ljava/io/InputStream; on met System.in dans la pile
invokespecial java/util/Scanner/<init>(Ljava/io/InputStream;)V ;appel constructor
invokevirtual java/util/Scanner/nextInt()I ; appel de nextInt()
istore 0
~~~~

Pour chaque ligne:

#. Nous créons une nouvelle instance de java.util.Scanner, et la référence est mise dans la pile.
#. On duplique la référence dans la pile avec `dup`.
Elle sera utilisée une fois pour le contructeur et la deuxième pour l'appel de la fonction `nextInt`.
#. On recupère la propriété statique `System.in`, de type `java.io.InputStream`, que nous mettons dans la pile.
#. On appelle le contructeur (`<init>`) de l'instance du Scanner avec l'input stream en paramètre.
#. On appelle la fonction `nextInt()` du Scanner qui retournera un entier, assigné à une variable local en ligne 6.

\newpage

## Gestionnaire d'erreur

Le gestionnaire d'erreur est un singleton global qui permet de stocker les messages d'erreurs.

Voici son diagramme de classes:

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=0.5\textwidth]{classes_errormanager}
    \end{center}
    \caption{Diagramme de classes du gestionnaire d'erreur}
    \label{Diagramme de classes du gestionnaire d'erreur}
\end{figure}

Ces messages d'erreurs ont deux sources:

* principalement de l'analyseur sémantique;
* de la table des symboles (où une erreur est directement lancée).

Pour que l'analyseur sémantique puisse reporter les erreurs correctement,
tous les nœud de l'arbre syntaxique abstrait possèdent trois champs:

* nom du fichier source;
* numéro de ligne du lexème dans le fichier source;
* numéro de colonne du lexème dans le fichier source;

Ces champs sont remplis lors de l'instanciation dans CUP.
Évidemment, JFlex doit être configuré pour reporter la ligne et la colonne lors de la construction du lexème.

Pour ce qui est de la table des symboles,
les trois champs sont donnés en paramètre lors de l'ajout d'un nouveau symbole dans la table.

\newpage

## Programme principal
\label{main}

Le programme principal est implémenté dans la classe `Hepialc`.
Il a pour rôle de mettre ensemble tous les composants développés et de permettre d'exécuter les tests.

Son usage est le suivant: `java Hepialc $mode $sourcefile [nooptimise]`

* `mode`: sortie souhaitée, pour les tests;
* `sourcefile`: fichier source, dans le language HEPIAL;
* `nooptimise`: permet de spécifier si l'AST ne devrait pas être optimisé.

3 modes sont supportés:

* `gensourcecode`: permet de regénérer les sources dans le language HEPIAL à partir de l'arbre syntaxique abstrait, pour les tests;
* `checksourcecode`: permet de tester l'analyseur sémantique;
* `genbytecode`: permet de générer le bytecode pour la JVM.

Le programme principal effectue la compilation de la manière suivante:

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=0.5\textwidth]{main_flow}
    \end{center}
    \caption{Diagramme de séquence du programme principal}
    \label{Diagramme de séquence du programme principal}
\end{figure}

\newpage

## Tests

L'ensemble des tests se trouve dans le dossier `src/tests` et il y en a de 4 types,
chacun dans un sous-dossier différent:

* test de l'arbre syntaxique abstrait, dans le dossier `source_code_gen`;
* test de l'analyseur sémantique, dans le dossier `source_code_check`;
* test de l'optimiseur, dans le dossier `source_code_optimise`;
* test du générateur de bytecode, dans le dossier `byte_code_gen`.

Chacun de ces tests exécute le programme principal dans le mode correspondant:

* `source_code_gen`: `gensourcecode`;
* `source_code_check`: `checksourcecode`;
* `source_code_optimise`: `gensourcecode`;
* `byte_code_gen`: `genbytecode`.

L'architecture d'exécution des tests est le même pour les 4 types de tests.

Le dossier comporte 3 types de fichiers:

* `hepial*`: fichier source de tests, numéroté;
* `output*.txt`: sortie attendue pour le mode souhaité, numéroté;
* `run_tests.sh`: script d'exécution des tests.

Pour chaque fichier source, le script lance la compilation dans le mode souhaité et
compare la sortie de la compilation avec le fichier `output*.txt` correspondant.
Si la sortie correspond à celle attendue,
alors le test passe et un `OK` en vert est affiché pour le test,
sinon un `Failure` en rouge est affiché.

Tous les tests, excepté celui de l'optimiseur, utilisent l'option `nooptimise`.

Pour les détails d'implémentation des différents tests, référez-vous aux sections suivantes.

\newpage

### Arbre syntaxique abstrait

Comme pour l'analyseur sémantique,
le générateur de code source suit le design *visiteur* (voir: \ref{visitor}).
Elle est implémentée dans une seule classe `SourceCodeGenerator` qui
implémente `visit(ASTNode*)` pour tous les types instanciables de l'arbre.

Pour chacun des nœuds, le générateur va construire une chaine de caractères dans le language HEPIAL.
Lorsque l'arbre a été parcouru et la chaine complète, celle-ci est simplement affiché.

Ceci sert à vérifier que l'arbre syntaxique abstrait généré correspond bien au source analysé.

### Analyseur sémantique

L'implémentation de ce test ne requière pas de code supplémentaire puisque
le programme principal effectue l'analyse sémantique dans tous les cas.

### Optimiseur

L'implémentation de ce test ne requière pas de code supplémentaire car
il utilise le code correspondant aux tests de l'arbre syntaxique abstrait,
mais en ne passant pas l'option `nooptimise`.

### Générateur de bytecode

L'architecture d'exécution des tests diffère un peu mais reste essentiellement le même.
Lorsque le bytecode a été généré, celui-ci est affiché dans la sortie console.
Le script de test récupère cette sortie et effectue deux actions:

* compare le bytecode généré avec le fichier `bytecode*.txt` correspondant;
* compile le bytecode généré avec Jasmin, exécute le programme resultant et
compare la sortie avec le fichier `output*.txt` correspondant.

Il est aussi possible de specifier le contenu de l'entrée standard à passer au programme.
Ceci est crucial pour tester les programmes faisant usage de l'instruction `lire`.

\newpage

### Sortie des tests

La commande `make tests` affiche le résultat de l'exécution des tests:

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=0.58\textwidth]{tests_output}
    \end{center}
    \caption{Sortie de la totalité des tests}
    \label{Sortie de la totalité des tests}
\end{figure}

\newpage

## Usage

L'usage du projet se fait entièrement par l'intermédiaire du `Makefile`
dans le dossier `src`.

Ce `Makefile` prend en charge les commandes suivantes:

* `build`: compile les sources;
* `tests`: lance tous les tests;
* `tests_ast`: lance les tests de l'arbre syntaxique abstrait;
* `tests_semantique`: lance les tests de l'analyseur sémantique;
* `tests_optimiser`: lance les tests de l'optimiseur;
* `tests_bytecode`: lance les tests du générateur de bytecode;
* `clean`: supprime tous les fichiers temporaires générés durant la compilation;
* `compile`: permet de compiler un fichier HEPIAL choisi;
* `run`: permet de compiler et exécuter un fichier HEPIAL choisi.

Pour compiler le code et exécuter les tests, lancez simplement `make`.

Pour compiler un fichier HEPIAL de votre choix, utilisez `make compile i=$sourcefile`.

Pour compiler et exécuter un fichier HEPIAL de votre choix, utilisez `make run i=$sourcefile`.

\newpage

### Exécution de programe HEPIAL

Ici un exemple d'exécution du fichier Factorial.hepial. Voici le contenu du fichier:

~~~~ {.pascal .numberLines}
programme Factorial

entier n;
entier result;

entier facto(entier x)
debutfonc
	si x == 0 alors
		retourne(1);
	sinon
		retourne(x*facto(x-1));
	finsi
finfonc

debutprg
    ecrire "Entrez un numéro entre 1 et 12";
    lire n;
	si n<13 alors
		result = facto(n);
	sinon
		ecrire "Votre nombre est trop grand !";
	finsi
	ecrire "La factorielle est égale à :";
	ecrire result;
finprg
~~~~

Et le résultat de l'exécution:

~~~~ {.bash}

$ make run i=tests/Factorial.hepial
Entrez un numéro entre 1 et 12
5
La factorielle est égale à :
120

~~~~

\newpage

# Conclusion

Ce projet a été l'opportunité d'appliquer et valider la théorie apprise pendant
les deux semestres du cours de Techniques de compilation.
Nous avons bien compris les étapes clés:

* Analyse lexicale
* Analyse syntaxique
    * ainsi la contruction de l'AST
* Analyse sémantique
* Optimization de l'AST
* Génération de l'assembleur pour la JVM

Le résultat est un language avec des constructions riches tels que les tableaux, des fonctions.
Il aurait été intéressant de faire un language différent, utilisant des paradigmes moins communs,
peut-être un language plus fonctionnel.

Il a été enrichissant de mieux comprendre comment la JVM fonctionne,
qui offre des avantages comparativement à d'autres languages assembleur:

* multi-platforme
* concept de classes
* concept de fonctions
* gestion automatique de la mémoire

Des améliorations pourraient encore être faites au niveau de l'implémentation:

* l'analyse de la taille de la pile afin d'éviter une constante
* étendre l'optimiseur afin de gérer des cas supplémentaires

C'est un projet intéressant, mais demande un effort important.
