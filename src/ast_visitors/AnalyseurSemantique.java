/*
 * Semantic analyser that walks the AST.
 * Check for scope and type compatibility.
 *
 * @author Claudio Sousa, David Gonzalez
 */

import java.util.*;

public class AnalyseurSemantique implements ASTVisitor {

    /**
     * Check that two types matche and add error if not
     * @param type1 First type to check
     * @param type2 Second type to check
     * @param node The node for which the check is done
     * @param message Error message to add in case of mismatch
     */
    private boolean checkType(Type type1, Type type2, ASTNode node, String message) {
        boolean ret = (type1 != null) && (type2 != null) && (!type1.estConforme(type2));
        if (ret) {
            ErrorManager.getInstance().add(message, node.getFilename(), node.getLine(), node.getColumn());
        }
        return ret;
    }

    public Object visit(Addition node) { return this.visit((Binaire)node); }

    /**
     * Check assignation type compatibility
     */
    public Object visit(Affectation node) {
        Type dest_type = (Type)node.getDestination().accept(this);
        Type source_type = (Type)node.getSource().accept(this);
        this.checkType(dest_type, source_type, node,
                       "assignation incompatible de '" + source_type + "' à '" + dest_type + "'");

        // Search for the identifier at the left expression
        Expression identifier_node = node.getDestination();
        do {
            if (identifier_node instanceof Indice) {
                identifier_node = ((Indice)identifier_node).getIdentifier();
            }
        } while (!(identifier_node instanceof Idf));
        // Error if it is constant
        String identifier_name = ((Idf)identifier_node).getNom();
        Symbole idf_sym = TDS.getInstance().identifier(new Entree(identifier_name));
        if ((idf_sym != null) && idf_sym.isConst()) {
            ErrorManager.getInstance().add(
                "assignation invalide, '" + identifier_name + "' est déclaré constant",
                node.getFilename(), node.getLine(), node.getColumn()
            );
        }

        return null;
    }

    /**
     * Check parameter length and types
     * by recreating a function type for the call and compare with the type from the TDS.
     * Also check that the identifier is a function.
     * @return function return type
     */
    public Object visit(Appel node) {
        TypeFonction func_type = null;
        Type var_type = (Type)node.getNomFonction().accept(this);

        if (var_type != null) {
            if (var_type instanceof TypeFonction) {
                func_type = (TypeFonction)var_type;
                Type return_type = (func_type != null) ? func_type.getReturnType() : null;
                TypeFonction call_type = new TypeFonction(return_type);

                ArrayList<Expression> params = node.getParameters();
                for (int i = 0; i < params.size(); ++i) {
                    call_type.ajoutArgument((Type)params.get(i).accept(this));
                }

                this.checkType(func_type, call_type, node,
                               "appel incompatible '" + call_type + "' pour la fonction '" +
                                   node.getNomFonction().getNom() + "' avec signature '" + func_type + "'");
            }
            else {
                ErrorManager.getInstance().add(
                    "appel invalide, '" + node.getNomFonction().getNom() + "' n'est pas une fonction",
                    node.getFilename(), node.getLine(), node.getColumn()
                );
            }
        }

        return (func_type != null) ? func_type.getReturnType() : null;
    }

    /**
     * Check type compatibility for all binary expression
     * @return Type of expression
     */
    public Object visit(Binaire node) {
        Type t1 = (Type)node.getGauche().accept(this);
        Type t2 = (Type)node.getDroite().accept(this);
        boolean no_compatible = this.checkType(t1, t2, node,
                                               "operandes incompatible '" + t1 + "' et '" + t2 +
                                                   "' pour l'opération '" + node.operateur() + "'");

        return (no_compatible) ? null : t1;
    }

    public Object visit(Bloc node) {
        for (Instruction inst: node.getInstructions()){
            inst.accept(this);
        }
        return null;
    }

    public Object visit(Chaine node) { return new TypeChaine(); }

    /**
     * Check condition expression is a boolean
     */
    public Object visit(Condition node) {
        Type cond_type = (Type)node.getCondition().accept(this);
        this.checkType(cond_type, new TypeBooleen(), node,
                       "l'expression conditionnelle doit résulter à un booléen, non pas à un '" + cond_type + "'");

        node.getThenInstruction().accept(this);
        if (node.hasElse()){
            node.getElseInstruction().accept(this);
        }
        return null;
    }

    /**
     * Check that the expression result to the identifier type
     */
    public Object visit(DeclarationConstant node) {
        Type id_type = (Type)node.getIdentifier().accept(this);
        Type expr_type = (Type)node.getConstantExpression().accept(this);
        this.checkType(id_type, expr_type, node,
                       "assignation constante incompatible de '" + expr_type + "' à '" + id_type + "'");

        return null;
    }

    public Object visit(DeclarationFonction node) {
        TDS.getInstance().entreeBloc();

        node.getIdentifier().accept(this);
        ArrayList<Instruction> parameters = node.getParameters().getInstructions();
        for (int i = 0; i < parameters.size(); ++i) {
            parameters.get(i).accept(this);
        }
        node.getDeclaration().accept(this);
        node.getInstructions().accept(this);

        TDS.getInstance().sortieBloc();
        return null;
    }

    public Object visit(DeclarationProgramme node) {
        TDS.getInstance().resetBlocNumber();
        TDS.getInstance().entreeBloc();
        //node.getIdentifier().accept(this); // Do not check programme identifier
        node.getDeclaration().accept(this);
        node.getInstructions().accept(this);
        TDS.getInstance().sortieBloc();
        return null;
    }

    public Object visit(DeclarationVariable node) {
        node.getIdentifier().accept(this);
        return null;
    }

    public Object visit(Diff node) { return this.visit((Relation)node); }

    public Object visit(Division node) { return this.visit((Binaire)node); }

    public Object visit(Ecrire node) {
        node.getSource().accept(this);
        return null;
    }

    public Object visit(Egal node) { return this.visit((Relation)node); }

    public Object visit(Et node) {
        this.visit((Binaire)node);
        return new TypeBooleen();
    }

    public Object visit(Faux node) { return new TypeBooleen(); }

    /**
     * Check identifier scope
     * @return Variable type
     */
    public Object visit(Idf node) {
        Entree variable = new Entree(node.getNom());
        Symbole var_sym = TDS.getInstance().identifier(variable);
        if (var_sym == null) {
            ErrorManager.getInstance().add(
                "identifiant '" + variable.getName() + "' introuvable",
                node.getFilename(), node.getLine(), node.getColumn()
            );
        }

        return (var_sym != null) ? var_sym.getType() : null;
    }

    /**
     * Check that the type of the indexing expression is always a number.
     * Also check that an index is done on an array.
     * @return array type without the indexed dimension
     */
    public Object visit(Indice node) {
        Type id_type = (Type)node.getIdentifier().accept(this);

        this.checkType((Type)node.getIndex().accept(this), new TypeEntier(), node,
                       "l'expression d'indexation doit résulter à un type integral");

        if (!(id_type instanceof TypeTableau)) {
            ErrorManager.getInstance().add(
                "indexation sur un type non-tableau '" + id_type + "'",
                node.getFilename(), node.getLine(), node.getColumn()
            );
        }

        return (id_type instanceof TypeTableau) ? ((TypeTableau)id_type).getSameWithoutFirstDimension() : id_type;
    }

    public Object visit(InfEgal node) { return this.visit((Relation)node); }

    public Object visit(Inferieur node) { return this.visit((Relation)node); }

    public Object visit(Lire node) {
        node.getDestination().accept(this);
        return null;
    }

    public Object visit(Moins node) { return this.visit((Unaire)node); }

    public Object visit(Nombre node) { return new TypeEntier(); }

    /**
     * @return Always return bool
     */
    public Object visit(Non node) {
        this.visit((Unaire)node);
        return new TypeBooleen();
    }

    public Object visit(Ou node) {
        this.visit((Binaire)node);
        return new TypeBooleen();
    }

    public Object visit(Parentheses node) { return node.getExpression().accept(this); }

    /**
     * Check that all parts of the loop declaration are of the right type
     */
    public Object visit(Pour node) {
        Type it_type = (Type)node.getIteratorName().accept(this);
        this.checkType(it_type, new TypeEntier(), node,
                       "la variable iterateur doit être un entier, non pas un '" + it_type + "'");

        Type from_type = (Type)node.getFrom().accept(this);
        this.checkType(from_type, new TypeEntier(), node,
                       "l'expression de départ doit résulter à un entier, non pas à un '" + from_type + "'");

        Type to_type = (Type)node.getTo().accept(this);
        this.checkType(to_type, new TypeEntier(), node,
                       "l'expression de fin doit résulter à un entier, non pas à un '" + to_type + "'");

        node.getInstruction().accept(this);
        return null;
    }

    public Object visit(Produit node) { return this.visit((Binaire)node); }

    /**
     * Check type compatibility for relation expression
     * @return Always return bool
     */
    public Object visit(Relation node) {
        this.visit((Binaire)node);
        return new TypeBooleen();
    }

    public Object visit(Retour node) { return node.getSource().accept(this); }

    public Object visit(Soustraction node) { return this.visit((Binaire)node); }

    public Object visit(SupEgal node) { return this.visit((Relation)node); }

    public Object visit(Superieur node) { return this.visit((Relation)node); }

    /**
     * Check condition expression is a boolean
     */
    public Object visit(Tantque node) {
        Type cond_type = (Type)node.getCondition().accept(this);
        this.checkType(cond_type, new TypeBooleen(), node,
                       "l'expression conditionnelle doit résulter à un booléen, non pas à un '" + cond_type + "'");

        node.getInstruction().accept(this);
        return null;
    }

    public Object visit(Tilda node) { return this.visit((Unaire)node); }

    /**
     * Return the operand type
     */
    public Object visit(Unaire node) { return node.getOperand().accept(this); }

    public Object visit(Vrai node) { return new TypeBooleen(); }
}
