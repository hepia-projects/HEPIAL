/*
 * Optimzes AST by pre-calculating constant expressions
 *
 * @author Claudio Sousa, David Gonzalez
 */

import java.util.*;

public class ConstantExpressionOptimizer implements ASTVisitor{

    /**
     * Determine if the given node is a constant node
     * @return TRUE if constant, FALSE otheriwse
     */
    public boolean isConstant(Expression node) {
        return (node instanceof Nombre) || (node instanceof Faux) || (node instanceof Vrai);
    }

    /**
     * Determine if the expression is composed of constant expressions
     * @param node Node to test
     * @return TRUE if the expression is constant, FALSE otherwise
     */
    public boolean isConstantExp(Expression node) {
        boolean constant = false;

        if (node instanceof Binaire) {
            Binaire nodeB = (Binaire)node;
            constant = this.isConstant(nodeB.getGauche()) && this.isConstant(nodeB.getDroite());
        }
        else if (node instanceof Unaire) {
            Unaire nodeU = (Unaire)node;
            constant = this.isConstant(nodeU.getOperand());
        }

        return constant;
    }

    /**
     * Create an integer constant expression node from the given value
     * @param node
     * @param val
     * @return Constant expression node created
     */
    public Expression createConstantNode(Expression node, int val) {
        return new Nombre(val, node.getFilename(), node.getLine(), node.getColumn());
    }
    /**
     * Create a boolean constant expression node from the given value
     * @param node
     * @param val
     * @return Constant expression node created
     */
    public Expression createConstantNode(Expression node, boolean val) {
        return (val) ?
            new Vrai(node.getFilename(), node.getLine(), node.getColumn()) :
            new Faux(node.getFilename(), node.getLine(), node.getColumn());
    }

    public Object visit(Addition node) { return this.visit((Binaire)node); }

    public Object visit(Affectation node) {
        node.getDestination().accept(this);
        Expression nodeP = (Expression)node.getSource().accept(this);
        if (nodeP != null) {
            node.grefferSource(nodeP);
        }
        return null;
    }

    public Object visit(Appel node) {
        node.getNomFonction().accept(this);
        for (int i = 0; i < node.getParameterLength(); ++i) {
            Expression nodeP = (Expression)node.getParameter(i).accept(this);
            if (nodeP != null) {
                node.setParameter(i, nodeP);
            }
        }
        return null;
    }

    public Object visit(Binaire node) {
        Expression nodeGP = (Expression)node.getGauche().accept(this);
        if (nodeGP != null) {
            node.grefferGauche(nodeGP);
        }
        Expression nodeDP = (Expression)node.getDroite().accept(this);
        if (nodeDP != null) {
            node.grefferDroit(nodeDP);
        }

        Expression ret = null;
        if (this.isConstantExp(node)) {
            Expression operandNodeG = node.getGauche();
            Expression operandNodeD = node.getDroite();
            if ((operandNodeG instanceof Nombre) && (operandNodeD instanceof Nombre)) {
                int val = node.apply(((Nombre)operandNodeG).getValeur(), ((Nombre)operandNodeD).getValeur());

                if ((node instanceof Relation) || (node instanceof Et) || (node instanceof Ou)) {
                    ret = this.createConstantNode(node, val != 0);
                }
                else {
                    ret = this.createConstantNode(node, val);
                }
            }
            else if (
                ((operandNodeG instanceof Faux) || (operandNodeG instanceof Vrai)) &&
                ((operandNodeD instanceof Faux) || (operandNodeD instanceof Vrai))
            ) {
                ret = this.createConstantNode(
                    node,
                    node.apply(operandNodeG instanceof Vrai, operandNodeD instanceof Vrai)
                );
            }
        }

        return ret;
    }

    public Object visit(Bloc node) {
        for (Instruction inst: node.getInstructions()){
            inst.accept(this);
        }
        return null;
    }

    public Object visit(Chaine node) { return null; }

    public Object visit(Condition node) {
        Expression nodeP = (Expression)node.getCondition().accept(this);
        if (nodeP != null) {
            node.grefferCondition(nodeP);
        }

        node.getThenInstruction().accept(this);
        if (node.hasElse()){
            node.getElseInstruction().accept(this);
        }
        return null;
    }

    public Object visit(DeclarationConstant node) {
        node.getIdentifier().accept(this);
        node.getConstantExpression().accept(this);
        return null;
    }

    public Object visit(DeclarationFonction node) {
        node.getIdentifier().accept(this);
        ArrayList<Instruction> parameters = node.getParameters().getInstructions();
        for (int i = 0; i < parameters.size(); ++i) {
            parameters.get(i).accept(this);
        }
        node.getDeclaration().accept(this);
        node.getInstructions().accept(this);
        return null;
    }

    public Object visit(DeclarationProgramme node) {
        node.getIdentifier().accept(this);
        node.getDeclaration().accept(this);
        node.getInstructions().accept(this);
        return null;
    }

    public Object visit(DeclarationVariable node) {
        node.getIdentifier().accept(this);
        return null;
    }

    public Object visit(Diff node) { return this.visit((Binaire)node); }

    public Object visit(Division node) { return this.visit((Binaire)node); }

    public Object visit(Ecrire node) {
        Expression nodeP = (Expression)node.getSource().accept(this);
        if (nodeP != null) {
            node.setSource(nodeP);
        }
        return null;
    }

    public Object visit(Egal node) { return this.visit((Binaire)node); }

    public Object visit(Et node) { return this.visit((Binaire)node); }

    public Object visit(Faux node) { return null; }

    public Object visit(Idf node) { return null; }

    public Object visit(Indice node) {
        node.getIdentifier().accept(this);
        Expression nodeP = (Expression)node.getIndex().accept(this);
        if (nodeP != null) {
            node.setIndex(nodeP);
        }
        return null;
    }

    public Object visit(InfEgal node) { return this.visit((Binaire)node); }

    public Object visit(Inferieur node) { return this.visit((Binaire)node); }

    public Object visit(Lire node) {
        node.getDestination().accept(this);
        return null;
    }

    public Object visit(Moins node) { return this.visit((Unaire)node); }

    public Object visit(Nombre node) { return null; }

    public Object visit(Non node) { return this.visit((Unaire)node); }

    public Object visit(Ou node) { return this.visit((Binaire)node); }

    public Object visit(Parentheses node) {
        Expression nodeP = (Expression)node.getExpression().accept(this);
        if (nodeP != null) {
            node.grefferExpression(nodeP);
        }

        return (this.isConstant(nodeP)) ? nodeP : null;
    }

    public Object visit(Pour node) {
        node.getIteratorName().accept(this);

        Expression nodeP = (Expression)node.getFrom().accept(this);
        if (nodeP != null) {
            node.grefferFrom(nodeP);
        }

        nodeP = (Expression)node.getTo().accept(this);
        if (nodeP != null) {
            node.grefferTo(nodeP);
        }

        node.getInstruction().accept(this);
        return null;
    }

    public Object visit(Produit node) { return this.visit((Binaire)node); }

    public Object visit(Retour node) {
        Expression nodeP = (Expression)node.getSource().accept(this);
        if (nodeP != null) {
            node.grefferSource(nodeP);
        }
        return null;
    }

    public Object visit(Soustraction node) { return this.visit((Binaire)node); }

    public Object visit(SupEgal node) { return this.visit((Binaire)node); }

    public Object visit(Superieur node) { return this.visit((Binaire)node); }

    public Object visit(Tantque node) {
        Expression nodeP = (Expression)node.getCondition().accept(this);
        if (nodeP != null) {
            node.grefferCondition(nodeP);
        }

        node.getInstruction().accept(this);
        return null;
    }

    public Object visit(Tilda node) { return this.visit((Unaire)node); }

    public Object visit(Unaire node) {
        Expression nodeP = (Expression)node.getOperand().accept(this);
        if (nodeP != null) {
            node.grefferOperand(nodeP);
        }

        Expression ret = null;
        if (this.isConstantExp(node)) {
            Expression operandNode = node.getOperand();
            if (operandNode instanceof Nombre) {
                int val = node.apply(((Nombre)operandNode).getValeur());
                if (node instanceof Non) {
                    ret = this.createConstantNode(node, val != 0);
                }
                else {
                    ret = this.createConstantNode(node, val);
                }
            }
            else if ((operandNode instanceof Faux) || (operandNode instanceof Vrai)) {
                ret = this.createConstantNode(node, node.apply(operandNode instanceof Vrai));
            }
        }

        return ret;
    }

    public Object visit(Vrai node) { return null; }
}
