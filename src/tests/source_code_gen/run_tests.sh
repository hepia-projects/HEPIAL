#!/usr/bin/env bash

CUPLIB=lib/java-cup-11a.jar
CLASSPATH=$CUPLIB:tds:error_manager:ast:ast_visitors:.
TESTS=7

echo "=== TEST GENERATE SOURCE CODE FROM AST ==="

for i in `eval echo {1..$TESTS}`
do
    echo -n "Test ${i}:       "
    OUTPUT=$(java -classpath $CLASSPATH Hepialc gensourcecode tests/source_code_gen/hepial${i} nooptimise 2>&1)

    echo -e "$OUTPUT" | diff - tests/source_code_gen/output${i}.txt
    if [ $? -eq 0 ]; then
        echo -en "\e[42mOK!"
    else
        echo -en "\e[41mFailed!"
    fi
    echo -en '\e[49m\n' #set default background color
done
