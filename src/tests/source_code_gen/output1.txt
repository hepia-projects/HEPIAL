programme hepial1
    entier x;
    booleen y;
    constant booleen z = vrai;
debutprg
    x = -1;
    x = 2 * (10 + 2);
    ecrire x;
    x = x - 2;
    ecrire x;
    x = x * 3;
    ecrire x;
    x = x / 2;
    ecrire x;
    y = x <> 1;
    y = x == 1;
    y = x <= 1;
    y = x >= 1;
    y = x > 1;
    y = x < 1;
    y = z et vrai;
    y = z ou faux;
    y = non z;
    y = ~z;
finprg
