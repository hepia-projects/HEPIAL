#!/usr/bin/env bash
CUPLIB=lib/java-cup-11a.jar
CLASSPATH=$CUPLIB:tds:error_manager:ast:ast_visitors:.
TESTS=15

echo "=== TEST COMPILATION & EXECUTION ==="

for i in `eval echo {1..$TESTS}`
do
    echo "Test ${i}:"
    BYTECODE=
    BYTECODE=$(java -classpath $CLASSPATH Hepialc genbytecode tests/byte_code_gen/test_${i}.input nooptimise 2>&1)

    echo -n "  - Bytecode: "
    echo -e "$BYTECODE" | diff - tests/byte_code_gen/test_${i}.bytecode
    if [ $? -eq 0 ]; then
        echo -en "\e[42mOK!"
    else
        echo -en "\e[41mFailed!"
    fi
    echo -en '\e[49m\n' #set default background color

    echo -e "$BYTECODE" | java -jar lib/jasmin.jar -d /tmp /dev/stdin > /dev/null
    OUTPUT=
    if [ -f "tests/byte_code_gen/test_${i}.stdin" ]; then
       OUTPUT=$(java -classpath /tmp Program < tests/byte_code_gen/test_${i}.stdin)
    else
       OUTPUT=$(java -classpath /tmp Program)
    fi

    echo -n "  - Output:   "
    echo -e "$OUTPUT" | diff - tests/byte_code_gen/test_${i}.output
    if [ $? -eq 0 ]; then
        echo -en "\e[42mOK!"
    else
        echo -en "\e[41mFailed!"
    fi
    # echo -e "$OUTPUT" | diff - tests/byte_code_gen/test_${i}.output  && echo -en "\e[42mOK!" || echo -en "\e[41mFailed!"
    echo -en '\e[49m\n' #set default background color
done
