/*
 * Main program that launch the parsing.
 *
 * @author Claudio Sousa, David Gonzalez
 */

import java.util.Vector;
import java.io.FileReader;
import java_cup.runtime.Symbol;

public class Hepialc {
    public static void main(String[] arg) {
        try {
            if (arg.length < 2) {
                throw new Exception("Please specify the two parameters: the parsing mode and the file name");
            }

            parser myP = new parser(new HepialLexer(new FileReader(arg[1])));

            try {
                DeclarationProgramme program = (DeclarationProgramme)myP.parse().value;
                if (program == null)
                    return;

                program.accept(new AnalyseurSemantique());
                if (ErrorManager.getInstance().hasErrorsOccured()) {
                    ErrorManager.getInstance().printErrorsOccured();
                    return;
                }

                if ((arg.length < 3) || (!arg[2].equals("nooptimise"))) {
                    program.accept(new ConstantExpressionOptimizer());
                }

                switch(arg[0]){
                    case "genbytecode":
                        ByteCodeGenerator codeGenerator = new ByteCodeGenerator();
                        program.accept(codeGenerator);
                        System.out.println(codeGenerator.getByteCode());
                        break;
                    case "gensourcecode":
                        SourceCodeGenerator sourceGenerator = new SourceCodeGenerator();
                        program.accept(sourceGenerator);
                        System.out.println(sourceGenerator.getCode());
                        break;
                    case "checksourcecode":
                        break;
                    default:
                        throw new Exception("Unrecognized running mode: '" + arg[0] + "'");
                }
            } catch (Exception e) {
                //le contenu du fichier est incorrect
                System.out.println("Parse error: " + e);
                e.printStackTrace();
            }
        } catch (Exception e) {
            //le contenu du fichier est incorrect
            System.out.println("Invalid file: " + e);
            e.printStackTrace();
        }
    }
}
