/*
 * Base class that represent any base type of the language.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public abstract class Type {
    /**
     * Determine if the given type is compatible with this one
     */
    public abstract boolean estConforme(Type other);
}
