/*
 * Represent a single identifier (whatever the level) in the symbol table.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Entree {
    /**
     * Name of the entry
     */
    private String name = "";

    /**
     * Constructor
     */
    public Entree(String name) {
        this.name = name;
    }

    /**
     * Return the entry's name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Correctly test two entry's instances
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        Entree other = (Entree)obj;
        return this.name.equals(other.name);
    }
    /**
     * Return the entry's hash
     */
    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    /**
     * Transform string visualization of the data
     */
    public String toString() {
        return this.name;
    }

}
