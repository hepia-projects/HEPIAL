/*
 * Represent the 'int' base type.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class TypeEntier extends Type {
    /**
     * Determine if the given type is compatible with this one
     */
    public boolean estConforme(Type other) {
        return other instanceof TypeEntier;
    }

    /**
     * Transform this type into a visualisable string
     */
    public String toString() {
        return "entier";
    }
}
