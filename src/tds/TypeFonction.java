/*
 * Represent the 'function' base type, with a return type and args types.
 *
 * @author Claudio Sousa, David Gonzalez
 */

import java.util.ArrayList;

public class TypeFonction extends Type {
    private Type return_type = null;
    private ArrayList<Type> arg_types = null;

    /**
     * Constructor
     */
    public TypeFonction(Type return_type) {
        this.return_type = return_type;
        this.arg_types = new ArrayList<Type>();
    }

    /**
     * Get the return type of the function
     */
    public Type getReturnType() {
        return this.return_type;
    }
    /**
     * Get the argument types
     */
    public ArrayList<Type> getArgTypes() {
        return this.arg_types;
    }

    /**
     * Add a new argument to this function
     */
    public void ajoutArgument(Type arg_type) {
        this.arg_types.add(arg_type);
    }

    /**
     * Determine if the given type is compatible with this one
     */
    public boolean estConforme(Type other) {
        if (!(other instanceof TypeFonction)) {
            return false;
        }

        TypeFonction other_fonct = (TypeFonction)other;
        if (!this.return_type.estConforme(other_fonct.return_type)) {
            return false;
        }

        if (this.arg_types.size() != other_fonct.arg_types.size()){
            return false;
        }

        for (int i = 0; i < this.arg_types.size(); ++i) {
            if (!this.arg_types.get(i).estConforme(other_fonct.arg_types.get(i))){
                return false;
            }
        }

        return true;
    }

    /**
     * Transform this type into a visualisable string
     */
    public String toString() {
        String output = this.return_type + "(";
        for (int i = 0; i < this.arg_types.size(); ++i) {
            output += this.arg_types.get(i);
            if (i < (this.arg_types.size() - 1)) {
                output += ",";
            }
        }
        output += ")";

        return output;
    }
}
