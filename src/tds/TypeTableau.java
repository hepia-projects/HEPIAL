/*
 * Represent the 'array' base type, with a unique content type and possibly multi-dimensional.
 *
 * @author Claudio Sousa, David Gonzalez
 */

import java.util.ArrayList;

public class TypeTableau extends Type {
    private Type content_type = null;
    private ArrayList<int[]> dimension_ranges = null;

    /**
     * Constructor
     */
    public TypeTableau(Type content_type) {
        this.content_type = content_type;
        this.dimension_ranges = new ArrayList<int[]>();
    }

    /**
     * Get type of the array content
     */
    public Type GetContentType(){
        return content_type;
    }
    /**
     * Get the range dimension
     */
    public ArrayList<int[]> GetDimensions(){
        return dimension_ranges;
    }

    /**
     * Add a new dimension to the array, specified by the given range
     */
    public void ajoutDimension(int begin, int end) {
        this.dimension_ranges.add(new int[] { begin, end });
    }

    /**
     * Get the same array type but with the first dimension strip out.
     * If there is only one, the content type is returned.
     */
    public Type getSameWithoutFirstDimension() {
        Type ret = null;
        if (this.dimension_ranges.size() == 1) {
            ret = this.GetContentType();
        }
        else {
            TypeTableau ret_tab = new TypeTableau(this.GetContentType());
            ret_tab.dimension_ranges.addAll(this.dimension_ranges);
            ret_tab.dimension_ranges.remove(0);
            ret = ret_tab;
        }
        return ret;
    }

    /**
     * Get the same array type but with the last dimension strip out.
     * If there is only one, the content type is returned.
     */
    public Type getSameWithoutLastDimension() {
        Type ret = null;
        if (this.dimension_ranges.size() == 1) {
            ret = this.GetContentType();
        }
        else {
            TypeTableau ret_tab = new TypeTableau(this.GetContentType());
            ret_tab.dimension_ranges.addAll(this.dimension_ranges);
            ret_tab.dimension_ranges.remove(ret_tab.dimension_ranges.size() - 1);
            ret = ret_tab;
        }
        return ret;
    }

    /**
     * Determine if the given type is compatible with this one
     */
    public boolean estConforme(Type other) {
        if (!(other instanceof TypeTableau)) {
            return false;
        }

        TypeTableau other_tab = (TypeTableau)other;
        if (!this.content_type.estConforme(other_tab.content_type)) {
            return false;
        }

        if (this.dimension_ranges.size() != other_tab.dimension_ranges.size()){
            return false;
        }

        for (int i = 0; i < this.dimension_ranges.size(); ++i) {
            int[] this_range = this.dimension_ranges.get(i);
            int[] other_range = other_tab.dimension_ranges.get(i);
            if (this_range[0] != other_range[0] || this_range[1] != other_range[1]){
                return false;
            }
        }
        return true;
    }

    /**
     * Transform this type into a visualisable string
     */
    public String toString() {
        String output = this.content_type + "[";
        for (int i = 0; i < this.dimension_ranges.size(); ++i) {
            output += Integer.toString(this.dimension_ranges.get(i)[0]) + ".." + Integer.toString(this.dimension_ranges.get(i)[1]);
            if (i < (this.dimension_ranges.size() - 1)) {
                output += ",";
            }
        }
        output += "]";

        return output;
    }
}
