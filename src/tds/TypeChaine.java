/*
 * Represent the 'string' type.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class TypeChaine extends Type {
    /**
     * Determine if the given type is compatible with this one
     */
    public boolean estConforme(Type other) {
        return other instanceof TypeChaine;
    }

    /**
     * Transform this type into a visualisable string
     */
    public String toString() {
        return "chaine";
    }
}
