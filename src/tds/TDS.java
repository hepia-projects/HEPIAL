/*
 * Singleton class for manipulating found symbol in code (Symbol table)
 *
 * @author Claudio Sousa, David Gonzalez
 */

import java.util.*;

public class TDS {
    /**
     * Singleton instance
     */
    private static TDS instance = new TDS();
    /**
     * Symbols table
     */
    private HashMap<Entree, HashMap<Integer, Symbole>> table;
    /**
     * Block number's stack
     */
    private Stack<Integer> pile;
    /**
     * Current level of the block
     */
    private int numerobloc = 0;

    /**
     * Return the unique instance of this class
     */
    public static TDS getInstance() {
        return TDS.instance;
    }

    /**
     * Private constructor
     */
    private TDS() {
        this.table = new HashMap<Entree, HashMap<Integer, Symbole>>();
        this.pile = new Stack<Integer>();
    }

    /**
     * Add a new entry in the symbols table in the current level
     */
    public void ajouter(Entree e, Symbole s, String filename, int line, int col) {
        if (!this.table.containsKey(e)) {
            this.table.put(e, new HashMap<Integer, Symbole>());
        }
        HashMap<Integer, Symbole> entree_table = this.table.get(e);

        if (!entree_table.containsKey(this.numerobloc)) {
            entree_table.put(this.numerobloc, s);
        } else {
            ErrorManager.getInstance().add(
                "identifiant '" + e.getName() + "' doublement déclaré", filename, line, col
            );
        }
    }

    /**
     * Return the corresponding symbol of the entry starting from the current block to the global
     */
    public Symbole identifier(Entree e) {
        if (!this.table.containsKey(e)){
            return null;
        }

        HashMap<Integer, Symbole> entree_table = this.table.get(e);
        for(int i = this.pile.size() - 1; i >= 0; i--){
            int blocnumber = this.pile.elementAt(i);
            if (entree_table.containsKey(blocnumber)){
                return entree_table.get(blocnumber);
            }
        }
        return null;
    }

    /*
     * reset the block number to initial value
     */
    public void resetBlocNumber() {
        this.numerobloc = 0;
    }

    /**
     * Start a new block
     */
    public void entreeBloc() {
        this.numerobloc++;
        this.pile.push(numerobloc);
    }

    /**
     * End the current block
     */
    public void sortieBloc() {
        this.pile.pop();
    }
}
