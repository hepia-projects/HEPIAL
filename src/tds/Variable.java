/*
 * Represent a single variable (whatever the level), which is a tye and a name.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Variable {
    /**
     * Symbol type (of the language)
     */
    private Type type = null;
    /**
     * Name of the variable
     */
    private String name = "";

    /**
     * Constructor
     */
    public Variable(Type t, String name) {
        this.type = t;
        this.name = name;
    }

    /**
     * Return the variable's type
     */
    public Type getType() {
        return this.type;
    }
    /**
     * Return the variable's name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Transform string visualization of the data
     */
    public String toString() {
        return this.type + " " + this.name;
    }

}
