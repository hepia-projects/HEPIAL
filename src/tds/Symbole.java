/*
 * Represent a variable of a specific level of the symbols table.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Symbole {
    /**
     * Symbol type (of the language)
     */
    private Type type = null;
    /**
     * Symbol constness
     */
    private boolean constant = false;

    /**
     * Constructor
     */
    public Symbole(Type t, boolean c) {
        this.type = t;
        this.constant = c;
    }

    /**
     * Return the symbol's type
     */
    public Type getType() {
        return this.type;
    }
    /**
     * Determine the constness of the symbol
     */
    public boolean isConst() {
        return this.constant;
    }

    /**
     * Transform string visualization of the data
     */
    public String toString() {
        return (this.constant ? "constant " : "") + this.type;
    }
}
