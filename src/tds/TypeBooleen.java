/*
 * Represent the 'bool' base type.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class TypeBooleen extends Type {
    /**
     * Determine if the given type is compatible with this one
     */
    public boolean estConforme(Type other) {
        return other instanceof TypeBooleen;
    }

    /**
     * Transform this type into a visualisable string
     */
    public String toString() {
        return "booleen";
    }
}
