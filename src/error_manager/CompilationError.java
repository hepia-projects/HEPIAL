/*
 * Class for storing error messages and related info
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class CompilationError {

    /**
     * Error message
     */
    private String message = "";
    /**
     * File where the error occured
     */
    private String filename = "";
    /**
     * Line where the error occured
     */
    private int line = -1;
    /**
     * Column where the error occured
     */
    private int column = -1;

    /**
     * Constructor
     */
    public CompilationError(String msg, String filename, int line, int col) {
        this.message = msg;
        this.filename = filename;
        this.line = line;
        this.column = col;
    }

    /**
     * Stringify this error
     */
    public String toString() {
        return this.filename + ":" + (this.line + 1) + ":" + this.column + ": " + this.message;
    }
}
