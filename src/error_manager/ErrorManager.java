/*
 * Singleton class for managing errors
 *
 * @author Claudio Sousa, David Gonzalez
 */

import java.util.*;

public class ErrorManager {

    ArrayList<CompilationError> errorList;

    /**
     * Singleton instance
     */
    private static ErrorManager instance = new ErrorManager();

    /**
     * Retourne l'instance unique de la classe
     */
    public static ErrorManager getInstance() {
        return ErrorManager.instance;
    }

    /**
     * Class constructor
     */
    private ErrorManager() {
        this.errorList = new ArrayList<CompilationError>();
    }

    /**
     * Add new error
     */
    public void add(CompilationError error) {
        this.errorList.add(error);
    }
    /**
     * Add new error
     */
    public void add(String msg, String filename, int line, int col) {
        this.add(new CompilationError(msg, filename, line, col));
    }

    /**
     * Determine if there is any error
     */
    public boolean hasErrorsOccured() {
        return this.errorList.size() > 0;
    }

    /**
     * Print the error on the console
     */
    public void printErrorsOccured() {
        for (CompilationError e: this.errorList)
            System.out.println(e);
    }
}
