/*
 * Base class that represent a expression wrapped by parenthesis
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Parentheses extends Expression {
    /**
     * The expression
     */
    protected Expression expression;

    /**
     * Constructor
     */
    public Parentheses(Expression expression, String fl, int line, int col) {
        super(fl, line, col);
        this.expression = expression;
    }

    /**
     * Get the expression
     */
    public Expression getExpression() {
        return this.expression;
    }

    /**
     * Set the expression
     */
    public void grefferExpression(Expression exp) {
        this.expression = exp;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
