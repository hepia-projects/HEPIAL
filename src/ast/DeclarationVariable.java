/*
 * Represent a variable declaration instruction node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class DeclarationVariable extends Instruction {
    /**
     * The declared variable identifier
     */
    protected Idf identifier;

    /**
     * Constructor
     */
    public DeclarationVariable(Idf identifier, String fl, int line, int col) {
        super(fl, line, col);
        this.setIdentifier(identifier);
    }

    /**
     * Get the declared variable identifier
     */
    public Idf getIdentifier() {
        return this.identifier;
    }

    /**
     * Set the declared variable identifier
     */
    public void setIdentifier(Idf identifier) {
        this.identifier = identifier;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
