/*
 * Represent a RETURN instruction node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Retour extends Instruction {
    /**
     * The source operand (at its right)
     */
    protected Expression source;

    /**
     * Constructor
     */
    public Retour(Expression src, String fl, int line, int col) {
        super(fl, line, col);
        this.grefferSource(src);
    }

    /**
     * Get the source operand (at its right)
     */
    public Expression getSource() {
        return this.source;
    }

    /**
     * Set the source operand (at its right)
     */
    public void grefferSource(Expression exp) {
        this.source = exp;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
