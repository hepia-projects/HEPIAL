/*
 * Represent a READ instruction node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Lire extends Instruction {
    /**
     * The destination operand
     */
    protected Idf destination;

    /**
     * Constructor
     */
    public Lire(Idf dest, String fl, int line, int col) {
        super(fl, line, col);
        this.setDestination(dest);
    }

    /**
     * Get the destination operand
     */
    public Idf getDestination() {
        return this.destination;
    }

    /**
     * Set the destination operand
     */
    public void setDestination(Idf dest) {
        this.destination = dest;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }

}
