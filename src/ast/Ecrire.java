/*
 * Represent a WRITE instruction node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Ecrire extends Instruction {
    /**
     * The source operand
     */
    protected Expression source;

    /**
     * Constructor
     */
    public Ecrire(Expression src, String fl, int line, int col) {
        super(fl, line, col);
        this.setSource(src);
    }

    /**
     * Get the source operand
     */
    public Expression getSource() {
        return this.source;
    }

    /**
     * Set the source operand
     */
    public void setSource(Expression exp) {
        this.source = exp;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
