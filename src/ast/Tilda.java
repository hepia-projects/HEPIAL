/*
 * Represent an NOT bitwise expression node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Tilda extends Unaire {
    /**
     * Constructor
     */
    public Tilda(String fl, int line, int col) {
        super(fl, line, col);
    }

    /**
     * Get the unary operator
     */
    public String operateur() {
        return "~";
    }

    /**
     * Apply the operator on the given value.
     */
    public int apply(int value) {
        return ~value;
    }
    /**
     * Apply the operator on the given value.
     */
    public boolean apply(boolean value) {
        return !value;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
