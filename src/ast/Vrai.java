/*
 * Represent the TRUE node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Vrai extends Expression {
    /**
     * Constructor
     */
    public Vrai(String fl, int line, int col) {
        super(fl, line, col);
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
