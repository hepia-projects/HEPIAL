/*
 * Base class that represent a binary expression node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public abstract class Binaire extends Expression {
    /**
     * The expression at its left
     */
    protected Expression operandeGauche;
    /**
     * The expression at its right
     */
    protected Expression operandeDroit;

    /**
     * Constructor
     */
    public Binaire(String fl, int line, int col) {
        super(fl, line, col);
    }

    /**
     * Get the left expression
     */
    public Expression getGauche() {
        return this.operandeGauche;
    }
    /**
     * Get the right expression
     */
    public Expression getDroite() {
        return this.operandeDroit;
    }
    /**
     * Get the binary operator.
     * Must be implemented by the child class.
     */
    public abstract String operateur();

    /**
     * Set the left expression
     */
    public void grefferGauche(Expression exp) {
        this.operandeGauche = exp;
    }
    /**
     * Set the right expression
     */
    public void grefferDroit(Expression exp) {
        this.operandeDroit = exp;
    }

    /**
     * Apply the operator on the two given values.
     * Must be implemented by the child class.
     */
    public abstract int apply(int gauche, int droite);
    /**
     * Apply the operator on the two given values.
     * Must be implemented by the child class.
     */
    public abstract boolean apply(boolean gauche, boolean droite);

    /**
     * Transform this node into a visualisable string
     */
    public String toString() {
        return this.operandeGauche + " " + this.operateur() + " " + this.operandeDroit;
    }
}
