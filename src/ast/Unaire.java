/*
 * Base class that represent an unary expression node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public abstract class Unaire extends Expression {
    /**
     * The expression
     */
    protected Expression operande;

    /**
     * Constructor
     */
    public Unaire(String fl, int line, int col) {
        super(fl, line, col);
    }

    /**
     * Get the expression
     */
    public Expression getOperand() {
        return this.operande;
    }
    /**
     * Get the unary operator.
     * Must be implemented by the child class.
     */
    public abstract String operateur();

    /**
     * Set the expression
     */
    public void grefferOperand(Expression exp) {
        this.operande = exp;
    }

    /**
     * Apply the operator on the the given value.
     * Must be implemented by the child class.
     */
    public abstract int apply(int value);
    /**
     * Apply the operator on the the given value.
     * Must be implemented by the child class.
     */
    public abstract boolean apply(boolean value);
}
