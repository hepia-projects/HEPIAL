/*
 * Represent an WHILE instruction node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Tantque extends Instruction {
    /**
     * The condition expression
     */
    protected Expression condition;
    /**
     * The loop's instructions
     */
    protected Bloc instructions;

    /**
     * Constructor
     */
    public Tantque(Expression condition, Bloc instructions, String fl, int line, int col) {
        super(fl, line, col);
        this.grefferCondition(condition);
        this.grefferInstruction(instructions);
    }

    /**
     * Get the condition expression
     */
    public Expression getCondition() {
        return this.condition;
    }
    /**
     * Get the loop's instructions
     */
    public Bloc getInstruction() {
        return this.instructions;
    }

    /**
     * Set the condition expression
     */
    public void grefferCondition(Expression condition) {
        this.condition = condition;
    }
    /**
     * Set the loop's instructions
     */
    public void grefferInstruction(Bloc instructions) {
        this.instructions = instructions;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
