/*
 * Represent a variable declaration instruction node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class DeclarationConstant extends DeclarationVariable {
    /**
     * Constant expression of this constant
     */
    protected Expression constantValue;

    /**
     * Constructor
     */
    public DeclarationConstant(Idf identifier, Expression constantExpr, String fl, int line, int col) {
        super(identifier, fl, line, col);
        this.setConstantExpression(constantExpr);
    }

    /**
     * Get the constant expression
     */
    public Expression getConstantExpression() {
        return this.constantValue;
    }

    /**
     * Set the constant expression
     */
    public void setConstantExpression(Expression constantExpr) {
        this.constantValue = constantExpr;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
