/*
 * Represent a function declaration instruction node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */
import java.util.*;

public class DeclarationFonction extends DeclarationProgramme {
    /**
     * Return type
     */
    protected Type return_type;
    /**
     * Argument names list (list of DeclarationVariable)
     */
    protected Bloc params = null;

    /**
     * Constructor
     */
    public DeclarationFonction(Type return_type, Idf identifier, String fl, int line, int col){
        super(identifier, fl, line, col);
        this.return_type = return_type;
        this.params = new Bloc(fl, line, col);
    }

    /**
     * Get the fonction return type
     */
    public Type getReturnType() {
        return this.return_type;
    }

    /**
     * Get the argument names list
     */
    public Bloc getParameters() {
        return this.params;
    }
    /**
     * Set the argument names list
     */
    public void addParameter(DeclarationVariable parameter) {
        this.params.addInstruction(parameter);
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
