/*
 * Represent a function call expression node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

import java.util.ArrayList;

public class Appel extends Expression {
    /**
     * Name of the function to call
     */
    protected Idf nomFonction;
    /**
     * List of arguments
     */
    protected ArrayList<Expression> params;

    /**
     * Constructor
     */
    public Appel(Idf nomFonction, ArrayList<Expression> params, String fl, int line, int col) {
        super(fl, line, col);
        this.setNomFonction(nomFonction);
        this.setParameters(params);
    }
    /**
     * Constructor without args
     */
    public Appel(Idf nomFonction, String fl, int line, int col) {
        this(nomFonction, new ArrayList<Expression>(), fl, line, col);
    }

    /**
     * Get the name of the function to call
     */
    public Idf getNomFonction() {
        return this.nomFonction;
    }
    /**
     * Get the list of arguments
     */
    public ArrayList<Expression> getParameters() {
        return this.params;
    }

    /**
     * Set the name of the function to call
     */
    public void setNomFonction(Idf nomFonction) {
        this.nomFonction = nomFonction;
    }

    /**
     * Return the number of argument
     */
    public int getParameterLength() {
        return this.params.size();
    }
    /**
     * Get the parameter given by the index
     */
    public Expression getParameter(int i) {
        return this.params.get(i);
    }
    /**
     * Set the parameter given by the index
     */
    public void setParameter(int i, Expression exp) {
        this.params.set(i, exp);
    }
    /**
     * Set the list of arguments
     */
    public void setParameters(ArrayList<Expression> params) {
        this.params = params;
    }
    /**
     * Add an argument to the list of arguments
     */
    public void addParameters(Expression param) {
        this.params.add(param);
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
