/*
 * Represent the FALSE node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Faux extends Expression {

    /**
     * Constructor
     */
    public Faux(String fl, int line, int col) {
        super(fl, line, col);
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
