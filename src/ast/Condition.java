/*
 * Represent an IF instruction node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Condition extends Instruction {
    /**
     * The condition expression
     */
    protected Expression condition;
    /**
     * The instructions when the condition is true
     */
    protected Bloc thenInstructions;
    /**
     * The instructions when the condition is false
     */
    protected Bloc elseInstructions;

    /**
     * Constructor
     */
    public Condition(Expression condition, Bloc thenInstructions, Bloc elseInstructions, String fl, int line, int col) {
        super(fl, line, col);
        this.grefferCondition(condition);
        this.grefferThenInstruction(thenInstructions);
        this.grefferElseInstruction(elseInstructions);
    }
    /**
     * Constructor without else bloc
     */
    public Condition(Expression condition, Bloc thenInstructions, String fl, int line, int col) {
        this(condition, thenInstructions, null, fl, line, col);
    }

    /**
     * Get the condition expression
     */
    public Expression getCondition() {
        return this.condition;
    }
    /**
     * Get the instructions when the condition is true
     */
    public Bloc getThenInstruction() {
        return this.thenInstructions;
    }
    /**
     * Get the instructions when the condition is false
     */
    public Bloc getElseInstruction() {
        return this.elseInstructions;
    }
    /**
     * Determine if the IF statement has an else instruction set
     */
    public boolean hasElse() {
        return this.elseInstructions != null;
    }

    /**
     * Set the condition expression
     */
    public void grefferCondition(Expression condition) {
        this.condition = condition;
    }
    /**
     * Set the instructions when the condition is true
     */
    public void grefferThenInstruction(Bloc thenInstructions) {
        this.thenInstructions = thenInstructions;
    }
    /**
     * Set the instructions when the condition is false
     */
    public void grefferElseInstruction(Bloc elseInstructions) {
        this.elseInstructions = elseInstructions;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
