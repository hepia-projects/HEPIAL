/*
 * Represent an FOR instruction node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Pour extends Instruction {
    /**
     * The variable name for iteration
     */
    protected Idf iteratorName;
    /**
     * The from expression
     */
    protected Expression from;
    /**
     * The to expression
     */
    protected Expression to;
    /**
     * The loop's instructions
     */
    protected Bloc instructions;

    /**
     * Constructor
     */
    public Pour(Idf iteratorName, Expression from, Expression to, Bloc instructions, String fl, int line, int col) {
        super(fl, line, col);
        this.setIteratorName(iteratorName);
        this.grefferFrom(from);
        this.grefferTo(to);
        this.grefferInstruction(instructions);
    }

    /**
     * Get the variable name for iteration
     */
    public Idf getIteratorName() {
        return this.iteratorName;
    }
    /**
     * Get the from expression
     */
    public Expression getFrom() {
        return this.from;
    }
    /**
     * Get the to expression
     */
    public Expression getTo() {
        return this.to;
    }
    /**
     * Get the loop's instructions
     */
    public Bloc getInstruction() {
        return this.instructions;
    }

    /**
     * Set the variable name for iteration
     */
    public void setIteratorName(Idf iteratorName) {
        this.iteratorName = iteratorName;
    }
    /**
     * Set the from expression
     */
    public void grefferFrom(Expression from) {
        this.from = from;
    }
    /**
     * Set the to expression
     */
    public void grefferTo(Expression to) {
        this.to = to;
    }
    /**
     * Set the loop's instructions
     */
    public void grefferInstruction(Bloc instructions) {
        this.instructions = instructions;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
