/*
 * Base class that represent an expression node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public abstract class Expression extends ASTNode {

    /**
     * Constructor
     */
    public Expression(String fl, int line, int col) {
        super(fl, line, col);
    }
}
