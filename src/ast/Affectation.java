/*
 * Represent an assignment node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Affectation extends Instruction {
    /**
     * The source operand (at its right)
     */
    protected Expression source;
    /**
     * The destination variable or array (at its left)
     */
    protected Expression destination;

    /**
     * Constructor
     */
    public Affectation(Expression dest, Expression src, String fl, int line, int col) {
        super(fl, line, col);
        this.grefferSource(src);
        this.grefferDestination(dest);
    }

    /**
     * Get the source operand (at its right)
     */
    public Expression getSource() {
        return this.source;
    }
    /**
     * Get the destination variable or array (at its left)
     */
    public Expression getDestination() {
        return this.destination;
    }

    /**
     * Set the source operand (at its right)
     */
    public void grefferSource(Expression exp) {
        this.source = exp;
    }
    /**
     * Set the destination variable or array (at its left)
     */
    public void grefferDestination(Expression exp) {
        this.destination = exp;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
