/*
 * Base class that represent an instruction node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public abstract class Instruction extends ASTNode {

    /**
     * Constructor
     */
    public Instruction(String fl, int line, int col) {
        super(fl, line, col);
    }
}
