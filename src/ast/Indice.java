/*
 * Represent a subscript expression node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Indice extends Expression {
    /**
     * The array index expression
     */
    protected Expression index;

    /**
     * The array identifier
     */
    protected Expression identifier;

    /**
     * Constructor
     */
    public Indice(Expression index, Expression identifier, String fl, int line, int col) {
        super(fl, line, col);
        this.index = index;
        this.identifier = identifier;
    }

    /**
     * Get the index expression
     */
    public Expression getIndex() {
        return this.index;
    }
    /**
     * Set the index expression
     */
    public void setIndex(Expression exp) {
        this.index = exp;
    }

    /**
     * Get the array identifier
     */
    public Expression getIdentifier() {
        return this.identifier;
    }

    /**
     * Sets the array identifier
     */
    public void setIdentifier(Expression identifier) {
        this.identifier = identifier;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
