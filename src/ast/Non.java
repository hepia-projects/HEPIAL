/*
 * Represent an NOT boolean expression node inside the AST.
 *
 * @author Claudio Sousa, David Gonzalez
 */

public class Non extends Unaire {
    /**
     * Constructor
     */
    public Non(String fl, int line, int col) {
        super(fl, line, col);
    }

    /**
     * Get the unary operator
     */
    public String operateur() {
        return "non";
    }

    /**
     * Apply the operator on the given value.
     */
    public int apply(int value) {
        return (!(value != 0)) ? 1 : 0;
    }
    /**
     * Apply the operator on the given value.
     */
    public boolean apply(boolean value) {
        return !value;
    }

    /**
     * Accepts a AST visitor
     */
    Object accept(ASTVisitor visitor){
        return visitor.visit(this);
    }
}
