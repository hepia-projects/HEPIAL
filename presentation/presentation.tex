\documentclass{beamer}

\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}

\usepackage{graphicx}
\graphicspath{{./img/}}
\usepackage{hyperref}
\usepackage{pgfpages}
\usepackage{listings}
\usepackage{caption}

\mode<presentation>
{
    \useinnertheme[shadow]{rounded}
    \useoutertheme{default}
    \usecolortheme{lily}
    \usecolortheme{whale}
    \usecolortheme[named=darkgray]{structure}

    \beamertemplatenavigationsymbolsempty
    \setbeamercovered{transparent=10}
    \setbeamertemplate{footline}
    {
        \hfill \footnotesize{\insertframenumber/\inserttotalframenumber} \hspace{0.5cm}
        \vspace{0.5cm}
    }
}

% Remove figure text of image
\captionsetup[figure]{labelformat=empty}

\lstset{
    basicstyle=\ttfamily\small,
    stringstyle=\ttfamily\color{green!50!black},
    keywordstyle=\bfseries\color{blue},
    commentstyle=\itshape\color{red!50!black},
    showstringspaces=true,
    tabsize=4,
    frame=single,
    numbers=left,
    numberstyle=\tiny,
    firstnumber=1,
    stepnumber=1,
    numbersep=5pt,
    breaklines=true
}

\title[]{Techniques de compilation}
\subtitle{Compilateur HEPIAL}
\author{Claudio Sousa, David Gonzalez}
\institute{HEPIA}
\date{18 juin 2018}

\pgfdeclareimage[width=1cm]{logo}{logo}
\logo{\pgfuseimage{logo}}

\AtBeginSection[]
{
    \begin{frame}<beamer>
        \frametitle{Sommaire}
        \tableofcontents[currentsection, currentsubsection]
    \end{frame}
}
\AtBeginSubsection[]
{
    \begin{frame}<beamer>
        \frametitle{Sommaire}
        \tableofcontents[currentsection, currentsubsection]
    \end{frame}
}

\setcounter{tocdepth}{3}

\begin{document}

\begin{frame}[plain]
    \titlepage
\end{frame}
\addtocounter{framenumber}{-1}

\begin{frame}
    \frametitle{Sommaire}
    \tableofcontents
\end{frame}

\section{Contexte}

\begin{frame}
    \frametitle{Contexte}
    \begin{itemize}
        \item Création d'un compilateur
        \item Language fait maison: HEPIAL
        \item Support des structures et instructions basiques:
        \begin{itemize}
            \item types: entier, booléen, tableau
            \item variables et constantes
            \item fonctions globales
            \item expressions
            \item conditions, boucles
            \item lecture/écriture sur console
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Contexte: Exemple de code HEPIAL}
    \begin{lstlisting}[language=pascal]
programme hepial

entier n;
entier result;

entier carre(entier i)
    entier r;
    debutfonc
        r = i * i;
        retourne r;
    finfonc

debutprg
    n = 2;
    result = carre(n);
    ecrire result;
finprg
    \end{lstlisting}
\end{frame}

\section{Composants}

\begin{frame}
    \frametitle{Composants}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{components}
        \end{center}
        \caption{Composants du compilateur avec entrée (Bleu) et sortie (Rouge)}
    \end{figure}
\end{frame}

\section{Fonctionnement}

\subsection{Analyseur lexical}

\begin{frame}
    \frametitle{Analyseur lexical: Fonctionnement}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{lexical}
        \end{center}
        \caption{Fonctionnement de l'analyseur lexical}
    \end{figure}
\end{frame}

\subsection{Analyseur syntaxique}

\begin{frame}
    \frametitle{Analyseur syntaxique: Fonctionnement}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{syntaxique}
        \end{center}
        \caption{Fonctionnement de l'analyseur syntaxique}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Arbre syntaxique abstrait}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{syntaxique_ast}
        \end{center}
        \caption{Construction de l'arbre syntaxique abstrait en postfixe}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Table des symboles}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Numéro de bloc par fonction
                \item Variables par bloc
                \begin{itemize}
                    \item fonction principale
                    \item locales aux fonctions
                    \item paramètres de fonctions
                \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[H]
                \begin{center}
                    \includegraphics[width=1\textwidth]{syntaxique_tds}
                \end{center}
                \caption{Exemple de numéros de bloc}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Analyseur sémantique}

\begin{frame}
    \frametitle{Analyseur sémantique: Fonctionnement}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{semantique}
        \end{center}
        \caption{Fonctionnement de l'analyseur sémantique}
    \end{figure}
\end{frame}

\subsection{Optimiseur de code}

\begin{frame}
    \frametitle{Optimiseur de code: Fonctionnement}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{optimiser}
        \end{center}
        \caption{Exemple d'optimisation d'expressions constantes}
    \end{figure}
\end{frame}

\subsection{Générateur de bytecode}

\begin{frame}
    \frametitle{Générateur de bytecode: programme example}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=0.9\textwidth]{bytecode-program}
        \end{center}
        \caption{Programme example}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Générateur de bytecode: main}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{bytecode-main}
        \end{center}
        \caption{Function main}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Générateur de bytecode: carre}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{bytecode-carre}
        \end{center}
        \caption{Function main}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Générateur de bytecode: si}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{bytecode-si}
        \end{center}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Générateur de bytecode: pour}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{bytecode-pour}
        \end{center}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Générateur de bytecode: lire}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{bytecode-lire}
        \end{center}
    \end{figure}
\end{frame}

\section{Tests}

\begin{frame}
    \frametitle{Tests}
    \begin{itemize}
        \item Développement piloté par les tests (TDD)
        \begin{itemize}
            \item entrée
            \item sortie attendu
            \item développement
        \end{itemize}
        \item 4 suite de tests:
        \begin{itemize}
            \item arbre syntaxique abstrait
            \item analyseur sémantique
            \item optimiseur
            \item générateur de bytecode
        \end{itemize}
    \end{itemize}
\end{frame}

\section{Démonstration}

\begin{frame}
    \frametitle{Démonstration}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=0.8\textwidth]{tests_output}
        \end{center}
    \end{figure}
\end{frame}

\section*{Questions}

\begin{frame}
    \frametitle{Questions}
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=0.3\textwidth]{question}
        \end{center}
    \end{figure}
\end{frame}

\end{document}
