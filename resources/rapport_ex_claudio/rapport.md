---
title: Analyseur SGML
author: Cláudio Sousa
date: 2018
header-includes:
- \usepackage{pdfpages}
- \usepackage[english]{babel}
- \usepackage{hyperref}
- \usepackage[dvipsnames]{xcolor}
- \hypersetup{colorlinks=true,  linkcolor=blue}
- \setcounter{tocdepth}{5}
- \usepackage{fancyhdr}
- \pagestyle{fancy}
- \fancyhead{}
- \fancyhead[CO,CE]{ITI 3 - Techniques de compilation}
- \fancyfoot[LE,RO]{\thepage}
- \fancyfoot[CO,CE]{Cláudio Sousa}
header: This is fancy
footer: So is this
bibliography: biblio.bib
geometry:
 - left=1.5cm
 - right=1.5cm
 - top=2.5cm
 - bottom=2.5cm
nocite:
abstract:
...

\ \

\begin{center}
ITI 3 - Techniques de compilation

HEPIA
\end{center}

\newpage

\newpage

\tableofcontents

\newpage

# Introduction

Ce document décrit le travail effectuée dans le cadre du laboratoire *Labo 2 : un analyseur SGML*[^1], qui a consisté à réaliser deux programmes afin de vérifier la structure d’un document SGML[^2] et SGML étendu.

Les programmes ont été réalisés à l’aide de deux outils : l’analyseur lexical *JFlex*[^3] et l’analyseur syntaxique CUP[^4].

Le premier programme réalisé vérifie qu'un fichier SGML est bien structuré (ouverture/fermeture des balises, attributs entre guillemets), le deuxième programme étend la définition de SGML pour proposer des sections conditionnelles.


# Première partie : le validateur SGML \label{validateur_SGML}

La première partie du laboratoire consiste à vérifier si un fichier SGML est bien structuré.

Un document SGML est considéré valide si :

  * le document est constitué d'une seule balise à la racine
  * chaque balise ouvrante a une balise fermante avec le même nom de balise
  * une balise peut être vide (*ex : `<br/>`*)
  * optionnellement, une balise peut contenir du texte et/ou une liste de balises
  * l'hiérarchie des balises est récursive
  * l'identifiant d'une balise est constitué de 1 ou plus caractères alphabétiques en majuscule ou minuscule
  * une balise ouvrante ou vide peuvent contenir une liste d'attributs identifiés de la forme `attr="value"` où `attr` est formé de 1 ou plus caractères alphabétiques en majuscule ou minuscule, et la valeur se trouve entre guillemets (`"`), et est composée de tout caractère sauf le guillemet, sans restriction sur la longueur

Si un fichier est bien structuré, le programme donnera en sortie la liste des identifiants des balises trouvées dans le document, sinon une erreur est émise. L'identifiant de balise imprimé est préfixé de "`+`" si c'est une balise ouvrante ou de "`-`" si c'est une balise fermante. L'identifiant d'une balise vide n'a pas de préfixe.


## Exemple

L'input suivant :

~~~~ {.sgml}
<SGML>
    <tag/>
    <Test numero="1,23" id="value">
        tata titi
    </Test>
</SGML>
~~~~

est reconnu comme étant correct par le programme, et il donnera le suivant :

~~~~{.bash}
+SGML tag +Test -Test -SGML
~~~~

## Code source

Le code source de cette première partie du projet réside dans le dossier `labo2.a`. On peut y trouver les fichiers et dossiers suivants :

  * **sgml.flex :** fichier JFlex qui détermine le lexique SGML.
  * **sgml.cup :** fichier CUP qui détermine la syntaxe SGML.
  * **tests :** dossier contenant les fichiers tests et leurs résultats attendus.
  * **Test.java :** fichier Java qui utilise le parser réalisé pour analyser un fichier donnée en paramètre.
  * **Makefile :** fichier Makefile qui réalise la compilation du projet et lance les tests.
  * **bin, lib :** dossiers contenant des librairies et exécutables CUP et JFlex.


## Analyseur lexical

Le fichier JFlex `sgml.flex` dans le projet décrit les règles lexicales qu'un fichier SGML doit respecter. Voici son contenu :

*Code source :*

~~~~ {.lex .numberLines}
import java_cup.runtime.*;
import java.util.Vector;
import java.util.regex.Pattern;

%%
%class SGML
%line
%column
%cup

%{
    // caractères spéciaux délimitant les balises
    Pattern pattern = Pattern.compile("(^</?|/?>$)");

    /* Retourne l'identifiant d'une balise d'après sa définition
       Accepte des balises ouvrantes, fermante et vides.
    */
    public String tagName(String fullTag) {
        fullTag = fullTag.trim();
        fullTag = pattern.matcher(fullTag).replaceAll("");
        String[] parts = fullTag.split(" ");
        return parts[0];
    }
%}

word = [A-Za-z]+
attribut = {word}=\"[^\"]*\"
tag_new_partial = \<{word}(\s+{attribut})*\s*
tag_open = {tag_new_partial}\>
tag_empty = {tag_new_partial}\/\>
tag_closing = \<\/\s*{word}\s*\>
blank = [\s|\t|\n|\r]+
content =  [^\<\n\r\t]+
%%

// balise d'ouverture
{tag_open}      { return new Symbol(sym.TAG_OPEN, tagName(yytext())); }

// balise vide
{tag_empty}     { return new Symbol(sym.TAG_EMPTY, tagName(yytext())); }

// balise de fermeture
{tag_closing}   { return new Symbol(sym.TAG_CLOSING, tagName(yytext())); }

// du texte à l'intérieur des balises (data)
{content}       { return new Symbol(sym.CONTENT, yytext()); }

// caractères blancs ignorés
{blank}         {;}

~~~~

### Fonctions

#### String tagName(String fullTag)

La fonction `tagName` reçoit en paramètre une balise trouvée - ouverture de balise, fermeture, ou balise vide - et retourne l'identifiant pour cette balise. Ceci est réalisé en enlevant les caractères qui délimitent la balise `(^</?|/?>$)` et en retournant le premier mot.

*Code source :*

~~~~ {.java .numberLines startFrom="12"}
// caractères spéciaux délimitant les balises
Pattern pattern = Pattern.compile("(^</?|/?>$)");

/* Retourne l'identifiant d'une balise d'après sa définition
    Accepte des balises ouvrantes, fermante et vides.
*/
public String tagName(String fullTag) {
    fullTag = fullTag.trim();
    fullTag = pattern.matcher(fullTag).replaceAll("");
    String[] parts = fullTag.split(" ");
    return parts[0];
}
~~~~

**Exemples :**

~~~~ {.java }
tagName("<SGML>") //retourne 'SGML'
tagName("<tag/>") //retourne 'tag'
tagName("<Test numero=\"1,23\" id=\"value\">") //retourne 'Test'
tagName("</Test>") //retourne 'Test'
~~~~

### Expressions
Les lignes `36-49` du fichier flex de la page précédente listent les différents expressions qu'on s'attend à trouver dans un document SGML. Voici leur signification :

#### Expression `{tag_open}`

Cette expression trouve les balises ouvrantes du document.

~~~~ {.java .numberLines startFrom="36"}
// balise d'ouverture
{tag_open}      { return new Symbol(sym.TAG_OPEN, tagName(yytext())); }
~~~~

**Expression régulière**

Son expression régulière est la suivante : \color{Red}`<`\color{Brown}`\s*`\color{Purple}`[A-Za-z]+`\color{Blue}`(\s+`\color{Orange}`[A-Za-z]+="`\color{ForestGreen}`[^"]*`\color{Orange}`"`\color{Blue}`)*`\color{Brown}`\s*`\color{Red}`>`\color{black}

La partie de l'expression \color{Red}`<`\color{Brown}`\s*`\color{black}...\color{Brown}`\s*`\color{Red}`>` \color{black} délimite les balises ouvrantes qui doivent commencer par \color{Red}`<` \color{black} et se terminer par \color{Red}`>`\color{black}, avec possibilité d'avoir des espaces à l'intérieur \color{Brown}`\s*`\color{black}.

La partie violette \color{Purple}`[A-Za-z]+` \color{Black} dit que la balise ouvrante doit forcément être suivie de l'identifiant de la balise. Cet identifiant se compose de lettres majuscules et minuscules et doit être de longueur supérieure ou égale à $1$.

La partie en bleu \color{Blue}`(\s+`\color{Black}...\color{Blue}`)*` \color{Black} entoure la définition d'attribut (point suivant) et spécifie qu'on peut avoir $0$ ou $n$ occurrences de ces attributs. On spécifie aussi que chaque attribut doit être précédé d'au moins $1$ espace.

À l'intérieur de la partie en bleu se trouve la définition de l'attribut : \color{Orange}`[A-Za-z]+="`\color{ForestGreen}`[^"]*`\color{Orange}`"`\color{Black}. La partie en orange spécifie que l'identifiant de l'attribut doit respecter les mêmes règles que l'identifiant de la balise et que cet identifiant doit être suivi de `=""`. Entre les guillemets, en vert \color{ForestGreen}`[^"]*`\color{Black}, on trouve les règles qui doivent respecter les valeurs des attributs : les valeurs n'ont pas de longueur minimale et peuvent être constituées de n'importe quel caractère sauf le guillemet `"`.

**Exemple :**

Pour l'input suivant :

~~~~ {.sgml}
<SGML>
    <tag/>
    <Test numero="1,23" id="value">
        tata titi
    </Test>
</SGML>
~~~~

Notre expression `{tag_open}` va trouver les correspondances suivantes :

  *  `<SGML>`
  *  `<Test numero="1,23" id="value">`

**Symbole CUP**

Pour chaque expression `{tag_open}` trouvée, un symbole CUP `TAG_OPEN` est instancié avec l'identifiant de la balise ouvrante, qui sera traité ultérieurement par CUP.

#### Expression `{tag_empty}`

Cette expression trouve les balises vides du document.

~~~~ {.java .numberLines startFrom="39"}
// balise vide
{tag_empty}     { return new Symbol(sym.TAG_EMPTY, tagName(yytext())); }
~~~~

**Expression régulière**

Son expression régulière est la suivante : `<\s*[A-Za-z]+(\s+[A-Za-z]+="[^"]*")*\s*`\color{Red}`/`\color{black}`>`

C'est la même que celle de `{tag_open}` à l'exception du caractère \color{Red}`/` \color{black}, en rouge dans l'expression ci-dessus, qui impose que la balise doit se terminer par `/>`.

**Exemple :**

Pour l'input suivant :

~~~~ {.sgml}
<SGML>
    <tag/>
    <Test numero="1,23" id="value">
        tata titi
    </Test>
</SGML>
~~~~

L'expression `{tag_empty}` va trouver la correspondance suivante :

  *  `<tag/>`

**Symbole CUP**

Lorqu'un `{tag_empty}` est trouvé, un symbole CUP `TAG_EMPTY` est instancié avec l'identifiant de la balise vide trouvée.


#### Expression `{tag_closing}`

Cette expression trouve les balises fermantes du document.

~~~~ {.java .numberLines startFrom="42"}
// balise de fermeture
{tag_closing}   { return new Symbol(sym.TAG_CLOSING, tagName(yytext()));
~~~~

**Expression régulière**

Son expression régulière est la suivante : \color{Red}`<\`\color{Brown}`\s*`\color{Purple}`[A-Za-z]+`\color{Brown}`\s*`\color{Red}`>`\color{black}

Une balise fermante doit commencer par \color{Red}`<\` \color{black} et se terminer par \color{Red}`>`\color{black}. À l'intérieur elle doit contenir l'identifiant de la balise à fermer \color{Purple}`[A-Za-z]+`\color{black}, optionnellement entourée d'espaces \color{Brown}`\s*`\color{black}.


**Exemple :**

Pour l'input suivant :

~~~~ {.sgml}
<SGML>
    <tag/>
    <Test numero="1,23" id="value">
        tata titi
    </Test>
</SGML>
~~~~

L'expression `{tag_closing}` va trouver les correspondances suivantes :

  *  `</Test>`
  *  `</SGML>`

**Symbole CUP**

Un `{tag_closing}` trouvé, donne lieu à l'instanciation d'un symbole CUP `TAG_CLOSING` avec l'identifiant de la balise fermante trouvée.


#### Expression `{content}`

Cette expression trouve le texte entre les balises

~~~~ {.java .numberLines startFrom="45"}
// du texte à l'intérieur des balises (data)
{content}       { return new Symbol(sym.CONTENT, yytext()); }
~~~~

**Expression régulière**

Son expression régulière est la suivante : `[^\<\n\r\t]+`

Le texte trouvé par cette expression est tout texte qui ne correspond pas à une expression listée précédemment, et s'arrête dès qu'elle trouve le début d'une balise (caractère `<` ).

**Exemple :**

Pour l'input suivant :

~~~~ {.sgml}
<SGML>
    du contenu
    <tag/>
    <Test numero="1,23" id="value">
        tata titi
    </Test>
</SGML>
~~~~

L'expression `{content}` va trouver les correspondances suivantes :

  *  `du contenu`
  *  `tata titi`

**Symbole CUP**

Un `{content}` trouvé, donne lieu à l'instanciation d'un symbole CUP `CONTENT` avec l'identifiant de la balise fermante trouvée.

#### Expression `{blank}`

Cette expression trouve les espaces, tabulations et retours de ligne dans le document.

~~~~ {.java .numberLines startFrom="48"}
// caractères blancs ignorés
{blank}         {;}
~~~~

**Expression régulière**

Son expression régulière est la suivante : `[\s|\t|\n|\r]+`

Cette expression trouve tous espaces et caractères spéciaux similaires (tabulations, retours de ligne) qui n'ont pas été retrouvés par une des expressions déclarées plus haut. Elle sert à ignorer les caractères non significatifs

**Symbole CUP**

Aucun symbole CUP n'est instancié pour les caractères trouvés par cette expression car ils sont ignorés.

## Analyseur syntaxique

Le fichier CUP `sgml.cup` dans le projet décrit les règles syntaxiques qu'un fichier SGML doit respecter. Son contenu est ci-dessous.

*Code source :*

~~~~ {.java .numberLines}
import java_cup.runtime.*;
import java.util.Vector;
import java.io.*;
import java.util.*;

parser code {:
    // Pile contenant la liste ordonnée des balises ouvertes trouvées
    Deque<String> tags = new ArrayDeque<String>();

    public void TagOpen(String tag){
        tags.push(tag);
        System.out.print("+" + tag + " ");
    }

    public void TagClose(String tag){
        System.out.print("-" + tag + " ");
        if (tags.size() == 0 || !tags.pop().equals(tag))
            // la pile est vide ou on ne ferme pas la dernière balise ouverte
            throw new RuntimeException("'"+tag+"' closes the wrong xml tag");
    }

    public void TagEmpty(String tag){
        System.out.print(tag+" ");
    }
:}

terminal String TAG_OPEN, TAG_EMPTY, TAG_CLOSING, CONTENT;

non terminal sgml;
non terminal tag, tag_open, content;

// définition d'un document sgml
sgml ::= tag;

// définition d'une balise : ouverture + content + fermeture, ou balise vide
tag ::= tag_open content TAG_CLOSING:t {: TagClose(t); :}
        | TAG_EMPTY:t {: TagEmpty(t); :};

// définition d'ouverture de balise
tag_open ::= TAG_OPEN:t {: TagOpen(t); :};

// définition de contenu : rien ou tags et/ou texte
content ::= | CONTENT:t content | tag content;
~~~~

### Variables

#### Deque<String> tags

La variable `tags` est une pile qui contient la liste des identifiants de balises ouvrants qui n'ont pas encore été fermées.

Chaque fois qu'on trouve une balise ouvrante, on rajoute son identifiant dans `tags` et, chaque fois qu'on trouve une balise fermante, on s'attend à ce qu'elle corresponde à la balise qui se trouve en haut de la pile.

### La syntaxe

#### Symbole non terminal de départ `sgml`

Le symbole `sgml` est symbole non terminal de départ. Sa définition est

~~~~ {.mathematica .numberLines startFrom="32"}
// définition d'un document sgml
sgml ::= tag;
~~~~

Cela veut dire qu'un document sgml est constitué d'un symbole du terminal `tag` (c.-à-d d'une balise au niveau racine).

#### Symbole non-terminal `tag`

Le symbole non-terminal `tag` correspond à la définition syntaxique de ce qui est une balise :

~~~~ {.mathematica .numberLines startFrom="35"}
// définition d'une balise : ouverture + content + fermeture, ou balise vide
tag ::= tag_open content TAG_CLOSING:t {: TagClose(t); :}
        | TAG_EMPTY:t {: TagEmpty(t); :};
~~~~

On comprend qu'un `tag` correspond soit à une balise vide (`TAG_EMPTY`), soit une balise complète (`tag_open content TAG_CLOSING`).

**Fonctions**

**`void TagClose(String tag)`**

La fonction `TagClose` est appelée pour chaque balise fermante trouvée. Elle imprime  l'identifiant de balise trouvée pré-fixée de `-`, et vérifie que bien que la balise fermante correspond à la dernière balise ouvrante trouvée. Si ce n'est pas le cas, une exception est levée.

*Code source :*

~~~~ {.java .numberLines startFrom="15"}
public void TagClose(String tag){
    System.out.print("-" + tag + " ");
    if (tags.size() == 0 || !tags.pop().equals(tag))
        // la pile est vide ou on ne ferme pas la dernière balise ouverte
        throw new RuntimeException("'"+tag+"' closes the wrong xml tag");
}
~~~~


**`void TagEmpty(String tag)`**

La fonction `TagEmpty` est appelée pour chaque balise auto-fermante trouvée. Elle imprime  l'identifiant de balise trouvée sans aucun préfixe.

*Code source :*

~~~~ {.java .numberLines startFrom="22"}
public void TagEmpty(String tag){
    System.out.print(tag+" ");
}
~~~~


#### Symbole non-terminal `tag_open`

Le symbole non-terminal `tag_open` correspond simplement au symbole terminal `TAG_OPEN`, c'est-à-dire, le symbole de la balise ouvrante.

~~~~ {.mathematica .numberLines startFrom="39"}
// définition d'ouverture de balise
tag_open ::= TAG_OPEN:t {: TagOpen(t); :};
~~~~

La définition de `TAG_OPEN` a sa propre règle syntaxique afin que la fonction correspondante (`TagOpen`) soit appelée dès que le symbole terminal est trouvé, et non pas lorsque toute la définition de la balise complète soit trouvée (voir définition de `tag` ci-dessus).


**Fonctions**

**`void TagOpen(String tag)`**

La fonction `TagOpen` est appelée pour chaque balise ouvrante trouvée. Elle rajoute l'identifiant de la balise trouvée dans la pile `tags` qui liste les balises ouvertes trouvées pas encore fermées. On imprime aussi l'identifiant de balise trouvée pré-fixée de `+`.

*Code source :*

~~~~ {.java .numberLines startFrom="10"}
public void TagOpen(String tag){
    tags.push(tag);
    System.out.print("+" + tag + " ");
}
~~~~


#### Symbole non-terminal `content`

Le symbole non-terminal `content` correspond à tous ce qui peut se trouver dans une balise.

Cela peut être : rien ($\varnothing$), du texte (symbole terminal `CONTENT`) et/ou une balise (`tag`).

~~~~ {.mathematica .numberLines startFrom="42"}
// définition de contenu : rien ou tags et/ou texte
content ::= | CONTENT:t content | tag content;
~~~~

## Tests

Dans le dossier *tests* il y a une liste de scripts tests. 

Chaque test est constitué d'un fichier *test* contient l'input à être parsé par le notre analyseur, ainsi qu'un fichier *output* contenant le résultat attendu pour l'analyse du fichier input en question :

**Script de test 1 :**

*Input :*

~~~~ {.sgml}
<A>
    <C/>
    <D t="1,23">
        tata
    </D>
</A>
~~~~

*Résultat attendu :*

~~~~ {}
+A C +D -D -A
~~~~


**Script de test 2 :**

*Input :*

~~~~ {.sgml}
<ABC id="13">
    <EMPTY/>
    <TEST>
        tata titi
    </TEST>
</ABC>
~~~~

*Résultat attendu :*

~~~~ {}
+ABC EMPTY +TEST -TEST -ABC
~~~~



**Script de test 3 :**

*Input :*

~~~~ {.sgml}
<ABC>
</A>
~~~~

*Résultat attendu :*

~~~~ {}
+ABC -A Parse error: java.lang.RuntimeException: 'A' closes the wrong xml tag
~~~~



### Exécution des tests

L'exécution des tests de l'analyseur SGML doit produire le résultat suivant si tous les tests sont exécutés correctement :

`Test 1:`**\textcolor{ForestGreen}{OK!}** `Résultat : +A C +D -D -A`

`Test 2:`**\textcolor{ForestGreen}{OK!}** `Résultat : +ABC EMPTY +TEST -TEST -ABC`

`Test 3:`**\textcolor{ForestGreen}{OK!}** `Résultat : +ABC -A Parse error: java.lang.RuntimeException: 'A' closes the wrong xml tag`

Pour plus d'informations, se référer au chapitre \ref{tests_target} *Target `Tests`*, section `Makefile`.

\newpage


# Deuxième partie : le SGML étendu

La deuxième partie de ce laboratoire consiste à étendre l'analyseur SGML développé dans la première partie afin de gérer des instructions supplémentaires de définition de SGML conditionnelle. On appellera ce format du *SGML étendu*.

Les instructions supplémentaires implémentées sont les suivantes :

  * définition de variables qui pourront être utilisées dans des blocs conditionnels pour des sections du document SGML

    Exemple de définition de variable `debug` avec valeur `true`:

    ~~~~{.sgml}
    <#set debug="true"/>
    ~~~~

  * blocs conditionnels dont le contenu n'est intégré dans le document que si la condition exprimée est valide. Peuvent être imbriqués récursivement.

    Exemple de définition de bloc qui est considéré uniquement si la variable `test` à la valeur `1`:

    ~~~~{.sgml}
    <#if test=="1">
        ...
    <#if/>
    ~~~~

Un document SGML étendu est considéré valide s'il respecte les règles suivantes en plus de celles d'un document SGML (chapitre *\ref{validateur_SGML} Première partie : le validateur SGML*):

  * le document peut optionnellement commencer par une liste de balises de definition de variable de la forme `<#set var="value"/>` où `var` est le nom de la variable à définir et `value` la valeur à assigner à la variable. Le nom de variable (`var`) doit être formé de 1 ou plus caractères alphabétiques en majuscule ou minuscule, et la valeur (`value`) se trouve entre guillemets (`"`), et est composée de tout caractère sauf le guillemet, sans restriction sur la longueur.

  * des blocs conditionnels peuvent être définis dans le document avec deux parties : la balise d'ouverture de bloc conditionnel de la forme `<#if var=="value">` et la balise de fermeture de bloc conditionnel de la forme `<#if/>`. A l'intérieur du bloc conditionnel, il peut y avoir optionnellement du texte, une liste de balises (récursivement), ou encore des nouveaux blocs conditionnels.

Cas particulier : un document SGML valide est un document SGML étendu valide.

## Exemples

L'input suivant a deux blocs conditionnels selon la valeur de la variable `test`, et on définit la valeur de cette variable à `true`:

~~~~ {.sgml}
<#set test="true"/>
<C>
    <#if test=="true">
        tata
    <#if/>
    <#if test=="false">
        titi
    <#if/>
</C>
~~~~

Après la passe de l'analyseur, le résultat produit contiendra le bloc conditionnel dont la condition est vraie `test=="true"`, mais le bloc dont la condition est fausse `test=="false"`:

~~~~ {.sgml}
<C>
    tata
</C>
~~~~

Si on change la valeur de la variable `test` à `false`, cela changera le bloc conditionnel qui sera pris en considération :

~~~~ {.sgml}
<#set test="false"/>
<C>
    <#if test=="true">
        tata
    <#if/>
    <#if test=="false">
        titi
    <#if/>
</C>
~~~~

Produit le résultat :

~~~~ {.sgml}
<C>
    titi
</C>
~~~~



## Code source

Le code source de cette première partie du projet réside dans le dossier `labo2.b` et contient les fichiers et dossiers suivants :

  * **sgml.flex :** fichier JFlex qui détermine le lexique SGML étendu.
  * **sgml.cup :** fichier CUP qui détermine la syntaxe SGML étendu.
  * **tests :** dossier contenant les fichiers tests et leurs résultats attendus.
  * **Test.java :** fichier Java qui utilise le parser réalisé pour analyser un fichier donnée en paramètre.
  * **Makefile :** fichier Makefile qui réalise la compilation du projet et lance les tests.
  * **bin, lib :** dossiers contenant des librairies et exécutables CUP et JFlex.

Les fichiers `Test.java` et `Makefile`, ainsi que les dossiers `bin, lib`, sont les mêmes que dans le dossier `labo2.a` correspondant à la première partie de ce laboratoire.

## Analyseur lexical

Le fichier JFlex `sgml.flex` dans le projet décrit les règles lexicales qu'un fichier SGML étendu doit respecter. Son contenu est le suivant :

~~~~ {.lex .numberLines}
import java_cup.runtime.*;
import java.util.Vector;
import java.util.regex.Pattern;

%%
%class SGML
%line
%column
%cup

%{
    // caractères spéciaux délimitant les balises
    Pattern pattern = Pattern.compile("(^</?|/?>$)");

    // Retourne les différentes parties d'une balise : id et attributs
    public String[] splitTag(String fullTag){
        fullTag = fullTag.trim();
        fullTag = pattern.matcher(fullTag).replaceAll("");
        return fullTag.split(" ");
    }

    // Retourne l'identifiant d'une balise d'après sa définition
    public String tagName(String fullTag) {
        String[] parts = splitTag(fullTag);
        return parts[0];
    }

    // Retourne le premier attribut d'une balise
    public String attribut(String fullTag) {
        String[] parts = splitTag(fullTag);
        return parts[1];
    }
%}

word = [A-Za-z]+
attribut = {word}=\"[^\"]*\"
set_tag = \<#set\s+{attribut}\s*\/\>
if_tag_start = \<#if\s+{word}==\"[^\"]*\"\s*\>
if_tag_end = \<#if\s*\/\>
tag_new_partial = \<\s*{word}(\s+{attribut})*\s*
tag_open = {tag_new_partial}\>
tag_empty = {tag_new_partial}\/\>
tag_closing = \<\/\s*{word}\s*\>
blank = [\s|\t|\n|\r]+
content = [^\<\n\r\t]+
%%

// balise de SGML étendu pour définir la valeur d'une variable
{set_tag}       { return new Symbol(sym.SET_TAG, attribut(yytext())); }

// balise de SGML étendu de début de bloc conditionnel
{if_tag_start}  { return new Symbol(sym.IF_TAG_START, attribut(yytext())); }

// balise de SGML étendu de fin de bloc conditionnel
{if_tag_end}    { return new Symbol(sym.IF_TAG_END); }

// balise d'ouverture
{tag_open}      { return new Symbol(sym.TAG_OPEN, new String[]{tagName(yytext()), yytext()}); }

// balise vide
{tag_empty}     { return new Symbol(sym.TAG_EMPTY, yytext()); }

// balise de fermeture
{tag_closing}   { return new Symbol(sym.TAG_CLOSING, new String[]{tagName(yytext()), yytext()}); }

// du texte à l'intérieur des balises (data)
{content}       { if (yytext().trim().length()>0) return new Symbol(sym.CONTENT, yytext().trim()); }

// caractères blancs ignorés
{blank}         {;}
~~~~

### Fonctions

#### String[] splitTag(String fullTag)

La fonction `splitTag` reçoit en paramètre une balise trouvée - ouverture de balise, fermeture, ou balise vide - et retourne les différentes parties de la balise : identifiant et attributs.

*Code source :*

~~~~ {.java .numberLines startFrom="12"}
// caractères spéciaux délimitant les balises
Pattern pattern = Pattern.compile("(^</?|/?>$)");

// Retourne les différentes parties d'une balise : id et attributs
public String[] splitTag(String fullTag){
    fullTag = fullTag.trim();
    fullTag = pattern.matcher(fullTag).replaceAll("");
    return fullTag.split(" ");
}
~~~~


#### String tagName(String fullTag)

La fonction `tagName` retourne l'identifiant d'une balise.

*Code source :*

~~~~ {.java .numberLines startFrom="22"}
// Retourne l'identifiant d'une balise d'après sa définition
public String tagName(String fullTag) {
    String[] parts = splitTag(fullTag);
    return parts[0];
}
~~~~

#### String attribut(String fullTag)

La fonction `attribut` retourne le premier attribut d'une balise, utilisé pour les balises SGML étendu pour identifier les variables et leur valeur pour les balises `#set` et `#if`.

*Code source :*

~~~~ {.java .numberLines startFrom="22"}
// Retourne le premier attribut d'une balise
public String attribut(String fullTag) {
    String[] parts = splitTag(fullTag);
    return parts[1];
}
~~~~

### Expressions
Le fichier flex est similaire à celui réalisé dans la première partie du laboratoire. Les lignes `48-55` sont nouvelles et gèrent les nouvelles balises introduites dans SGML étendu. Voici leur signification :

#### Expression `{seattendandt_tag}`

Cette expression trouve les balises du document qui définissent les variables SGML étendu.

~~~~ {.java .numberLines startFrom="38"}
// balise de SGML étendu pour définir la valeur d'une variable
{set_tag}       { return new Symbol(sym.SET_TAG, attribut(yytext())); }
~~~~

**Expression régulière**

Son expression régulière est la suivante : \color{Red}`<#set`\color{Brown}`\s+`\color{Purple}`[A-Za-z]+`\color{Blue}`="`\color{ForestGreen}`[^"]*`\color{Blue}`"`\color{Brown}`\s*`\color{Red}`/>`\color{black}

La partie de l'expression \color{Red}`<#set`\color{Brown}`\s+`\color{black}...\color{Brown}`\s*`\color{Red}`/>` \color{black}délimite ce type de balise, qui doit commencer par \color{Red}`<#set` \color{black}, suivie d'au moins un espace (\color{Brown}`\s+`\color{black}) et se terminer par \color{Red}`/>`\color{black}, optionnellement préfixé par des espaces \color{Brown}`\s*`\color{black}.

La partie violette \color{Purple}`[A-Za-z]+` \color{Black} dit que la balise doir forcément contenir l'identifiant de la variable à définir, que cet identifiant se compose de lettres majuscules et minuscules et doit être de longueur supérieure ou égale à $1$.

La partie en vert \color{ForestGreen}`[^"]*` \color{Black} dit que la valeur de la variable n'a pas de longueur minimale et peut être constituée de n'importe quel caractère sauf le guillemet `"`.

**Exemple :**

Pour l'input suivant :

~~~~ {.sgml}
<#set test="false"/>
<#set debug="true"/>
<APP>
  <#if debug=="true">
      <DEBUG enabled="ON">
          This is debug mode!
      </DEBUG>
  <#if/>
  <#if debug=="false">
      <main/>
  <#if/>
</APP>
~~~~

Notre expression `{set_tag}` va trouver les correspondances suivantes :

  *  `<#set test="false"/>`
  *  `<#set debug="true"/>`


**Symbole CUP**

Pour chaque expression `{set_tag}` trouvée, un symbole CUP `SET_TAG` est instancié avec la définition de variable trouvée (exemple : `debug="true"`).


#### Expression `{if_tag_start}`

Cette expression trouve les balises d'ouverture de bloc conditionnel.

~~~~ {.java .numberLines startFrom="51"}
// balise de SGML étendu de début de bloc conditionnel
{if_tag_start}  { return new Symbol(sym.IF_TAG_START, attribut(yytext())); }
~~~~

**Expression régulière**


Son expression régulière est la suivante : \color{Red}`<#if`\color{Brown}`\s+`\color{Purple}`[A-Za-z]+`\color{Blue}`=="`\color{ForestGreen}`[^"]*`\color{Blue}`"`\color{Brown}`\s*`\color{Red}`>`\color{black}.

Très semblable dans son expression à `{set_tag}`, elle doit commencer par \color{Red}`<#if` \color{black}, séparateur entre la variable et la valeur à tester est le double égal \color{Blue}`=="` \color{black} et  la balise doit se terminer par \color{Red}`>`\color{black}.

**Exemple :**

Pour l'input suivant :

~~~~ {.sgml}
<#set test="false"/>
<#set debug="true"/>
<APP>
  <#if debug=="true">
      <DEBUG enabled="ON">
          This is debug mode!
      </DEBUG>
  <#if/>
  <#if debug=="false">
      <main/>
  <#if/>
</APP>
~~~~

Notre expression `{if_tag_start}` va trouver les correspondances suivantes :

  *  `<#if debug=="true">`
  *  `<#if debug=="false">`


**Symbole CUP**

Pour chaque expression `{if_tag_start}` trouvée, un symbole CUP `IF_TAG_START` est instancié avec la définition de comparaison trouvée (exemple : `debug=="true"`).



#### Expression `{if_tag_end}`

Cette expression trouve les balises de fermeture de bloc conditionnel.

~~~~ {.java .numberLines startFrom="54"}
// balise de SGML étendu de fin de bloc conditionnel
{if_tag_end}    { return new Symbol(sym.IF_TAG_END); }
~~~~

**Expression régulière**


Son expression régulière est la suivante : \color{Red}`<#if`\color{Brown}`\s+`\color{Red}`/>`\color{black}.

Elle doit commencer par \color{Red}`<#if` \color{black}, se terminer par \color{Red}`/>`\color{black}, avec optionnellement des espaces à l'intérieur \color{Brown}`\s+`\color{black}.

**Exemple :**

Pour l'input suivant :

~~~~ {.sgml .numberLines}
<#set test="false"/>
<#set debug="true"/>
<APP>
  <#if debug=="true">
      <DEBUG enabled="ON">
          This is debug mode!
      </DEBUG>
  <#if/>
  <#if debug=="false">
      <main/>
  <#if/>
</APP>
~~~~

L'expression `{if_tag_end}` va trouver les correspondances suivantes :

  *  `<#if/>` (ligne 8)
  *  `<#if/>` (ligne 11)


**Symbole CUP**

Pour chaque expression `{if_tag_end}` trouvée, un symbole CUP `IF_TAG_END` est instancié sans paramètre.


## Analyseur syntaxique

Le fichier CUP `sgml.cup` dans le projet décrit les règles syntaxiques qu'un fichier SGML étendu doit respecter. Voici son contenu :

~~~~ {.java .numberLines}
import java_cup.runtime.*;
import java.util.Vector;
import java.io.*;
import java.util.*;

parser code {:
    // dictionnaire variable -> valeur des variables SGML étendu définies
    Map<String, String> variables = new HashMap<String, String>();

    // Pile des balises ouvrantes attendant fermeture
    Deque<String> tags = new ArrayDeque<String>();

    // est-que que le bloc conditionnel actuel doit être imprimé ?
    Deque<Boolean> validIfStack = new ArrayDeque<Boolean>();

    // retourne l'information si on se trouve dans un bloc valide
    public boolean isInValidif()
    {
        for(boolean b : validIfStack) if(!b) return false;
        return true;
    }

    // imprime une ligne trouvée, avec la bonne indentation
    // seulement si on est dans un bloc valide
    public void printLine(String line){
        if (!isInValidif())
            return;
        System.out.print(new String(new char[tags.size()]).replace("\0", "    "));
        System.out.println(line);
    }

    // nouvelle balise d'ouverture trouvée
    public void TagOpen(String[] tag){
        String tagName = tag[0];
        String fullTag = tag[1];

        if (!isInValidif())
            return;
        printLine(fullTag);
        tags.push(tagName);
    }

    // nouvelle balise de fermeture trouvée
    public void TagClose(String[] tag){
        String tagName = tag[0];
        String fullTag = tag[1];

        if (!isInValidif())
            return;
        if (tags.size() == 0 || !tags.pop().equals(tagName))
            throw new RuntimeException("'"+tagName+"' closes the wrong xml tag");
        printLine(fullTag);
    }

    // balise de définition de variable trouvé
    public void setTag(String set){
        String[] keyVal = set.split("=");
        variables.put(keyVal[0], keyVal[1]);
    }

    // balise de début de bloc conditionnel
    public void IfStart(String condition){
        String[] keyVal = condition.split("==");
        String value =  variables.get(keyVal[0]);

        // on empile l'information sur la validité du bloc
        // le bloc est valide si la variable a été définie ET définie avec la même valeur
        validIfStack.push(new Boolean(value != null && value.equals(keyVal[1])));
    }

    // balise de fin de bloc conditionnel
    public void IfEnd(){
        //on sort du bloc conditionnel
        validIfStack.pop();
    }
:}

terminal String[] TAG_OPEN, TAG_CLOSING;
terminal String CONTENT, SET_TAG, IF_TAG_START, TAG_EMPTY, IF_TAG_END;

non terminal sgml, tag, tag_open, content;
non terminal set_tags, if_start, if_end;

// définition d'un document sgml étendu
sgml ::= set_tags tag;

// définition d'une liste de balises de définition de variable
set_tags ::= | SET_TAG:t set_tags {: setTag(t); :};

// définition d'une balise : ouverture + content + fermeture, ou balise vide
tag ::= tag_open content TAG_CLOSING:t {: TagClose(t); :}
        | TAG_EMPTY:t {: printLine(t); :};

// définition d'ouverture de balise
tag_open ::= TAG_OPEN:t {:TagOpen(t); :};

// définition de contenu : rien, tags, texte ou bloc conditionnel
content ::= | CONTENT:t content {: printLine(t); :}
            | tag content
            | if_start content if_end content;

// balise d'ouverture de bloc conditionnel
if_start ::= IF_TAG_START:t  {: IfStart(t); :};

// balise de fermeture de bloc conditionnel
if_end ::= IF_TAG_END:t  {: IfEnd(); :};


~~~~

### Variables

#### Deque<String> tags

Comme pour la première partie du laboratoire, cette variable contient la pile de balises ouvertes et pas encore fermées.

#### Map<String, String> variables

La variable `variables` contient toutes les variables définies par les instructions `#set` dans le document SGML étendu. Ces valeurs sont utiles pour vérifier les conditions des blocs `#if`.


#### Deque<Boolean> validIfStack

La variable `validIfStack` contient la pile des résultats d'évaluation des blocs conditionnels dans lesquels le programme se trouve à tout instant. Cette information est nécessaire pour savoir si le contenu analysé à tout instant est dans un bloc conditionnel valide ou pas. Cette valeur est une pile et non pas un scalaire car on gère les blocs conditionnels récursifs.


### La syntaxe

#### Symbole non terminal de départ `sgml`

~~~~ {.mathematica .numberLines startFrom="84"}
// définition d'un document sgml étendu
sgml ::= set_tags tag;
~~~~

On spécifie que le document SGML étendu est constitué d'une liste de balises de définition de variables (`set_tags`) suivie d'une balise SGML (`tag`).

#### Symbole non terminal `set_tags`

~~~~ {.mathematica .numberLines startFrom="87"}
// définition d'une liste de balises de définition de variable
set_tags ::= | SET_TAG:t set_tags {: setTag(t); :};
~~~~

La liste de balises de définition de variables (`set_tags`) est une liste de symboles terminaux `SET_TAG`. Cette liste n'a pas de contrainte de taille : $|list| \in [0; \infty[$

**Fonctions**

**`void setTag(String set)`**

La fonction `setTag` est appelée pour chaque balise de définition de variable trouvée (symbole terminal ` SET_TAG`). Cette fonction enregistre dans le dictionnaire `variables` la variable à définir et sa valeur.

*Code source :*

~~~~ {.java .numberLines startFrom="15"}
// balise de définition de variable trouvé
public void setTag(String set){
    String[] keyVal = set.split("=");
    variables.put(keyVal[0], keyVal[1]);
}
~~~~


#### Symbole non terminal `tag`

Le symbole non-terminal `tag` correspond à la définition syntaxique de ce qui est une balise, exactement comme expliqué précédemment pour l'analyseur SGML, un `tag` correspond soit à une balise vide (`TAG_EMPTY`), soit une balise complète (`tag_open content TAG_CLOSING`).

~~~~ {.mathematica .numberLines startFrom="90"}
// définition d'une balise : ouverture + content + fermeture, ou balise vide
tag ::= tag_open content TAG_CLOSING:t {: TagClose(t); :}
        | TAG_EMPTY:t {: printLine(t); :};
~~~~


**Fonctions**

**`void TagClose(String[] tag)`**

La fonction `TagClose` est appelée pour chaque balise fermante trouvée. Elle vérifie  que la balise fermante correspond à la dernière balise ouvrante trouvée et l'imprime. Si ce n'est pas le cas, une exception est levée.

*Code source :*

~~~~ {.java .numberLines startFrom="43"}
// nouvelle balise de fermeture trouvée
public void TagClose(String[] tag){
    String tagName = tag[0];
    String fullTag = tag[1];

    if (!isInValidif())
        return;
    if (tags.size() == 0 || !tags.pop().equals(tagName))
        throw new RuntimeException("'"+tagName+"' closes the wrong xml tag");
    printLine(fullTag);
}
~~~~

**`void printLine(String line)`**

La fonction `printLine` est appelée pour chaque balise vide trouvée, mais aussi pour chaque ligne trouvée à imprimer. Cette fonction vérifie d'abord que l'analyseur ne se trouve pas dans un bloc conditionnel non valide, ensuite elle imprime la ligne, avec une indentation qui dépend du nombre de balises actuellement ouvertes.

*Code source :*

~~~~ {.java .numberLines startFrom="43"}
// imprime une ligne trouvée, avec la bonne indentation
// seulement si on est dans un bloc valide
public void printLine(String line){
    if (!isInValidif())
        return;
    System.out.print(new String(new char[tags.size()]).replace("\0", "    "));
    System.out.println(line);
}
~~~~



#### Symbole non terminal `tag_open`

Le symbole non-terminal `tag_open` correspond à la définition syntaxique d'une balise ouvrante.

~~~~ {.mathematica .numberLines startFrom="94"}
// définition d'ouverture de balise
tag_open ::= TAG_OPEN:t {:TagOpen(t); :};
~~~~


**Fonctions**

**`void TagOpen(String[] tag)`**

La fonction `TagOpen` est appelée pour chaque balise ouvrante trouvée. Si l'analyseur se trouve dans un bloc conditionnel valide, il imprime la balise et la rajoute dans la pile de tags ouverts à fermer `tags`.

*Code source :*

~~~~ {.java .numberLines startFrom="32"}
// nouvelle balise d'ouverture trouvée
public void TagOpen(String[] tag){
    String tagName = tag[0];
    String fullTag = tag[1];

    if (!isInValidif())
        return;
    printLine(fullTag);
    tags.push(tagName);
}
~~~~


#### Symbole non terminal `content`

Le symbole non-terminal `content` correspond à la définition syntaxique du contenu qui se trouve à l'intérieur d'une balise.

~~~~ {.mathematica .numberLines startFrom="97"}
// définition de contenu : rien, tags, texte ou bloc conditionnel
content ::= | CONTENT:t content {: printLine(t); :}
            | tag content
            | if_start content if_end content;
~~~~

La nouveauté est `if_start content if_end content` qui spécifie qu'on accepte ici des blocs conditionnels, récursivement.

#### Symbole non terminal `if_start`

Le symbole non-terminal `if_start` correspond à la balise d'ouverture de bloc conditionnelle, soit le symbole terminal `IF_TAG_START`.

~~~~ {.mathematica .numberLines startFrom="102"}
// balise d'ouverture de bloc conditionnel
if_start ::= IF_TAG_START:t  {: IfStart(t); :};
~~~~


**Fonctions**

**`void IfStart(String condition)`**

La fonction `IfStart` est appelée pour chaque `IF_TAG_START` trouvé. Cette fonction va évaluer la condition du bloc initial, en comparant la valeur de la variable (définie dans `variables`) avec la valeur comparée dans le bloc conditionnel. Le résultat de la comparaison est stocké dans la pile des évaluations de bloc conditionnel courants.

*Code source :*

~~~~ {.java .numberLines startFrom="61"}
// balise de début de bloc conditionnel
public void IfStart(String condition){
    String[] keyVal = condition.split("==");
    String value =  variables.get(keyVal[0]);

    // on empile l'information sur la validité du bloc
    // le bloc est valide si la variable a été définie ET définie avec la même valeur
    validIfStack.push(new Boolean(value != null && value.equals(keyVal[1])));
}
~~~~


#### Symbole non terminal `if_end`

Le symbole non-terminal `if_end` correspond à la balise de fermeture de bloc conditionnelle, soit le symbole terminal `IF_TAG_END`.

~~~~ {.mathematica .numberLines startFrom="105"}
// balise de fermeture de bloc conditionnel
if_end ::= IF_TAG_END:t  {: IfEnd(); :};
~~~~

**Fonctions**

**`void IfEnd()`**

La fonction `IfEnd` est appelée pour chaque `IF_TAG_END` trouvé. Cette fonction va dépiler la dernière valeur de la pile d'évaluations de bloc conditionnel courants.

*Code source :*

~~~~ {.java .numberLines startFrom="71"}
// balise de fin de bloc conditionnel
public void IfEnd(){
    //on sort du bloc conditionnel
    validIfStack.pop();
}
~~~~


## Tests

Dans le dossier *tests* il y a une liste de scripts tests, chacun composé de l'input à tester et du résultat attendu:


**Script de test 1 :**

*Input :*

~~~~ {.sgml}
<#set id="12"/>
<C>
    <#if id=="13">
        tata
    <#if/>
    <#if id=="12">
        titi
    <#if/>
</C>
~~~~

*Résultat attendu :*

~~~~ {.sgml}
<C>
    titi
</C>
~~~~


**Script de test 2 :**

*Input :*

~~~~ {.sgml}
<#set test="false"/>
<#set debug="true"/>
<APP>
    <#if debug=="true">
        <DEBUG enabled="ON">
            This is debug mode!
        </DEBUG>
        <#if test=="true">
            Test mode: YES
        <#if/>
        <#if test=="false">
            Test mode: NO
        <#if/>
    <#if/>
    <#if debug=="false">
        <main/>
    <#if/>
</APP>
~~~~

*Résultat attendu :*

~~~~ {.sgml}
<APP>
    <DEBUG enabled="ON">
        This is debug mode!
    </DEBUG>
    Test mode: NO
</APP>
~~~~


**Script de test 3 :**

*Input :*

~~~~ {.sgml}
<#set test="false"/>
<#set debug="false"/>
<APP>
    <#if debug=="true">
        <DEBUG enabled="ON">
            This is debug mode!
        </DEBUG>
        <#if test=="true">
            Test mode: YES
        <#if/>
        <#if test=="false">
            Test mode: NO
        <#if/>
    <#if/>
    <#if debug=="false">
        <main/>
    <#if/>
</APP>
~~~~

*Résultat attendu :*

~~~~ {.sgml}
<APP>
    <main/>
</APP>
~~~~

### Exécution des tests

L'exécution des tests de l'analyseur SGML étendu doit produire le résultat suivant si tous les tests sont exécutés correctement :

`Test 1:`**\textcolor{ForestGreen}{OK!}**
```
Résultat :
<C>
    titi
</C>

```

`Test 2:`**\textcolor{ForestGreen}{OK!}**
```
Résultat :
<APP>
    <DEBUG enabled="ON">
        This is debug mode!
    </DEBUG>
    Test mode: NO
</APP>

```

`Test 3:`**\textcolor{ForestGreen}{OK!}**
```
Résultat :
<APP>
    <main/>
</APP>
```

Pour plus d'informations, se référer au chapitre \ref{tests_target} *Target `Tests`*, section `Makefile`.


\newpage

# Compilation et exécution

Dans ce chapitre on décrit comment compiler les analyseurs et exécuter les scripts de tests.


## Dépendances

Le projet développé dépend de CUP et JFlex. Afin de faciliter l'exécution du code, chaque analyseur contient les dossiers **bin, lib** qui contiennent les exécutables et libraires de CUP et JFlex.

## Tests \label{tests}

Chaque analyseur contient un dossier **tests**. Dans ce dossier, plusieurs fichiers des test existent. Chaque test est composé de deux fichiers : un fichier de test (exemple : *test1.txt*) et un fichier de résultat attendu (exemple : *output1.txt*).

Lors de l'exécution des tests (cf chapitre \ref{tests_target} *Target `Tests`*, section `Makefile`), chaque fichier de test est exécuté et son résultat comparé avec le résultat prévu pour ce test.

**Exemple de résultat des tests :**

`Test 1:`**\textcolor{ForestGreen}{OK!}** `Résultat : +A C +D -D -A`

`Test 2:`**\textcolor{ForestGreen}{OK!}** `Résultat : +ABC EMPTY +TEST -TEST -ABC`

`Test 3:`**\textcolor{ForestGreen}{OK!}**`Résultat : +ABC -A Parse error: java.lang.RuntimeException: 'A' closes the wrong xml tag`


Chaque test montre le résultat en couleur. **\textcolor{ForestGreen}{OK!}** si le test à réussit, ou **\textcolor{Red}{Failed!}** si le test à échoué.

Un test réussit est un test qui produit le même résultat que celui prévu dans le fichier *output.txt* rattaché au test concerné.


## `Test.java`

Un fichier `Test.java` existe pour chaque analyseur. Ce fichier vise à être compilé en exécutable afin de tester notre analyseur avec un fichier passé en paramètre. Il va essayer d'analyser le contenu du fichier passé en paramètre. S'il échoue, une exception explicite est alors levée.

*Code source :*

~~~~ {.java .numberLines}
import java.util.Vector;
import java.io.FileReader;

public class Test {

  public static void main(String[] arg) {
    try {

      // Lecture du fichier passé en paramètre
      FileReader myFile = new FileReader(arg[0]);
      // instancier un Lexer sur le contenu de fichier
      SGML myLex = new SGML(myFile);
      // instanciation du parser sur le lexer
      parser myP = new parser(myLex);

      try {
        myP.parse();
      } catch (Exception e) {
        //le contenu du fichier est incorrect
        System.out.println("Parse error: " + e.toString());
      }

    } catch (Exception e) {
      //le contenu du fichier est incorrect
      System.out.println("Invalid file: " + e.toString());
    }
  }
}

~~~~

## `Makefile`

Chaque analyseur a un fichier `Makefile` qui permet de compiler le projet et lancer les tests.

*Code source :*

~~~~ {.Makefile .numberLines}

all : clean build tests

build : sym.class parser.class SGML.class Test.class

sym.java parser.java : sgml.cup
	java -jar lib/java-cup-11b.jar sgml.cup

%.class : %.java
	javac -classpath lib/java-cup-11b.jar:. $<

SGML.java: sgml.flex
	bin/jflex sgml.flex

clean:
	rm -rf *.class *~ parser.java SGML.java sym.java

tests : Test.class
	./tests/run_tests.sh 3

~~~~


### Target `clean`

Utilisez le paramètre `clean` pour effacer les fichiers créés par une compilation précédente :

~~~~ {}
$ make clean
rm -rf *.class *~ parser.java SGML.java sym.java
~~~~

### Target `build` \label{build_target}

Utilisez le paramètre `build` pour compiler le projet :

~~~~ {}
$ make build
java -jar lib/java-cup-11b.jar sgml.cup
------- CUP v0.11b 20160615 (GIT 4ac7450) Parser Generation Summary -------
  0 errors and 0 warnings
  6 terminals, 4 non-terminals, and 8 productions declared,
  producing 13 unique parse states.
  0 terminals declared but not used.
  0 non-terminals declared but not used.
  0 productions never reduced.
  0 conflicts detected (0 expected).
  Code written to "parser.java", and "sym.java".
---------------------------------------------------- (CUP v0.11b 20160615 (GIT 4ac7450))
javac -classpath lib/java-cup-11b.jar:. sym.java
javac -classpath lib/java-cup-11b.jar:. parser.java
bin/jflex sgml.flex
Reading "sgml.flex"
Constructing NFA : 98 states in NFA
Converting NFA to DFA :
...................
21 states before minimization, 19 states in minimized DFA
Writing code to "SGML.java"
javac -classpath lib/java-cup-11b.jar:. SGML.java
javac -classpath lib/java-cup-11b.jar:. Test.java
Note: Test.java uses or overrides a deprecated API.
Note: Recompile with -Xlint:deprecation for details.
~~~~

### Target `tests` \label{tests_target}

Utilisez le paramètre `tests` pour exécuter les tests :

~~~~ {}
$ make tests
./tests/run_tests.sh 3
~~~~

`Test 1:`**\textcolor{ForestGreen}{OK!}** `Résultat : +A C +D -D -A`

`Test 2:`**\textcolor{ForestGreen}{OK!}** `Résultat : +ABC EMPTY +TEST -TEST -ABC`

`Test 3:`**\textcolor{ForestGreen}{OK!}** `Résultat : +ABC -A Parse error: java.lang.RuntimeException: 'A' closes the wrong xml tag`

Le résultat de l'exécution des tests est affiché en colleur pour pouvoir rapidement identifier les tests échoués.

Pour plus d'informations sur les tests, voir le chapitre *\ref{tests} Tests*

### Target par défaut

Exécutez la commande `make` sans paramètre pour :

  #. *effacer* les fichiers créés pas une compilation précédente
  #. *compiler* les fichiers
  #. exécuter les *tests*

~~~~{}
$ make
rm -rf *.class *~ parser.java SGML.java sym.java
java -jar lib/java-cup-11b.jar sgml.cup
------- CUP v0.11b 20160615 (GIT 4ac7450) Parser Generation Summary -------
  0 errors and 0 warnings
  6 terminals, 4 non-terminals, and 8 productions declared,
  producing 13 unique parse states.
  0 terminals declared but not used.
  0 non-terminals declared but not used.
  0 productions never reduced.
  0 conflicts detected (0 expected).
  Code written to "parser.java", and "sym.java".
---------------------------------------------------- (CUP v0.11b 20160615 (GIT 4ac7450))
javac -classpath lib/java-cup-11b.jar:. sym.java
javac -classpath lib/java-cup-11b.jar:. parser.java
bin/jflex sgml.flex
Reading "sgml.flex"
Constructing NFA : 98 states in NFA
Converting NFA to DFA :
...................
21 states before minimization, 19 states in minimized DFA
Writing code to "SGML.java"
javac -classpath lib/java-cup-11b.jar:. SGML.java
javac -classpath lib/java-cup-11b.jar:. Test.java
Note: Test.java uses or overrides a deprecated API.
Note: Recompile with -Xlint:deprecation for details.
./tests/run_tests.sh 3
~~~~

`Test 1:`**\textcolor{ForestGreen}{OK!}** `Résultat : +A C +D -D -A`

`Test 2:`**\textcolor{ForestGreen}{OK!}** `Résultat : +ABC EMPTY +TEST -TEST -ABC`

`Test 3:`**\textcolor{ForestGreen}{OK!}** `Résultat : +ABC -A Parse error: java.lang.RuntimeException: 'A' closes the wrong xml tag`



# Références

[^1]: @enonce
[^2]: @wiki:SGML
[^3]: @jflex
[^4]: @cup