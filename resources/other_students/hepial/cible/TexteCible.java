package cible;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Author   :   Joao Mendes
 * Date     :   03.05.16.
 * Version  :   0.0.1
 */
public class TexteCible
{
    private static final TexteCible instance = new TexteCible();
    private final List<String> text;
    private final HashMap<String, Path> fileHashMap;
    private OutputStream currentClass;

    private TexteCible()
    {
        this.text = new ArrayList<>();
        this.fileHashMap = new HashMap<>();
        this.currentClass = null;
    }

    /**
     * Acces au singleton
     *
     * @return Le singleton de TDS
     */
    public static TexteCible getInstance()
    {
        return instance;
    }

    /**
     * Ajout dans la liste et ecrit dans le fichier de sortie
     *
     * @param text Ligne à ecrire
     */
    public void add(String text)
    {
        this.text.add(text);

        try
        {
            this.currentClass.write((text).getBytes());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Pareil que add() mais effectue un retour à la ligne apres
     *
     * @param text Le texte à ajouter
     */
    public void addLine(String text)
    {
        this.add(text + System.getProperty("line.separator"));
    }

    /**
     * Change de classe.
     *
     * @param className Nom de la classe dans laquelle aller.
     * @throws IOException Si il y a une erreur lors de la création du fichier pour la nouvelle classe.
     */
    public void changeClass(String className) throws IOException
    {
        Path newCurrentFile = this.fileHashMap.get(className);

        if (newCurrentFile == null)
            throw new NoSuchElementException("Class not exist");
        else
        {
            this.currentClass.close();
            this.currentClass = Files.newOutputStream(newCurrentFile, StandardOpenOption.APPEND);
        }
    }

    /**
     * Crée un nouveau fichier pour la nouvelle classe et ecrit dedans jusqu'a un changement de classe.
     *
     * @param className   Nom de la nouvelle classe
     * @param publicClass Si la classe est public ou pas.
     */
    public void newClass(String className, boolean publicClass)
    {
        this.newClass(className, publicClass, "java/lang/Object");
    }

    /**
     * Crée un nouveau fichier pour la nouvelle classe et ecrit dedans jusqu'a un changement de classe.
     *
     * @param className   Nom de la classe
     * @param publicClass Si la classe est pubil ou pas
     * @param superClasse Le nom de la classe qui est heritée
     */
    public void newClass(String className, boolean publicClass, String superClasse)
    {
        this.fileHashMap.put(className, Paths.get(className + ".out"));

        try
        {
            if (this.currentClass != null)
                this.currentClass.close();
            this.currentClass = Files.newOutputStream(this.fileHashMap.get(className));
        }
        catch (IOException ex)
        {
            System.err.println(ex.getMessage());
        }

        String visibility;
        if (publicClass)
            visibility = "public";
        else
            visibility = "";

        this.addLine(".class " + visibility + " " + className + "\n");
        this.addLine(".super " + superClasse + "\n");
        this.addLine("\n");

        //        this.addStdInit();
    }

    /**
     * Ajoute l'appel au constructeur de l'objet Object de java
     */
    public void addStdInit()
    {
        this.addLine(".method public <init>()V\n" +
                "   aload_0\n" +
                "   invokenonvirtual java/lang/Object/<init>()V\n" +
                "   return\n" +
                ".end method\n\n");
    }
}
