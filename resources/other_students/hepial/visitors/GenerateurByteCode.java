package visitors;

import abs.*;
import cible.TexteCible;
import errors.Errors;
import tds.SymConst;
import tds.SymFunc;
import tds.TDS;
import type.*;

import java.util.ArrayList;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class GenerateurByteCode implements Visiteur
{
    private static final Errors errors = Errors.getInstance();
    private static final GenerateurByteCode ourInstance = new GenerateurByteCode();
    private static final TexteCible cible = TexteCible.getInstance();
    private static int indentNb = 0;        // Variable pour effectuer des indetation dans le fichier .j
    private static int varId = 1;           // Identifiant des variables
    private static int loopId = 0;          // Identifiant des boucles pour avoir des labels avec des noms différents
    private static int errorId = 0;         // Identifiant pour les labels error
    private static int relationLabNb = 0;   // Identifiant pour les labels des relations
    private static int condNb = 0;          // Identifiant pour les labels des conditions
    private static String progName = "";    // Nom du programme pour l'appel des méthodes.

    private GenerateurByteCode()
    {

    }

    public static GenerateurByteCode getInstance()
    {
        return ourInstance;
    }

    @Override
    public Object visiter(Addition a)
    {
        visiter((Arithmetique) a);

        return null;
    }

    @Override
    public Object visiter(Affectation a)
    {
        cible.addLine(getIndent() + ";affect var " + a.getDestination().getValue());

        int varId;

        //      Si la destination n'est pas initialisée, l'initialise. Sinon récupère son identifiant
        if (!a.getDestination().isInitialized())
        {
            //          Attribue un identifiant à la variable
            varId = this.getNewVarId();
            a.getDestination().setVarId(varId);

            if (a.getDestination() instanceof Tableau)
            {
                //              Crée le tableau
                this.createArray((Tableau) a.getDestination());
            }

            //          Marque la variable comme initialisée
            a.getDestination().initialize();
        }
        else
            varId = a.getDestination().getVarId();

        //      Si c'est un tableau, ecrit le code pour l'acces au bon index.
        if (a.getDestination() instanceof Tableau)
        {
            a.getDestination().accepter(this);

            a.getSource().accepter(this);
            cible.addLine(getIndent() + "iastore");
        }
        //      Sinon, visite la source et enregiste la valeur dans la variable locale.
        else
        {
            a.getSource().accepter(this);

            if (varId <= 3)
                cible.addLine(getIndent() + "istore_" + varId);
            else
                cible.addLine(getIndent() + "istore " + varId);
        }

        return null;
    }

    @Override
    public Object visiter(Appel a)
    {
        //      visite les paramètres
        a.getParametres().accepter(this);

        //      récupère la classe de la méthode. si la classe est vide c'est que c'est le programme
        String cl = a.getClassOfMethod();
        if (cl.equals(""))
            cl = progName;

        //      Récupère la signature de la méthode et l'appelle
        String signature = ((SymFunc) TDS.getInstance().getSymFunc(a.getId().getValue())).getSignature();
        cible.addLine(getIndent() + "invokestatic " + cl + "/" + signature);

        return null;
    }

    @Override
    public Object visiter(Booleen b)
    {
        String indent = getIndent();

        //      True vaut -1 (tout les bits à 1) et False 0
        if (b.getBoolean())
        {
            cible.addLine(indent + ";Boolean true.");
            cible.addLine(indent + "iconst_m1");
        }
        else
        {
            cible.addLine(indent + ";Boolean false.");
            cible.addLine(indent + "iconst_0");
        }

        return null;
    }

    @Override
    public Object visiter(Chaine c)
    {
        cible.addLine(getIndent() + "ldc \"" + c.getString() + "\"");
        return null;
    }

    @Override
    public Object visiter(Condition c)
    {
        //      Execute la condition.
        c.getCond().accepter(this);

        //      Si == 0 va au ELSE sinon continue les instructions.
        cible.addLine(getIndent() + "ifeq Else" + condNb);
        indentNb++;

        //      Instructions du "alors"
        c.getAlors().accepter(this);
        //      Saute les instructions du Sinon
        cible.addLine(getIndent() + "goto EndCond" + condNb);
        //      Le sinon
        indentNb--;
        cible.addLine(getIndent() + "Else" + condNb + ":");
        indentNb++;
        c.getSinon().accepter(this);

        //      Le label de fin
        indentNb--;
        cible.addLine(getIndent() + "EndCond" + condNb + ":");

        condNb++;

        return null;
    }

    @Override
    public Object visiter(Constante c)
    {
        //      Visite l'expression de la constante
        ((SymConst) TDS.getInstance().getSymVar(c.getValue())).getExpression().accepter(this);
        return null;
    }

    @Override
    public Object visiter(Declaration d)
    {
        //      Visite les fonction a déclarer
        for (Fonction fct : d.getAllFonctions())
        {
            fct.accepter(this);
            cible.addLine("");
        }
        return null;
    }

    @Override
    public Object visiter(Different d)
    {
        this.cmpRelation(d);

        return null;
    }

    @Override
    public Object visiter(Division d)
    {
        d.getGauche().accepter(this);
        d.getDroite().accepter(this);
        this.genererErreur("0");
        d.getDroite().accepter(this);
        cible.addLine(getIndent() + "idiv");

        return null;
    }

    @Override
    public Object visiter(Ecrire e)
    {
        //      Commentaire pour le fichier .j et appel de l'objet PrintStream nécéssaire à l'affichage
        cible.addLine(getIndent() + ";PrintStream object held in java.lang.System.out");
        cible.addLine(getIndent() + "getstatic java/lang/System/out Ljava/io/PrintStream;");

        //      Visite l'expression
        e.getExpression().accepter(this);
        Type expType = e.getExpression().getType();

        //      Affiche l'expression en fonction de son type
        if (e.getExpression() instanceof Tableau)
        {
            cible.addLine(getIndent() + "iaload");
            cible.addLine(getIndent() + "invokevirtual java/io/PrintStream/println(I)V");
        }
        else if (expType.estConforme(TypeEntier.getInstance()))
            cible.addLine(getIndent() + "invokevirtual java/io/PrintStream/println(I)V");
        else if (expType.estConforme(TypeBooleen.getInstance()))
        {
            cible.addLine(getIndent() + "ifeq EcrireBool" + condNb);
            cible.addLine(getIndent() + "ldc \"vrai\"");
            cible.addLine(getIndent() + "goto EndEcrireBool" + condNb);
            cible.addLine(getIndent() + "EcrireBool" + condNb + ":");
            cible.addLine(getIndent() + "ldc \"faux\"");
            cible.addLine(getIndent() + "EndEcrireBool" + condNb + ":");
            condNb++;

            cible.addLine(getIndent() + "invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        }
        else
            cible.addLine(getIndent() + "invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");

        return null;
    }

    @Override
    public Object visiter(Egal e)
    {
        this.visiterRelation(e);

        return null;
    }

    @Override
    public Object visiter(Entier e)
    {
        String indent = this.getIndent();

        int value = e.getValue();
        if (value <= 5)
            cible.addLine(indent + "iconst_" + value);
        else
            cible.addLine(indent + "bipush " + value);

        return null;
    }

    @Override
    public Object visiter(Et e)
    {
        e.getGauche().accepter(this);
        e.getDroite().accepter(this);
        cible.addLine(getIndent() + "iand");

        return null;
    }

    @Override
    public Object visiter(Fonction f)
    {
        //      Signature de la fonction
        String signature = ((SymFunc) TDS.getInstance().getSymFunc(f.getId().getValue())).getSignature();
        cible.addLine(".method public static " + signature);
        indentNb++;

        //      Limites de pile et variables locales
        this.setStackAndLocalLimit(50, 50);

        //      Initialise les paramètres
        int paramId = 0;
        for (Expressions param : f.getParam().getAllParam())
        {
            ((Identifiant) param).setVarId(paramId);
            ((Identifiant) param).initialize();
            paramId++;
        }

        //      génère le corp de la fonction
        f.getCorps().accepter(this);
        f.getRetour().accepter(this);
        indentNb--;
        cible.addLine(".end method");

        return null;
    }

    @Override
    public Object visiter(Identifiant i)
    {
        if (TDS.getInstance().getSymVar(i.getValue()) instanceof SymConst)
        {
            ((SymConst) TDS.getInstance().getSymVar(i.getValue())).getExpression().accepter(this);
            return null;
        }
        else if (!i.isInitialized())
        {
            errors.add("variable " + i.getValue() + " non initialisée");
            return null;
        }

        int id = i.getVarId();

        if (id <= 3)
            cible.addLine(getIndent() + "iload_" + id);
        else
            cible.addLine(getIndent() + "iload " + id);
        return null;
    }

    @Override
    public Object visiter(Index i)
    {
        return null;
    }

    @Override
    public Object visiter(InfEgal ie)
    {
        //      Compare les valeurs
        this.cmpRelation(ie);

        //      sub 1 a -1, 0 ou 1 pour avoir le bon booleen.
        cible.addLine(getIndent() + "iconst_1");
        cible.addLine(getIndent() + "isub");

        return null;
    }

    @Override
    public Object visiter(Inferieur i)
    {
        this.visiterRelation(i);

        return null;
    }

    @Override
    public Object visiter(Lire l)
    {
        //      Si la variable n'est pas initialisée, l'initialise
        if (!l.getId().isInitialized())
        {
            l.getId().setVarId(getNewVarId());
            l.getId().initialize();
        }

        //      Crée l'objet nécéssaire à la récupération de la valeur de la console
        cible.addLine(getIndent() + "new java/util/Scanner");
        cible.addLine(getIndent() + "dup");
        cible.addLine(getIndent() + "getstatic java/lang/System/in Ljava/io/InputStream;");
        cible.addLine(getIndent() + "invokespecial java/util/Scanner/<init>(Ljava/io/InputStream;)V");
        cible.addLine(getIndent() + "invokevirtual java/util/Scanner/nextInt()I");

        //      Stocke la variable.D
        if (l.getId().getVarId() <= 3)
            cible.addLine(getIndent() + "istore_" + l.getId().getVarId());
        else
            cible.addLine(getIndent() + "istore " + l.getId().getVarId());

        return null;
    }

    @Override
    public Object visiter(ListeInstructions li)
    {
        for (Instructions inst : li.getAllInstructions())
        {
            inst.accepter(this);
            cible.addLine("");
        }
        return null;
    }

    @Override
    public Object visiter(Non n)
    {
        n.getOperande().accepter(this);

        //      Si c'est false (0) attribue 1 sinon 0.
        cible.addLine(getIndent() + "ifeq Not" + condNb);
        indentNb++;
        cible.addLine(getIndent() + "iconst_0");
        cible.addLine(getIndent() + "goto EndNot" + condNb);
        cible.addLine(getIndent() + "Not" + condNb + ":");
        cible.addLine(getIndent() + "iconst_1");
        indentNb--;
        cible.addLine(getIndent() + "EndNot" + condNb + ":");


        return null;
    }

    @Override
    public Object visiter(Ou o)
    {
        o.getGauche().accepter(this);
        o.getDroite().accepter(this);
        cible.addLine(getIndent() + "ior");

        return null;
    }

    @Override
    public Object visiter(Parametres p)
    {
        for (Expressions param : p.getAllParam())
        {
            param.accepter(this);
        }

        return null;
    }

    @Override
    public Object visiter(Pour p)
    {
        String indent = getIndent();

        cible.addLine(indent + ";Start of For loop.");
        new Affectation(p.getIdentifiants(), p.getBorneInf(), 0).accepter(this);
        cible.addLine(indent + "Loop" + loopId + " :");
        loopId++;
        indentNb++;

        p.getListeInstructions().accepter(this);

        indentNb--;
        loopId--;
        cible.addLine(indent + ";Condition for loop" + loopId);

        //      increment loop var.
        cible.addLine(getIndent() + "iinc " + p.getIdentifiants().getVarId() + " 1");

        p.getIdentifiants().accepter(this);
        p.getBorneSup().accepter(this);
        cible.addLine(indent + "if_icmple Loop" + loopId);


        loopId++;

        return null;
    }

    @Override
    public Object visiter(Produit p)
    {
        visiter((Arithmetique) p);
        return null;
    }

    @Override
    public Object visiter(Programme p)
    {
        progName = p.getNom().getValue();

        cible.newClass(p.getNom().getValue(), true);
        cible.addStdInit();

        p.getEntete().accepter(this);
        cible.addLine(".method public static main([Ljava/lang/String;)V");
        indentNb++;

        this.setStackAndLocalLimit(99, 99);

        p.getCorp().accepter(this);

        cible.addLine(getIndent() + "return");
        indentNb--;

        cible.addLine(".end method");
        return null;
    }

    @Override
    public Object visiter(RetourFonction rf)
    {
        rf.getRetour().accepter(this);
        cible.addLine(getIndent() + rf.getType().mnemonic() + "return");
        return null;
    }

    @Override
    public Object visiter(Soustraction s)
    {
        visiter((Arithmetique) s);
        return null;
    }

    @Override
    public Object visiter(SupEgal se)
    {
        //      Compare les valeurs
        this.cmpRelation(se);

        //      add 1 a -1, 0 ou 1 pour avoir le bon booleen.
        cible.addLine(getIndent() + "iconst_1");
        cible.addLine(getIndent() + "iadd");

        return null;
    }

    @Override
    public Object visiter(Superieur s)
    {
        this.visiterRelation(s);

        return null;
    }

    @Override
    public Object visiter(Tableau t)
    {
        int id = t.getVarId();

        ArrayList<Expressions> ind = t.getIndex().indexList();

        cible.addLine(getIndent() + "aload " + id);
        //      Index de niveau 1
        ind.get(0).accepter(this);
        cible.addLine(getIndent() + "aaload");
        //      Index de niveau 2
        ind.get(1).accepter(this);

        return null;
    }

    @Override
    public Object visiter(TantQue tq)
    {
        //      Label de debut de boucle
        cible.addLine(getIndent() + "StartWhile" + loopId + ":");

        //      Visite la condition
        tq.getExpression().accepter(this);

        //      Si == 0 (false) va à la fin.
        cible.addLine(getIndent() + "ifeq EndWhile" + loopId);
        indentNb++;

        //      Visite le corp de la boucle
        tq.getCorps().accepter(this);

        indentNb--;
        cible.addLine(getIndent() + "goto StartWhile" + loopId);
        cible.addLine(getIndent() + "EndWhile" + loopId + ":");

        loopId++;

        return null;
    }

    @Override
    public Object visiter(Tilda t)
    {
        //      Visite l'opérande
        t.getOperande().accepter(this);

        //      Applique un masque avec tout les bits a 1 (-1) et effectue un xor.
        cible.addLine(getIndent() + "iconst_m1");
        cible.addLine(getIndent() + "ixor");

        return null;
    }

    /**
     * Compare les deux expressions d'une relation
     *
     * @param r La relation
     */
    private void cmpRelation(Relation r)
    {
        //      Visite les opérandes et les transforme en long pour pouvoir appeler lcmp
        r.getGauche().accepter(this);
        cible.addLine(getIndent() + "i2l");
        r.getDroite().accepter(this);
        cible.addLine(getIndent() + "i2l");
        cible.addLine(getIndent() + "lcmp");
    }

    /**
     * Crée un nouveau tableau à 2 dimensions
     *
     * @param t Le tableau à créer
     */
    private void createArray(Tableau t)
    {
        ArrayList<Expressions> sizes = ((TypeTableau) TDS.getInstance().getSymVar(t.getValue()).getType()).getSizes();

        cible.addLine("");
        cible.addLine(getIndent() + ";Create new 2-D array of int");
        sizes.get(0).accepter(this);
        sizes.get(1).accepter(this);
        cible.addLine(getIndent() + "multianewarray [[I 2");
        cible.addLine(getIndent() + "astore " + t.getVarId());

        cible.addLine("");
    }

    /**
     * Crée le code qui va tester une erreur
     *
     * @param err L'erreur a controler
     */
    private void genererErreur(String err)
    {
        switch (err)
        {
            case "0":
                //              check if the variable at the peek of the stack is 0 and print error
                cible.addLine(getIndent() + "ifne ErrorLab" + errorId);

                indentNb++;
                Ecrire ecr = new Ecrire(new Chaine("Erreur: Division par '0'", 0), 0);
                ecr.getExpression().setType(TypeChaine.getInstance());
                ecr.accepter(this);
                cible.addLine(getIndent() + "bipush -1");
                cible.addLine(getIndent() + "invokestatic java/lang/System/exit(I)V");
                indentNb--;


                cible.addLine(getIndent() + "ErrorLab" + errorId + ":");
                errorId++;

                break;
            default:
                errors.add("Appel de la fct erreur avec un code inconnu");
        }
    }

    /**
     * Retourne l'indentation à utiliser dans le fichier jasmin.
     *
     * @return L'indentation à utiliser ( une indentation = 4 espaces)
     */
    private String getIndent()
    {
        String indent = "";
        for (int i = 0; i < indentNb; i++)
        {
            indent += "    ";
        }

        return indent;
    }

    /**
     * Donne le prochain identifiant de variable pas encore utilisé
     *
     * @return L'identifiant à utiliser
     */
    private int getNewVarId()
    {
        int id = varId;
        varId++;

        return id;
    }

    /**
     * Ecrit dans le fichier la limit de la pile et des variables locales
     *
     * @param stackLimit Taille de la pile
     * @param localLimit Nombre de variables locales.
     */
    private void setStackAndLocalLimit(int stackLimit, int localLimit)
    {
        cible.addLine(getIndent() + ".limit locals " + localLimit);
        cible.addLine(getIndent() + ".limit stack " + stackLimit);
    }

    /**
     * Visite une opération arithmétique.
     *
     * @param a L'opération à visiter.
     */
    private void visiter(Arithmetique a)
    {
        a.getGauche().accepter(this);
        a.getDroite().accepter(this);
        cible.addLine(this.getIndent() + a.mnemonique());

    }

    /**
     * Visite une relation
     *
     * @param r La relation à visiter
     */
    private void visiterRelation(Relation r)
    {
        this.cmpRelation(r);

        //      Si la retlation est vrai va au label pour push 1 sur la pile sinon push 0 et va a la fin.
        cible.addLine(getIndent() + "if" + r.mnemonique() + " Relation" + relationLabNb);
        cible.addLine(getIndent() + "iconst_0");
        cible.addLine(getIndent() + "goto EndRelation" + relationLabNb);
        cible.addLine(getIndent() + "Relation" + relationLabNb + ":");
        cible.addLine(getIndent() + "iconst_1");
        cible.addLine(getIndent() + "EndRelation" + relationLabNb + ":");

        relationLabNb++;
    }

}
