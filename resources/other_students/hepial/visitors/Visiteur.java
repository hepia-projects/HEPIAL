package visitors;

import abs.*;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public interface Visiteur
{
    Object visiter(Addition a);

    Object visiter(Affectation a);

    Object visiter(Appel a);

    Object visiter(Booleen b);

    Object visiter(Chaine c);

    Object visiter(Condition c);

    Object visiter(Constante c);

    Object visiter(Declaration d);

    Object visiter(Different d);

    Object visiter(Division d);

    Object visiter(Ecrire e);

    Object visiter(Egal e);

    Object visiter(Entier e);

    Object visiter(Et e);

    Object visiter(Fonction f);

    Object visiter(Identifiant i);

    Object visiter(Index i);

    Object visiter(InfEgal ie);

    Object visiter(Inferieur i);

    Object visiter(Lire l);

    Object visiter(ListeInstructions li);

    Object visiter(Non n);

    Object visiter(Ou o);

    Object visiter(Parametres p);

    Object visiter(Pour p);

    Object visiter(Produit p);

    Object visiter(Programme p);

    Object visiter(RetourFonction rf);

    Object visiter(Soustraction s);

    Object visiter(SupEgal se);

    Object visiter(Superieur s);

    Object visiter(Tableau t);

    Object visiter(TantQue tq);

    Object visiter(Tilda t);

}
