package visitors;

import abs.*;
import errors.Errors;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Evaluateur implements Visiteur
{
    private static final Evaluateur ourInstance = new Evaluateur();

    private Evaluateur()
    {
    }

    public static Evaluateur getInstance()
    {
        return ourInstance;
    }

    @Override
    public Object visiter(Addition a)
    {
        Object valG = a.getGauche().accepter(this);
        if (valG == null)
            return null;
        Object valD = a.getDroite().accepter(this);
        if (valD == null)
            return null;

        int g = (Integer) valG;
        int d = (Integer) valD;

        return g + d;
    }

    @Override
    public Object visiter(Soustraction s)
    {
        Object valG = s.getGauche().accepter(this);
        if (valG == null)
            return null;
        Object valD = s.getDroite().accepter(this);
        if (valD == null)
            return null;

        int g = (Integer) valG;
        int d = (Integer) valD;

        return g - d;
    }

    @Override
    public Object visiter(Produit p)
    {
        Object valG = p.getGauche().accepter(this);
        if (valG == null)
            return null;
        Object valD = p.getDroite().accepter(this);
        if (valD == null)
            return null;

        int g = (Integer) valG;
        int d = (Integer) valD;

        return g * d;
    }

    @Override
    public Object visiter(Division d)
    {
        Object valG = d.getGauche().accepter(this);
        if (valG == null)
            return null;
        Object valD = d.getDroite().accepter(this);
        if (valD == null)
            return null;

        int g = (Integer) valG;
        int dr = (Integer) valD;
        if (dr == 0)
        {
            Errors.getInstance().add("Division par zero impossible");
            return null;
        }

        return g / dr;
    }

    @Override
    public Object visiter(Egal e)
    {
        Object valG = e.getGauche().accepter(this);
        if (valG == null)
            return null;
        Object valD = e.getDroite().accepter(this);
        if (valD == null)
            return null;

        int g = (Integer) valG;
        int d = (Integer) valD;

        return g == d;
    }

    @Override
    public Object visiter(Different d)
    {
        Object valG = d.getGauche().accepter(this);
        if (valG == null)
            return null;
        Object valD = d.getDroite().accepter(this);
        if (valD == null)
            return null;

        int g = (Integer) valG;
        int dr = (Integer) valD;

        return g != dr;
    }

    @Override
    public Object visiter(SupEgal se)
    {
        Object valG = se.getGauche().accepter(this);
        if (valG == null)
            return null;
        Object valD = se.getDroite().accepter(this);
        if (valD == null)
            return null;

        int g = (Integer) valG;
        int d = (Integer) valD;

        return g >= d;
    }

    @Override
    public Object visiter(InfEgal ie)
    {
        Object valG = ie.getGauche().accepter(this);
        if (valG == null)
            return null;
        Object valD = ie.getDroite().accepter(this);
        if (valD == null)
            return null;

        int g = (Integer) valG;
        int d = (Integer) valD;

        return g <= d;
    }

    @Override
    public Object visiter(Superieur s)
    {
        return null;
    }

    @Override
    public Object visiter(Inferieur i)
    {
        return null;
    }

    @Override
    public Object visiter(Et e)
    {
        return null;
    }

    @Override
    public Object visiter(Ou o)
    {
        return null;
    }

    @Override
    public Object visiter(Identifiant i)
    {
        return null;
    }

    @Override
    public Object visiter(Constante c)
    {
        return null;
    }

    @Override
    public Object visiter(Booleen b)
    {
        return null;
    }

    @Override
    public Object visiter(Entier e)
    {
        return e.getValue();
    }

    @Override
    public Object visiter(Appel a)
    {
        return null;
    }

    @Override
    public Object visiter(Affectation a)
    {
        return null;
    }

    @Override
    public Object visiter(Condition c)
    {
        return null;
    }

    @Override
    public Object visiter(Pour p)
    {
        return null;
    }

    @Override
    public Object visiter(TantQue tq)
    {
        return null;
    }

    @Override
    public Object visiter(Parametres p)
    {
        return null;
    }

    @Override
    public Object visiter(ListeInstructions li)
    {
        return null;
    }

    @Override
    public Object visiter(Fonction f)
    {
        return null;
    }

    @Override
    public Object visiter(RetourFonction rf)
    {
        return null;
    }

    @Override
    public Object visiter(Declaration d)
    {
        return null;
    }

    @Override
    public Object visiter(Programme p)
    {
        return null;
    }

    @Override
    public Object visiter(Lire l)
    {
        return null;
    }

    @Override
    public Object visiter(Ecrire e)
    {
        return null;
    }

    @Override
    public Object visiter(Non n)
    {
        return null;
    }

    @Override
    public Object visiter(Tilda t)
    {
        return null;
    }

    @Override
    public Object visiter(Index i)
    {
        return null;
    }

    @Override
    public Object visiter(Tableau t)
    {
        return null;
    }

    @Override
    public Object visiter(Chaine c)
    {
        return null;
    }

}
