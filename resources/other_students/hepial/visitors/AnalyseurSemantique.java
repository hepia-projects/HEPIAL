package visitors;

import abs.*;
import errors.Errors;
import tds.SymConst;
import tds.SymFunc;
import tds.Symbole;
import tds.TDS;
import type.Type;
import type.TypeBooleen;
import type.TypeChaine;
import type.TypeEntier;

import java.util.ArrayList;

/**
 * Singleton visiteur
 *
 * @author David Wittwer
 * @version 0.0.1
 */
public class AnalyseurSemantique implements Visiteur
{
    private static final AnalyseurSemantique ourInstance = new AnalyseurSemantique();
    private static final TDS tds = TDS.getInstance();
    private static final Errors errors = Errors.getInstance();

    private AnalyseurSemantique()
    {
    }

    public static AnalyseurSemantique getInstance()
    {
        return ourInstance;
    }

    public Object visiter(Addition a)
    {
        this.visiterArithmetique(a);

        return null;
    }

    @Override
    public Object visiter(Affectation a)
    {
        if (tds.getSymVar(a.getDestination().getValue()) instanceof SymConst)
        {
            Errors.getInstance().add("Affectation impossible sur une constante.\n    " + a.getDestination().getValue() + " est une constante!");
            return null;
        }

        a.getSource().accepter(this);
        Type typeSource = a.getSource().getType();
        a.getDestination().accepter(this);
        Type typeDest = a.getDestination().getType();

        if (typeSource == null || typeDest == null)
            return null;

        if (typeDest.estConforme(typeSource))
            a.setType(typeSource);
        else
            Errors.getInstance().add("Type incompatibles: " + typeSource.toString() + " ne convient pas a " + typeDest.toString());

        return null;
    }

    @Override
    public Object visiter(Appel a)
    {
        SymFunc symFunc = (SymFunc) TDS.getInstance().getSymFunc(a.getId().getValue());
        if (symFunc == null)
        {
            Errors.getInstance().add("Fonction " + a.getId().getValue() + " non déclarée");
            return null;
        }

        Type fctType = symFunc.getType();

        // Controler le type des paramètres.
        a.getParametres().accepter(this);
        ArrayList<Expressions> actualParams = a.getParametres().getAllParam();
        ArrayList<Expressions> expectedParams = symFunc.getParametres().getAllParam();

        if (actualParams.size() != expectedParams.size())
        {
            Errors.getInstance().add("nombre de paramètres non compatibles lors de l'appel à la fonction " + a.getId().getValue());
            return null;
        }

        boolean error = false;
        for (int i = 0; i < actualParams.size(); i++)
        {
            Type actualType = actualParams.get(i).getType();
            Type expectedType = expectedParams.get(i).getType();
            if (actualType == null || !actualType.estConforme(expectedType))
            {
                Errors.getInstance().add("    Type attendu: " + expectedType + " Type reçu: " + actualType);
                Errors.getInstance().add("Type du paramètre pour la fonction " + a.getId() + " incohérent.");
                error = true;
            }
        }

        if (error)
            return null;

        a.getId().setType(fctType);
        a.setType(fctType);
        return null;
    }

    @Override
    public Object visiter(Booleen b)
    {
        b.setType(TypeBooleen.getInstance());
        return null;
    }

    @Override
    public Object visiter(Chaine c)
    {
        c.setType(TypeChaine.getInstance());

        return null;
    }

    @Override
    public Object visiter(Condition c)
    {
        //        visite la condition
        c.getCond().accepter(this);
        Type tc = c.getCond().getType();
        if (tc == null)
            return null;

        if (!tc.estConforme(TypeBooleen.getInstance()))
            Errors.getInstance().add("La condition n'est pas de type '" + TypeBooleen.getInstance() + "'");

        //        Visite le reste
        c.getAlors().accepter(this);
        c.getSinon().accepter(this);

        return null;
    }

    @Override
    public Object visiter(Constante c)
    {
        this.visiterIdentifiants(c);

        return null;
    }

    @Override
    public Object visiter(Declaration d)
    {
        //      Viste les fonction l'une apres l'autres
        for (Fonction fct : d.getAllFonctions())
        {
            fct.accepter(this);
        }

        return null;
    }

    @Override
    public Object visiter(Different d)
    {
        if (!this.visiterBinaire(d))
            d.setType(TypeBooleen.getInstance());

        return null;
    }

    @Override
    public Object visiter(Division d)
    {
        this.visiterArithmetique(d);

        return null;
    }

    @Override
    public Object visiter(Ecrire e)
    {
        e.getExpression().accepter(this);
        Type te = e.getExpression().getType();
        if (te == null)
        {
            Errors.getInstance().add("Expression non typée pour ecrire");
        }
        return null;
    }

    @Override
    public Object visiter(Egal e)
    {
        if (!this.visiterBinaire(e))
            e.setType(TypeBooleen.getInstance());

        return null;
    }

    @Override
    public Object visiter(Entier e)
    {
        e.setType(TypeEntier.getInstance());
        return null;
    }

    @Override
    public Object visiter(Et e)
    {
        boolean error = this.visiterBinaire(e);

        Type tg = e.getGauche().getType();

        if (tg.estConforme(TypeEntier.getInstance()))
        {
            this.errTypeNonCompatible(tg, e.operateur());
            error = true;
        }

        if (!error)
            e.setType(tg);

        return null;
    }

    @Override
    public Object visiter(Fonction f)
    {
        String blkId = f.getId().getValue();
        TDS.getInstance().enterBlock(blkId);

        if (f.getParam() != null)
            f.getParam().accepter(this);
        f.getCorps().accepter(this);
        f.getRetour().accepter(this);

        Type retType = f.getRetour().getType();
        Type fctType = TDS.getInstance().getSymFunc(blkId).getType();
        if (!retType.estConforme(fctType))
            Errors.getInstance().add("Type incompatible: Fonction " + blkId + "() déclarée " + fctType.toString() + " et retour de type " + retType.toString());
        else
            f.setType(fctType);

        TDS.getInstance().exitBlock();

        ((SymFunc) TDS.getInstance().getSymFunc(f.getId().getValue())).setSignature(f.buildSignature());

        return null;
    }

    @Override
    public Object visiter(Identifiant i)
    {
        this.visiterIdentifiants(i);

        return null;
    }

    @Override
    public Object visiter(Index i)
    {
        for (Expressions ind : i.indexList())
        {
            ind.accepter(this);

            if (!ind.getType().estConforme(TypeEntier.getInstance()))
            {
                errors.add("un type entier est nécéssaire pour l'index d'un tableau");
            }
        }

        return null;
    }

    @Override
    public Object visiter(InfEgal ie)
    {
        boolean error = this.visiterBinaire(ie);

        Type tg = ie.getGauche().getType();
        if (tg.estConforme(TypeBooleen.getInstance()))
        {
            this.errTypeNonCompatible(tg, "<=");
            error = true;
        }

        if (!error)
            ie.setType(TypeBooleen.getInstance());

        return null;
    }

    @Override
    public Object visiter(Inferieur i)
    {
        if (this.visiterBinaire(i))
            return null;


        Type tg = i.getGauche().getType();
        if (tg.estConforme(TypeBooleen.getInstance()))
            this.errTypeNonCompatible(tg, "<");
        else
            i.setType(TypeBooleen.getInstance());


        return null;
    }

    @Override
    public Object visiter(Lire l)
    {
        l.getId().accepter(this);
        Type type = l.getId().getType();
        if (type == null)
            Errors.getInstance().add("Expression Lire non typée");

        return null;
    }

    @Override
    public Object visiter(ListeInstructions li)
    {
        for (Instructions inst : li.getAllInstructions())
        {
            inst.accepter(this);
        }

        return null;
    }

    @Override
    public Object visiter(Non n)
    {
        n.getOperande().accepter(this);
        Type tOp = n.getOperande().getType();

        if (tOp == null || !tOp.estConforme(TypeBooleen.getInstance()))
        {
            errors.add("Type '" + tOp + "' est incompatible avec l'opération 'non'");
            return null;
        }

        n.setType(tOp);
        return null;
    }

    @Override
    public Object visiter(Ou o)
    {
        boolean error = this.visiterBinaire(o);

        Type tg = o.getGauche().getType();
        if (tg.estConforme(TypeEntier.getInstance()))
        {
            this.errTypeNonCompatible(tg, "ou");
            error = true;
        }

        if (!error)
            o.setType(tg);

        return null;
    }

    @Override
    public Object visiter(Parametres p)
    {
        ArrayList<Expressions> parametres = p.getAllParam();

        for (Expressions exp : parametres)
        {
            exp.accepter(this);
        }

        return parametres;
    }

    @Override
    public Object visiter(Pour p)
    {
        p.getIdentifiants().accepter(this);
        Type ti = p.getIdentifiants().getType();
        if (ti.estConforme(TypeBooleen.getInstance()))
        {
            Errors.getInstance().add("Type " + ti.toString() + " non accepté pour l'instruction 'pour'");
            return null;
        }

        p.getBorneInf().accepter(this);
        Type tbi = p.getBorneInf().getType();
        p.getBorneSup().accepter(this);
        Type tbs = p.getBorneSup().getType();

        if (!ti.estConforme(tbi) || !ti.estConforme(tbs))
        {
            String string = "Types incompatibles pour l'instruction 'pour'" + System.getProperty("line.separator");
            string += "    Type variable: " + ti + System.getProperty("line.separator");
            string += "    Type borne inf: " + tbi + System.getProperty("line.separator");
            string += "    Type borne sup: " + tbs + System.getProperty("line.separator");

            Errors.getInstance().add(string);
        }

        p.getListeInstructions().accepter(this);

        return null;
    }

    @Override
    public Object visiter(Produit p)
    {
        this.visiterArithmetique(p);

        return null;
    }

    @Override
    public Object visiter(Programme p)
    {
        p.getEntete().accepter(this);
        p.getCorp().accepter(this);

        return null;
    }

    @Override
    public Object visiter(RetourFonction rf)
    {
        rf.getRetour().accepter(this);
        rf.setType(rf.getRetour().getType());

        return null;
    }

    @Override
    public Object visiter(Soustraction s)
    {
        visiterArithmetique(s);

        return null;
    }

    @Override
    public Object visiter(SupEgal se)
    {
        boolean error = this.visiterBinaire(se);

        Type tg = se.getGauche().getType();
        if (tg.estConforme(TypeBooleen.getInstance()))
        {
            this.errTypeNonCompatible(tg, ">=");
            error = true;
        }

        if (!error)
            se.setType(TypeBooleen.getInstance());

        return null;
    }

    @Override
    public Object visiter(Superieur s)
    {
        boolean error = this.visiterBinaire(s);

        Type tg = s.getGauche().getType();
        if (tg.estConforme(TypeBooleen.getInstance()))
        {
            this.errTypeNonCompatible(tg, ">");
            error = true;
        }

        if (!error)
            s.setType(TypeBooleen.getInstance());

        return null;
    }

    @Override
    public Object visiter(Tableau t)
    {
        this.visiterIdentifiants(t);

        return null;
    }

    @Override
    public Object visiter(TantQue tq)
    {
        tq.getExpression().accepter(this);
        Type te = tq.getExpression().getType();
        if (te == null)
            return null;

        if (!te.estConforme(TypeBooleen.getInstance()))
        {
            Errors.getInstance().add("Condition de la boucle tantque pas de type '" + TypeBooleen.getInstance() + "'");
        }

        tq.getCorps().accepter(this);

        return null;
    }

    @Override
    public Object visiter(Tilda t)
    {
        t.getOperande().accepter(this);
        Type tOp = t.getOperande().getType();

        if (tOp == null || !tOp.estConforme(TypeEntier.getInstance()))
        {
            errors.add("Type '" + tOp + "' est incompatible avec l'opération '~'");
            return null;
        }

        t.setType(tOp);
        return null;
    }

    /**
     * Ajoute une erreur de type dans le Singleton des erreurs.
     *
     * @param type Le type incompatible
     * @param op   L'opérations avec laquelle le type est incompatible
     */
    private void errTypeNonCompatible(Type type, String op)
    {
        Errors.getInstance().add("Type '" + type + "' est incompatible avec lopération '" + op + "'");
    }

    /**
     * Visite de base pour un binaire
     *
     * @param b Le binaire à visiter
     * @return True si il y a une erreur.
     */
    private boolean visiterBinaire(Binaire b)
    {
        b.getGauche().accepter(this);
        Type tg = b.getGauche().getType();
        b.getDroite().accepter(this);
        Type td = b.getDroite().getType();

        if (tg == null || td == null || !tg.estConforme(td))
        {
            String string = "Types incompatibles pour l'opération '" + b.operateur() + "'" + System.getProperty("line.separator");
            string += "    Type gauche: " + tg + System.getProperty("line.separator");
            string += "    Type droit: " + td + System.getProperty("line.separator");

            Errors.getInstance().add(string);
            return true;
        }
        else
            return false;
    }

    /**
     * Visite un identifiants (Constante, Identifiant, Tableau)
     *
     * @param i L'identifiants à visiter.
     */
    private void visiterIdentifiants(Identifiants i)
    {
        Symbole symboleId = TDS.getInstance().getSymVar(i.getValue());
        if (symboleId == null)
            Errors.getInstance().add("Variable " + i.getValue() + " non déclarée");
        else
            i.setType(symboleId.getType());
    }

    /**
     * Viste une expression arithmétique.
     *
     * @param a L'expression à visiter.
     */
    private void visiterArithmetique(Arithmetique a)
    {
        //      Visite le binaire qui vérifie que le type des deux expressions est le même
        boolean error = this.visiterBinaire(a);

        //      Si le type gauche est booleen il y a une erreur
        Type tg = a.getGauche().getType();
        if (tg.estConforme(TypeBooleen.getInstance()))
        {
            this.errTypeNonCompatible(tg, a.operateur());
            error = true;
        }

        //      Si il ny a pas d'erreur definit le type de l'expression.
        if (!error)
            a.setType(tg);
    }
}
