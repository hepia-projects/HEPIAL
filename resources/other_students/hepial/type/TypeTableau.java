package type;

import abs.Expressions;

import java.util.ArrayList;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class TypeTableau extends Type
{
    private Type type;
    private Expressions size1;
    private Expressions size2;

    public TypeTableau(Expressions size1, Expressions size2)
    {
        this.size1 = size1;
        this.size2 = size2;
    }

    public ArrayList<Expressions> getSizes()
    {
        ArrayList<Expressions> list = new ArrayList<>();
        list.add(size1);
        list.add(size2);

        return list;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return this.type + "[" + this.size1 + ".." + this.size2 + "]";
    }

    @Override
    public boolean estConforme(Type other)
    {
        return this.type.estConforme(other);
    }

    @Override
    public String methodMnemonic()
    {
        return "[[" + this.type.methodMnemonic();
    }

    @Override
    public String mnemonic()
    {
        return "a";
    }
}
