package type;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class TypeEntier extends Type
{
    private static final TypeEntier ourInstance = new TypeEntier();

    private TypeEntier()
    {
    }

    public static TypeEntier getInstance()
    {
        return ourInstance;
    }

    public boolean estConforme(Type other)
    {
        return other instanceof TypeEntier;
    }

    @Override
    public String methodMnemonic()
    {
        return "I";
    }

    @Override
    public String mnemonic()
    {
        return "i";
    }

    @Override
    public String toString()
    {
        return "Entier";
    }
}
