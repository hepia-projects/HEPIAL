package type;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class TypeChaine extends Type
{
    private static final TypeChaine ourInstance = new TypeChaine();

    private TypeChaine()
    {
    }

    public static TypeChaine getInstance()
    {
        return ourInstance;
    }

    @Override
    public boolean estConforme(Type t)
    {
        return t instanceof TypeChaine;
    }

    @Override
    public String methodMnemonic()
    {
        return "[Ljava/lang/String;";
    }

    @Override
    public String mnemonic()
    {
        return "a";
    }

    @Override
    public String toString()
    {
        return "Chaine";
    }
}
