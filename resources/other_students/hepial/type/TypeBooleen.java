package type;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class TypeBooleen extends Type
{
    private static final TypeBooleen ourInstance = new TypeBooleen();

    private TypeBooleen()
    {
    }

    public static TypeBooleen getInstance()
    {
        return ourInstance;
    }

    public boolean estConforme(Type other)
    {
        return other instanceof TypeBooleen;
    }

    @Override
    public String methodMnemonic()
    {
        return "I";
    }

    @Override
    public String mnemonic()
    {
        return "i";
    }

    @Override
    public String toString()
    {
        return "Booleen";
    }
}
