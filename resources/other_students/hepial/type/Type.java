package type;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public abstract class Type
{
    /**
     * Test si les types sont compatibles.
     *
     * @param other Le type de l'autre objet
     * @return True si c'est compatible False sinon
     */
    public abstract boolean estConforme(Type other);

    /**
     * Le mnémonique du type pour une méthode
     *
     * @return String du mnémonique.
     */
    public abstract String methodMnemonic();

    /**
     * Le mnémonique pour un type (int, float, ...)
     *
     * @return String du mnémonique.
     */
    public abstract String mnemonic();

}
