package tds;

import abs.Expressions;
import type.Type;

/**
 * Author   :   Joao Mendes
 * Date     :   03.05.16.
 * Version  :   0.0.1
 */
public class SymConst extends Symbole
{
    private final Expressions expression;

    public SymConst(String id, Type t, Expressions expression)
    {
        super(id, t);
        this.expression = expression;
    }

    public Expressions getExpression()
    {
        return expression;
    }
}
