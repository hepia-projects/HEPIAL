package tds;

import errors.Errors;

import java.util.HashMap;
import java.util.Stack;

/**
 * Author   :   Joao Mendes
 * Date     :   05.04.16.
 * Version  :   0.0.1
 */
public class TDS
{
    private static final TDS instance = new TDS();
    private final Stack<Integer> blockStack;
    private final Stack<String> blockIdStack;
    private final HashMap<String, Stack> table;
    private Integer blockNumber;

    /**
     * Constructeur de la classe TDS
     */
    private TDS()
    {
        this.table = new HashMap<String, Stack>();
        this.blockStack = new Stack<Integer>();
        this.blockIdStack = new Stack<String>();
        this.blockNumber = -1;
    }

    /**
     * Acces au singleton
     *
     * @return Le singleton de TDS
     */
    public static TDS getInstance()
    {
        return instance;
    }

    /**
     * Indique que l'on est entres dans un bloc
     *
     * @param blockId Identifiant du bloc
     */
    public void enterBlock(String blockId)
    {
        this.blockNumber++;
        this.blockIdStack.push(blockId);
        this.blockStack.push(this.blockNumber);
    }

    /**
     * Indique que l'on est sortis d'un bloc
     */
    public void exitBlock()
    {
        this.blockStack.pop();
        this.blockIdStack.pop();
        this.blockNumber--;
    }

    /**
     * Ajoute un symbole a la table
     *
     * @param e Identifiant du symbole
     * @param s Symbole
     */
    public void addSymbol(String e, Symbole s)
    {
        Stack ls = table.get(e);
        // maj des champs du block
        s.setBlock(this.blockNumber);
        s.setBlockId(this.blockIdStack.get(this.blockNumber));

        if (ls == null)
        {
            ls = new Stack();
            ls.push(s);
            table.put(e, ls);
        }
        else
        {
            Symbole first = (Symbole) (ls.peek());

            if (first.equals(s))
            {
                Errors.getInstance().add("Double declaration de :  " + first.getIdent());
            }
            else
            {
                ls.push(s);
            }
        }
    }

    /**
     * Cherche et renvoie un symbole fonction correspondant a l'id
     *
     * @param id Identifiant de la fonction
     * @return Symbole de la fonction
     */
    public Symbole getSymFunc(String id)
    {
        Stack ls = table.get(id);

        if (ls != null)
        {
            for (int i = 0; i < ls.size(); i++)
            {
                if (ls.get(i) instanceof SymFunc)
                    return (SymFunc) ls.get(i);
            }
        }
        return null;
    }

    /**
     * Cherche et renvoie un symbole variable correspondant a l'id
     *
     * @param id Identifiant de la variable
     * @return Symbole de la variable
     */
    public Symbole getSymVar(String id)
    {

        Stack ls = table.get(id);

        if (ls != null)
        {
            for (int i = 0; i < ls.size(); i++)
            {
                if (ls.get(i) instanceof SymVar)
                    return (SymVar) ls.get(i);

                if (ls.get(i) instanceof SymConst)
                    return (SymConst) ls.get(i);
            }
        }

        return null;
    }

    /**
     * Affiche la table des symboles
     */
    public void show()
    {
        for (String k : this.table.keySet())
        {
            Stack ls = this.table.get(k);

            for (int i = 0; i < ls.size(); i++)
            {
                System.out.println(ls.get(i).toString());
            }
        }
    }
}
