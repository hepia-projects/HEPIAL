package tds;

import type.Type;

/**
 * Author   :   Joao Mendes
 * Date     :   03.05.16.
 * Version  :   0.0.1
 */
public abstract class Symbole
{
    private final String ident;
    private final Type type;
    private Integer block;
    private String blockId;
    private Integer symId;
    private boolean initialized;

    public Symbole(String id, Type t)
    {
        this.ident = id;
        this.type = t;
        this.symId = null;
        this.initialized = false;
    }

    public String getIdent()
    {
        return ident;
    }

    public Integer getBlock()
    {
        return block;
    }

    public void setBlock(Integer block)
    {
        this.block = block;
    }

    public String getBlockId()
    {
        return blockId;
    }

    public void setBlockId(String blockId)
    {
        this.blockId = blockId;
    }

    public Type getType()
    {
        return type;
    }

    /**
     * Redefinition de la methode toString
     *
     * @return La string de la classe
     */
    public String toString()
    {
        return this.getIdent() + " " + this.getBlock().toString() + " " + this.getBlockId() + " " + this.getType().toString();
    }


    /**
     * Surcharge de la méthode equals
     */
    @Override
    public boolean equals(Object other)
    {
        if (other == null)
            return false;

        if (other == this)
            return true;

        if (!(other instanceof Symbole))
            return false;

        Symbole otherSymbole = (Symbole) other;

        return (otherSymbole.getIdent().equals(this.getIdent())) &&
                (otherSymbole.getType().toString().equals(this.getType().toString())) &&
                (otherSymbole.getBlock() == this.getBlock() && (otherSymbole.getBlockId().equals(this.getBlockId())));

    }

    public Integer getSymId()
    {
        return symId;
    }

    public void setSymId(Integer symId)
    {
        this.symId = symId;
    }

    public boolean isInitialized()
    {
        return initialized;
    }

    public void initializeSym()
    {
        this.initialized = true;
    }
}
