package tds;

import abs.Parametres;
import type.Type;

/**
 * Author   :   Joao Mendes
 * Date     :   03.05.16.
 * Version  :   0.0.1
 */
public class SymFunc extends Symbole
{
    private Parametres parametres;
    private String signature;

    public SymFunc(String id, Type t)
    {
        super(id, t);
        this.parametres = null;
        this.signature = "";
    }

    public Parametres getParametres()
    {
        return parametres;
    }

    public void setParametres(Parametres parametres)
    {
        this.parametres = parametres;
    }

    public String getSignature()
    {
        return signature;
    }

    public void setSignature(String signature)
    {
        this.signature = signature;
    }
}

