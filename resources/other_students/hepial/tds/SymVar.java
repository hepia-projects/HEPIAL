package tds;

import type.Type;

/**
 * Author   :   Joao Mendes
 * Date     :   03.05.16.
 * Version  :   0.0.1
 */
public class SymVar extends Symbole
{
    public SymVar(String id, Type t)
    {
        super(id, t);
    }
}
