package errors;

import java.util.Stack;

/**
 * Author   :   Joao Mendes
 * Date     :   19.04.16.
 * Version  :   0.0.1
 */
public class Errors
{
    private static final Errors instance = new Errors();
    private final Stack<String> errorsList;

    /**
     * Constructeur de la classe Errors
     */
    private Errors()
    {
        errorsList = new Stack<String>();
    }

    /**
     * Acces au singleton de la classe Errors
     *
     * @return Le singleton de la classe
     */
    public static Errors getInstance()
    {
        return instance;
    }

    /**
     * Ajoute une erreur a la liste
     *
     * @param err Erreur a ajouter
     */
    public void add(String err)
    {
        errorsList.push(err);
    }

    /**
     * Controle si il y a au moin une erreur.
     *
     * @return true si il y a une erreur false sinon
     */
    public boolean asError()
    {
        return !this.errorsList.empty();
    }

    /**
     * Redefinition de toString
     *
     * @return Le string de la classe
     */
    public String toString()
    {
        String errls = "";

        while (!errorsList.isEmpty())
        {
            errls += errorsList.pop() + System.getProperty("line.separator");
        }

        return errls;
    }
}
