package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Pour extends Instructions
{
    private final Identifiants identifiants;
    private final Expressions borneInf;
    private final Expressions borneSup;
    private final ListeInstructions listeInstructions;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Pour(Identifiant identifiants, Expressions borneInf, Expressions borneSup, ListeInstructions listeInstructions, int line)
    {
        super(line);
        this.identifiants = identifiants;
        this.borneInf = borneInf;
        this.borneSup = borneSup;
        this.listeInstructions = listeInstructions;
    }

    /**
     * Getteur pour l'identifiant
     *
     * @return L'identifiant de l'instruction
     */
    public Identifiants getIdentifiants()
    {
        return identifiants;
    }

    /**
     * Getteur pour la borne inferieure de la boucle
     *
     * @return Expression de la borne inférieure
     */
    public Expressions getBorneInf()
    {
        return borneInf;
    }

    /**
     * Getteur pour la borne supèrieure
     *
     * @return Expression de la borne supérieure
     */
    public Expressions getBorneSup()
    {
        return borneSup;
    }

    /**
     * Getteur pour les instructions de la boucle
     *
     * @return Les instructions de la boucle
     */
    public ListeInstructions getListeInstructions()
    {
        return listeInstructions;
    }

    @Override
    public String toString()
    {
        return "pour " + this.identifiants.toString() + " allantde " + this.borneInf.toString() + " a " + this.borneSup.toString() + " faire\n" + this.listeInstructions.toString() + "finpour\n";
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;

        string += this.identifiants.buildTree(level);
        string += this.borneInf.buildTree(level);
        string += this.borneSup.buildTree(level);
        string += this.listeInstructions.buildTree(level);

        return string;
    }
}
