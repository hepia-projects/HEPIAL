package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Different extends Relation
{
    /**
     * Constructeur par defaut avec l'opérande gauche et droite.
     *
     * @param line Ligne de l'instruction.
     */
    public Different(int line)
    {
        super(line);
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String operateur()
    {
        return "<>";
    }

    @Override
    public String mnemonique()
    {
        return "ne";
    }
}