package abs;

import type.Type;
import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public abstract class ArbreAbstrait
{
    private final int line;
    private Type type;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    ArbreAbstrait(int line)
    {
        this.line = line;
        this.type = null;
    }

    /**
     * Récupère la ligne de l'instruction.
     *
     * @return Ligne de l'instruction
     */
    public int getLine()
    {
        return line;
    }

    /**
     * Méthode pour permettre à un visiteur de visiter l'objet.
     *
     * @param v Le visiteur
     * @return L'objet retourné par la visite.
     */
    public abstract Object accepter(Visiteur v);

    /**
     * Crée une string pour afficher l'arbre abstrait.
     *
     * @param level Nombre d'indentation à faire.
     * @return La string à afficher.
     */
    public String buildTree(int level)
    {
        String indent = "";
        for (int i = 0; i < level; i++)
        {
            indent += "|   ";
        }

        return indent + this.getClass().toString() + "\t\tType: " + this.getType() + "\n";
    }

    /**
     * Getteur pour le type de l'objet
     *
     * @return Retourne l'objet Type correspondant.
     */
    public Type getType()
    {
        return this.type;
    }

    /**
     * Setteur pour le type
     *
     * @param type Le Type à définir.
     */
    public void setType(Type type)
    {
        this.type = type;
    }
}
