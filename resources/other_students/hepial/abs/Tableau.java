package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Tableau extends Identifiants
{
    private Index index;

    /**
     * Constructeur par defaut.
     *
     * @param val  La valeur de la string.
     * @param line La  ligne de l'instruction.
     */
    public Tableau(String val, int line)
    {
        super(val, line);
    }

    public Index getIndex()
    {
        return this.index;
    }

    public void setIndex(Index index)
    {
        this.index = index;
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }


}
