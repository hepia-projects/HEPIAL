package abs;

import visitors.Visiteur;

import java.util.ArrayList;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Parametres extends Instructions
{
    private final ArrayList<Expressions> parametres;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Parametres(int line)
    {
        super(line);
        this.parametres = new ArrayList<>();
    }

    /**
     * Ajoute un paramètre.
     * @param exp Paramètre à ajouter.
     */
    public void addParam(Expressions exp)
    {
        this.parametres.add(exp);
    }

    /**
     * Getteur pour tous les paramètres.
     * @return Liste d'Expression qui est les paramètres.
     */
    public ArrayList<Expressions> getAllParam()
    {
        return this.parametres;
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String toString()
    {
        String string = "";
        for (Expressions exp : this.parametres)
        {
            string += exp + " ";
        }

        return string;
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        for (Expressions exp : this.parametres)
        {
            string += exp.buildTree(level);
        }

        return string;
    }
}
