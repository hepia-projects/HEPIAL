package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Ou extends Relation
{
    /**
     * Constructeur par defaut avec l'opérande gauche et droite.
     *
     * @param line Ligne de l'instruction.
     */
    public Ou(int line)
    {
        super(line);
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String operateur()
    {
        return "ou";
    }

    @Override
    public String mnemonique()
    {
        return "";
    }
}
