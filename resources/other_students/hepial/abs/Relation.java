package abs;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public abstract class Relation extends Binaire
{
    /**
     * Constructeur par defaut avec l'opérande gauche et droite.
     *
     * @param line Ligne de l'instruction.
     */
    public Relation(int line)
    {
        super(null, null, line);
    }
}
