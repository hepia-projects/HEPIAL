package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class RetourFonction extends Instructions
{
    private final Expressions retour;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public RetourFonction(Expressions retour, int line)
    {
        super(line);
        this.retour = retour;
    }

    public Expressions getRetour()
    {
        return retour;
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String toString()
    {
        return "retourne " + retour.toString() + "\n";
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;

        string += this.retour.buildTree(level);

        return string;
    }
}
