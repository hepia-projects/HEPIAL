package abs;

import visitors.Visiteur;

import java.util.ArrayList;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Index extends Expressions
{
    private ArrayList<Expressions> indexList;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Index(int line)
    {
        super(line);
        this.indexList = new ArrayList<>();
    }

    @Override
    public Object accepter(Visiteur v)
    {

        return v.visiter(this);
    }

    /**
     * Ajoute un index.
     *
     * @param index Expression de l'index.
     */
    public void addIndex(Expressions index)
    {
        this.indexList.add(index);
    }

    /**
     * Getteur pour les index.
     *
     * @return Liste des index
     */
    public ArrayList<Expressions> indexList()
    {
        return this.indexList;
    }

}
