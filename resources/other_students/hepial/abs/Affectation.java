package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Affectation extends Instructions
{
    private final Expressions source;
    private final Identifiants destination;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Affectation(Identifiants destination, Expressions source, int line)
    {
        super(line);
        this.source = source;
        this.destination = destination;
    }

    /**
     * Getteur pour la source de l'affectation
     *
     * @return L'Expression source.
     */
    public Expressions getSource()
    {
        return source;
    }

    /**
     * Getteur pour la destination de l'affectation.
     *
     * @return L'Identifiant destination.
     */
    public Identifiants getDestination()
    {
        return destination;
    }


    public String toString()
    {
        return this.destination.toString() + " = " + this.source.toString();
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;

        string += this.destination.buildTree(level);
        string += this.source.buildTree(level);
        return string;
    }
}
