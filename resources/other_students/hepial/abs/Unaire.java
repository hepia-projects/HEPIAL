package abs;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public abstract class Unaire extends Expressions
{
    private Expressions operande;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Unaire(int line)
    {
        super(line);
    }

    /**
     * Getteur pour avoir l'opérande.
     *
     * @return L'operande.
     */
    public Expressions getOperande()
    {
        return this.operande;
    }

    /**
     * Setteru pour l'opérande.
     *
     * @param operande L'opérande é attribuer.
     */
    public void setOperande(Expressions operande)
    {
        this.operande = operande;
    }

    /**
     * Getteur por l'opérateur de l'expression.
     *
     * @return La String de l'opérateur.
     */
    public abstract String operateur();

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        string += this.operande.buildTree(level);

        return string;
    }

    @Override
    public String toString()
    {
        return this.operateur() + " " + this.operande;
    }
}
