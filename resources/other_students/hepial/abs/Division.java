package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Division extends Arithmetique
{
    public Division(int line)
    {
        super(line);
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String operateur()
    {
        return "/";
    }

    @Override
    public String mnemonique()
    {
        return "idiv";
    }
}
