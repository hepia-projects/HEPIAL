package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Et extends Relation
{
    /**
     * Constructeur par defaut avec l'opérande gauche et droite.
     *
     * @param line Ligne de l'instruction.
     */
    public Et(int line)
    {
        super(line);
    }

    @Override
    public String operateur()
    {
        return "et";
    }

    @Override
    public String mnemonique()
    {
        return "";
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }
}
