package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Soustraction extends Arithmetique
{
    public Soustraction(int line)
    {
        super(line);
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String operateur()
    {
        return "-";
    }

    @Override
    public String mnemonique()
    {
        return "isub";
    }
}
