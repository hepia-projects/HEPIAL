package abs;

import tds.TDS;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public abstract class Identifiants extends Expressions
{
    private static final TDS tds = TDS.getInstance();
    private String value;

    /**
     * Constructeur par defaut.
     *
     * @param val  La valeur de la string.
     * @param line La  ligne de l'instruction.
     */
    public Identifiants(String val, int line)
    {
        super(line);
        this.value = val;
    }

    /**
     * Récupère la valeur de la string.
     *
     * @return La valeur de la string.
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Définit la valeur de la string.
     *
     * @param value La valeur à attribuer.
     */
    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return value;
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        String ident = "";
        for (int i = 0; i < level; i++)
        {
            ident += "|   ";
        }

        return string + ident + this.value + "\n";
    }

    /**
     * Getteur pour le numéro de la variable pour la génération du code
     *
     * @return Integer du nom de la variable.
     */
    public Integer getVarId()
    {
        return tds.getSymVar(this.value).getSymId();
    }

    /**
     * Attribue un numéro à la variable.
     *
     * @param varId Le numéro à attribuer
     */
    public void setVarId(int varId)
    {
        tds.getSymVar(this.value).setSymId(varId);
    }

    /**
     * Controle si la variable est initialisée ou pas.
     *
     * @return True si elle est initialisé False sinon.
     */
    public boolean isInitialized()
    {
        return tds.getSymVar(this.value).isInitialized();
    }

    /**
     * Initialise la variable
     */
    public void initialize()
    {
        tds.getSymVar(this.getValue()).initializeSym();
    }
}
