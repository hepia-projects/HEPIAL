package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Produit extends Arithmetique
{
    public Produit(int line)
    {
        super(line);
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String operateur()
    {
        return "*";
    }

    @Override
    public String mnemonique()
    {
        return "imul";
    }
}
