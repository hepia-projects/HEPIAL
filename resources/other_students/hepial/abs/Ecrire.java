package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Ecrire extends Instructions
{
    private Expressions expression;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Ecrire(Expressions expression, int line)
    {
        super(line);
        this.expression = expression;
    }

    /**
     * Getteur pour l'Expression à ecrire
     * @return L'Expression à ecrire.
     */
    public Expressions getExpression()
    {
        return expression;
    }

    /**
     * Setteur pour l'Expression à ecrire
     * @param expression L'Expression à écrire.
     */
    public void setExpression(Expressions expression)
    {
        this.expression = expression;
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        string += this.expression.buildTree(level);

        return string;
    }

    @Override
    public String toString()
    {
        if (this.expression != null)
            return "ecrire " + this.expression.toString();
        else
            return "ecrire";
    }
}
