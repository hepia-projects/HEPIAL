package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Entier extends Expressions
{
    private Integer value;

    /**
     * Constructeur par defaut.
     *
     * @param val  La valeur à attribuer.
     * @param line La ligne de l'instruction.
     */
    public Entier(Integer val, int line)
    {
        super(line);
        this.value = val;
    }

    /**
     * Retourne la valeur de l'entier.
     *
     * @return La valeur de l'entier.
     */
    public Integer getValue()
    {
        return value;
    }

    /**
     * Définit la valeur de l'entier.
     *
     * @param value La valeur à attribuer.
     */
    public void setValue(Integer value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return value.toString();
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        String indent = "";

        for (int i = 0; i < level; i++)
        {
            indent += "|   ";
        }

        return string + indent + this.value + "\n";
    }
}
