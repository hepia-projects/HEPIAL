package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Programme extends Instructions
{
    private Identifiant nom;
    private Declaration entete;
    private ListeInstructions corp;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Programme(Identifiant nom, int line)
    {
        super(line);
        this.nom = nom;
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    public Declaration getEntete()
    {
        return entete;
    }

    public void setEntete(Declaration entete)
    {
        this.entete = entete;
    }

    public ListeInstructions getCorp()
    {
        return corp;
    }

    public void setCorp(ListeInstructions corp)
    {
        this.corp = corp;
    }

    public Identifiant getNom()
    {
        return nom;
    }

    public void setNom(Identifiant nom)
    {
        this.nom = nom;
    }

    @Override
    public String toString()
    {
        String str = "programme " + this.nom.toString() + "\n";

        if (this.entete != null)
            str += this.entete.toString();
        if (this.corp != null)
            str += this.corp.toString();

        return str;
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;

        string += this.nom.buildTree(level);
        string += this.entete.buildTree(level);
        string += this.corp.buildTree(level);

        return string;
    }
}
