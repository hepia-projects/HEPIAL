package abs;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public abstract class Binaire extends Expressions
{
    private Expressions opGauche;
    private Expressions opDroite;

    /**
     * Constructeur par defaut avec l'opérande gauche et droite.
     *
     * @param opG  Opérande gauche.
     * @param opD  Opérande droite.
     * @param line Ligne de l'instruction.
     */
    Binaire(Expressions opG, Expressions opD, int line)
    {
        super(line);
        this.opGauche = opG;
        this.opDroite = opD;
    }

    /**
     * Retourne l'opérande de droite.
     *
     * @return Opérande de droite.
     */
    public Expressions getDroite()
    {
        return opDroite;
    }

    /**
     * Définit L'opérande de droite
     *
     * @param opDroite L'opérande à définir.
     */
    public void setDroite(Expressions opDroite)
    {
        this.opDroite = opDroite;
    }

    /**
     * Retourne l'opérande de gauche.
     *
     * @return Opérande de gauche.
     */
    public Expressions getGauche()
    {
        return opGauche;
    }

    /**
     * Définit l'opérande de gauche.
     *
     * @param opGauche L'opérande à définir.
     */
    public void setGauche(Expressions opGauche)
    {
        this.opGauche = opGauche;
    }

    @Override
    public String toString()
    {
        return opGauche + " " + operateur() + " " + opDroite;
    }

    /**
     * Getteur pour l'opérateur
     *
     * @return La String corréspondant à l'opérateur
     */
    public abstract String operateur();

    /**
     * Getteur pour le mnémonique
     *
     * @return La String du mnémonique.
     */
    public abstract String mnemonique();

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        string += this.getGauche().buildTree(level);
        string += this.getDroite().buildTree(level);

        return string;
    }
}
