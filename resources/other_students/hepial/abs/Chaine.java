package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Chaine extends Expressions
{
    private String string;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Chaine(String string, int line)
    {
        super(line);
        this.string = string;
    }

    /**
     * Getteur pour la chaine de caractères.
     * @return La String.
     */
    public String getString()
    {
        return string;
    }

    /**
     * Setteur pour la chaine de caractères
     * @param string La String à attribuer.
     */
    public void setString(String string)
    {
        this.string = string;
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        String ident = "";
        for (int i = 0; i < level; i++)
        {
            ident += "|   ";
        }

        return string + ident + this.string + "\n";
    }
}
