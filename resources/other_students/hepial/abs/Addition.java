package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Addition extends Arithmetique
{
    /**
     * Constructeur qui appel le constructeur de Arithmétique
     *
     * @param line La ligne de l'instruction.
     */
    public Addition(int line)
    {
        super(line);
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String operateur()
    {
        return "+";
    }

    @Override
    public String mnemonique()
    {
        return "iadd";
    }
}
