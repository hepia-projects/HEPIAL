package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Lire extends Instructions
{
    private Identifiant id;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Lire(Identifiant id, int line)
    {
        super(line);
        this.id = id;
    }

    /**
     * Getteur pour l'Identifiant de l'instruction.
     *
     * @return L'Identifiant dans lequel mettre la valeur lue.
     */
    public Identifiant getId()
    {
        return id;
    }

    /**
     * Setteur pour l'Identifiant de l'instruction
     *
     * @param id L'Identifiant dans lequel mettre la valeur lue.
     */
    public void setId(Identifiant id)
    {
        this.id = id;
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String toString()
    {
        return "lire " + id.toString();
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        string += id.buildTree(level);

        return string;
    }
}
