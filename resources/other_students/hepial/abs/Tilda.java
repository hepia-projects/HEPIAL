package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Tilda extends Unaire
{
    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Tilda(int line)
    {
        super(line);
    }

    @Override
    public String operateur()
    {
        return "~";
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }
}
