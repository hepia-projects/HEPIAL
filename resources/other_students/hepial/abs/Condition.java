package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Condition extends Instructions
{
    private final Expressions cond;
    private final ListeInstructions alors;
    private final ListeInstructions sinon;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Condition(Expressions cond, ListeInstructions alors, ListeInstructions sinon, int line)
    {
        super(line);
        this.cond = cond;
        this.alors = alors;
        this.sinon = sinon;
    }

    /**
     * Getteur pour la condition.
     * @return Expression de la condition.
     */
    public Expressions getCond()
    {
        return cond;
    }

    /**
     * Getteur pour les instructions si la condition est vrai.
     * @return La ListeInstruction.
     */
    public ListeInstructions getAlors()
    {
        return alors;
    }

    /**
     * Getteur pour les instructions si la condition est fausse.
     * @return La ListeInstruction.
     */
    public ListeInstructions getSinon()
    {
        return sinon;
    }

    @Override
    public String toString()
    {
        return "si (" + this.cond.toString() + ") alors\n" + this.alors.toString() + "sinon\n" + this.sinon.toString() + "finsi\n";
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        string += this.cond.buildTree(level);
        string += this.alors.buildTree(level);
        string += this.sinon.buildTree(level);

        return string;
    }
}
