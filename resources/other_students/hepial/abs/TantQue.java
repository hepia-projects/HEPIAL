package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class TantQue extends Instructions
{
    private final Expressions expression;
    private final ListeInstructions corps;

    public TantQue(Expressions expressions, ListeInstructions corps, int line)
    {
        super(line);
        this.expression = expressions;
        this.corps = corps;
    }

    public Expressions getExpression()
    {
        return expression;
    }

    public ListeInstructions getCorps()
    {
        return corps;
    }

    @Override
    public String toString()
    {
        return "tantque (" + this.expression + ") faire\n" + this.corps.toString() + "fintantque";
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;

        string += this.expression.buildTree(level);
        string += this.corps.buildTree(level);

        return string;
    }
}
