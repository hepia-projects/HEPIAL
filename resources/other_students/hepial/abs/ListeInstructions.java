package abs;

import visitors.Visiteur;

import java.util.ArrayList;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class ListeInstructions extends Instructions
{
    private final ArrayList<Instructions> listInstruction;

    /**
     * Constructeur par defaut
     *
     * @param line Ligne de l'instruction
     */
    public ListeInstructions(int line)
    {
        super(line);
        this.listInstruction = new ArrayList<>();
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    /**
     * Ajoute une Instruction dans la liste.
     * @param instructions L'instruction à ajouter.
     */
    public void addInstruction(Instructions instructions)
    {
        this.listInstruction.add(instructions);
    }

    /**
     * Verifie si il reste une instruction dans la liste.
     * @return True si il reste une instruction
     */
    public boolean asNext()
    {
        return this.listInstruction.isEmpty();
    }

    /**
     * Enlève la première instruction de la liste d'instruction et la retourne.
     * @return La première insruction de la liste.
     */
    public Instructions getNextInstructions()
    {
        return this.listInstruction.remove(0);
    }

    /**
     * Getteur pour toute les instructions
     * @return Liste d'instruction
     */
    public ArrayList<Instructions> getAllInstructions()
    {
        return listInstruction;
    }

    /**
     * Getteur pour la dernière instruction de la liste.
     * @return La dernière instruction de la liste
     */
    public Instructions getLastInstruction()
    {
        return this.listInstruction.remove(this.listInstruction.size() - 1);
    }

    @Override
    public String toString()
    {
        String string = "";

        for (Instructions instr : this.listInstruction)
        {
            string += instr.toString() + "\n";
        }

        return string;
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        for (Instructions inst : this.listInstruction)
        {
            string += inst.buildTree(level);
        }

        return string;
    }
}
