package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Appel extends Expressions
{
    private final Identifiant id;
    private final Parametres parametres;
    private String classOfMethod;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Appel(Identifiant id, Parametres param, int line)
    {
        super(line);
        this.id = id;
        this.parametres = param;
        classOfMethod = "";
    }

    /**
     * Getteur pour l'Identifiant de la fonction à appeler.
     *
     * @return L'Identifiant à appeler.
     */
    public Identifiant getId()
    {
        return id;
    }

    /**
     * Getteur pour les paramètres de la fonction
     *
     * @return Les Paramètres de la fonction
     */
    public Parametres getParametres()
    {
        return parametres;
    }

    @Override
    public String toString()
    {
        return this.id.toString() + "(" + this.parametres.toString() + ")";
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        string += this.id.buildTree(level);
        string += this.parametres.buildTree(level);

        return string;
    }

    /**
     * Récupère la classe de la méthode.
     *
     * @return Nom de la classe.
     */
    public String getClassOfMethod()
    {
        return classOfMethod;
    }

    /**
     * Définit la classe de la méthode.
     *
     * @param classOfMethod Nom de la classe.
     */
    public void setClassOfMethod(String classOfMethod)
    {
        this.classOfMethod = classOfMethod;
    }
}
