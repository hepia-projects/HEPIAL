package abs;

import visitors.Visiteur;

import java.util.ArrayList;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Declaration extends Instructions
{
    private final ArrayList<Fonction> fonctionsList;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Declaration(int line)
    {
        super(line);
        fonctionsList = new ArrayList<>();
    }

    public void addFonction(Fonction f)
    {
        if (f != null)
            this.fonctionsList.add(f);
    }

    /**
     * Getteur pour les fonction déclarée.
     * @return Liste des Fonctions.
     */
    public ArrayList<Fonction> getAllFonctions()
    {
        return fonctionsList;
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        for (Fonction fct : this.fonctionsList)
        {
            string += fct.buildTree(level);
        }

        return string;
    }

    @Override
    public String toString()
    {
        String str = "decl:\n";

        for (Fonction fct : this.fonctionsList)
        {
            str += fct.toString();
        }

        return str + "enddecl\n";
    }

}
