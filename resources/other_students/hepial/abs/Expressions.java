package abs;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public abstract class Expressions extends ArbreAbstrait
{
    private int line;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    Expressions(int line)
    {
        super(line);
    }
}
