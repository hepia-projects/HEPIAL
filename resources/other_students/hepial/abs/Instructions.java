package abs;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public abstract class Instructions extends ArbreAbstrait
{
    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    Instructions(int line)
    {
        super(line);
    }
}
