package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Constante extends Identifiants
{
    /**
     * Constructeur par defaut.
     *
     * @param val  La valeur de la string.
     * @param line La  ligne de l'instruction.
     */
    public Constante(String val, int line)
    {
        super(val, line);
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }
}
