package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Fonction extends Instructions
{
    private final Identifiant id;
    private final Parametres param;
    private final ListeInstructions corps;
    private final RetourFonction retour;

    /**
     * Constructeur par défaut.
     *
     * @param line Ligne de l'instruction.
     */
    public Fonction(Identifiant id, Parametres param, ListeInstructions corps, RetourFonction retour, int line)
    {
        super(line);
        this.id = id;
        this.param = param;
        this.corps = corps;
        this.retour = retour;
    }

    /**
     * Getteur pour l'Identifiant de la fonction.
     *
     * @return L'Identifiant de la fonction.
     */
    public Identifiant getId()
    {
        return id;
    }

    /**
     * Getteur pour les Parametres de la fonction.
     *
     * @return Parametres de la fonction.
     */
    public Parametres getParam()
    {
        return param;
    }

    /**
     * Getteur pour les instructions de la fonction.
     *
     * @return ListeInstruction de la fonction.
     */
    public ListeInstructions getCorps()
    {
        return corps;
    }

    /**
     * Getteur pour le retour de la fonction.
     *
     * @return Retour de la fonction.
     */
    public RetourFonction getRetour()
    {
        return retour;
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        string += this.id.buildTree(level);
        if (this.param != null)
            string += this.param.buildTree(level);
        string += this.corps.buildTree(level);
        if (this.retour != null)
            string += this.retour.buildTree(level);
        return string;
    }

    @Override
    public String toString()
    {
        String str = this.id.toString() + "(";

        if (this.param != null)
            str += this.param.toString();
        str += ")\n" + this.corps.toString();

        if (this.retour != null)
            str += this.retour.toString();

        return str;
    }

    /**
     * Construit la signature de la fonction.
     *
     * @return String de la signature de la fonction.
     */
    public String buildSignature()
    {
        String sig = this.id.getValue() + "(";
        for (Expressions param : this.param.getAllParam())
        {
            sig += param.getType().methodMnemonic();
        }
        sig += ")" + this.retour.getType().methodMnemonic();

        return sig;
    }

}