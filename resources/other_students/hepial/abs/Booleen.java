package abs;

import visitors.Visiteur;

/**
 * @author David Wittwer
 * @version 0.0.1
 */
public class Booleen extends Expressions
{
    private Boolean aBoolean;

    /**
     * Constructeur par defaut.
     *
     * @param aBoolean La valeur à attribuer.
     * @param line     La ligne de l'instruction.
     */
    public Booleen(Boolean aBoolean, int line)
    {
        super(line);
        this.aBoolean = aBoolean;
    }

    /**
     * Récupère la valeur du booleen.
     *
     * @return La valeur du booleen.
     */
    public Boolean getBoolean()
    {
        return aBoolean;
    }

    /**
     * Définit la valeur du nombre.
     *
     * @param aBoolean La valeur à attribuer.
     */
    public void setBoolean(Boolean aBoolean)
    {
        this.aBoolean = aBoolean;
    }

    @Override
    public String toString()
    {
        return aBoolean.toString();
    }

    @Override
    public Object accepter(Visiteur v)
    {
        return v.visiter(this);
    }

    @Override
    public String buildTree(int level)
    {
        String string = super.buildTree(level);
        level++;
        String indent = "";

        for (int i = 0; i < level; i++)
        {
            indent += "|   ";
        }

        return string + indent + this.aBoolean + "\n";
    }
}
