
import java.util.HashMap;
import java.io.*;
import java.lang.String;
import java.util.ArrayList;
import java.util.Stack;

public class Association {
	private Symbole s;
	private Integer bloc;
		
	public Association(Symbole s1, Integer b){
		s = s1;
		bloc = b;
	}
	//getteurs
	public Integer getBloc(){
		return bloc;
	}
	public Symbole getSymbole(){
		return s;
	}
		
}