
import java.util.ArrayList;

public class TantQue extends Instructions {
	public Expression tq;
	public Instructions faire;
	
	public TantQue(Expression t, Instructions f){
		tq = t;
		faire = f;
	}
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}