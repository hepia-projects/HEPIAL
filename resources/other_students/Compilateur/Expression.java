public class Expression extends ArbreAbstrait {

	private String type;
	
	public String type(){
		return type;
	}
	public void setType(String s){
		type = s;
	}
	
	
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}