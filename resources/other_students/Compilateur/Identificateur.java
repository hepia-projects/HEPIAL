public class Identificateur extends Expression {
	String nom;
	
	public Identificateur(String n, String t){
		nom = n;
		setType(t);
	}
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
	
}