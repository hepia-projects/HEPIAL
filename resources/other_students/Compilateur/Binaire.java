public class Binaire extends Expression {
	Expression gauche;
	Expression droit;
	
	public Binaire(Expression g, Expression d){
		gauche = g;
		droit = d;
	}
	
	public Expression gauche(){
		return gauche;
	}
	public Expression droit(){
		return droit;
	}
	
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}