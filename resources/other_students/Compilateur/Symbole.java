
import java.util.HashMap;
import java.io.*;
import java.lang.String;
import java.util.ArrayList;
import java.util.Stack;

public class Symbole {
	private Integer ligne;
	private String type;
		
	public Symbole(){
		ligne = new Integer(0);
		type = new String();
	} 
	public Symbole(Integer i){
		ligne = new Integer(i);
		type = new String();
	} 
	public Symbole(String s){
		ligne = new Integer(0);
		type = new String(s);
	} 
	public Symbole(Integer i, String s){
		ligne = new Integer(i);
		type = new String(s);
	} 
	
	//getteurs
	public Integer getLigne(){
		return ligne;
	}
	public String getType(){
		return type;
	}
	
	//setteurs
	public void setLigne(Integer i){
		ligne = i;
	}
	public void setSymbole(String s){
		type = s;
	}
	
	//divers
	public String toString(){
		return new String(type+" à la ligne "+ligne);
	}
}