
import java.util.ArrayList;

public class Si extends Instructions {
	Expression condition;
	Instructions instructionSi;
	Instructions instructionSinon;	
	
	public Si(Expression c, Instructions l1, Instructions l2 ){
		condition = c;
		instructionSi = l1;
		instructionSinon = l2;
	}
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}