import java.util.ArrayList;


public class Bloc extends Instructions {
	ArrayList<Instructions> liste;
	
	public Bloc(){
		liste = new ArrayList(0);
	}
	
	public void add(Instructions i){
		liste.add(i);
	}
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}