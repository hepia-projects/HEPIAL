public class Addition extends Arithmetique {
	
	public Addition(Expression g, Expression d){
		super(g,d);
	}
	
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}

}