public class Ecrire extends Instructions {
	public String aEcrire;
	
	public Ecrire(String s){
		aEcrire = s;
	}
	
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}
