public class Relation extends Binaire {

	public Relation(Expression g, Expression d){
		super(g,d);
	}
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}