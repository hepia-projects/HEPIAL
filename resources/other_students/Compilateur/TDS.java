
import java.util.HashMap;
import java.io.*;
import java.lang.String;
import java.util.ArrayList;
import java.util.Stack;
import java.util.StringTokenizer;

public class TDS {
	
	/**************** champs privés ****************/
	private Stack pile;
	private HashMap dico;
	private int numeroBloc = -1;
	private static TDS instance = new TDS();
	
	/**************** geteur de l'unique objet TDS ****************/
	public static TDS getInstance(){
		return instance;	
	}
	
	/**************** constructeur ****************/
	private TDS(){
		pile = new Stack();
		dico = new HashMap();	
	}
	
	/**************** quand on entre dans un nouveau bloc ****************/
	public void entreeBloc(){
		numeroBloc++;
		pile.push(new Integer( numeroBloc ));
	}
	
	/**************** quand on sort d'un bloc ****************/
	public void sortieBloc(){
		numeroBloc--;
		pile.pop();
	}
	
	/**************** Ajout d'un identifier dans le dico ****************/
	public void ajouter (String nom, Symbole s){
		StringTokenizer st = new StringTokenizer(nom, "-");
		String temp = st.nextToken();
		System.out.println(temp);
		Stack p1 = (Stack)(dico.get(temp)); //récupérer le nom de l'entrée dans le dico
		if( p1 == null){ //si l'entrée n'existe pas
			//p1 sera une nouvelle pile qui comprend uniquement la nouvelle association que l'on fait entre
			//notre Symbole et le no de bloc
			p1 = new Stack();
			p1.push(new Association(s, numeroBloc));
			//ajout de la pile dans le dico
			dico.put(temp, p1);
		}else{
			//récupérer le somet de la pile sans le supprimer
			Association premier = (Association)(p1.peek());
			//si la dernière entrée est dans le même bloc
			if(premier.getBloc() == numeroBloc){
				//une erreure
				System.out.println("Identificateur deja existant !");
				//FlotErreurs.getInstance().add( s.getLigne(), ErreurSemantique.doubleDecl(nom, s.getLigne() ) );
				
			}else{
				p1.push(new Association(s, numeroBloc) );
			}
		}
	}
	
	/**************** trouver le symbole associé à un identificateur ****************/
	public Symbole identifier(String nom){
		Stack p1 = (Stack)(dico.get(nom));
		if(p1 == null){
			System.out.println("le nom: "+nom+" n'est pas dans la tds");
			return null;
		}
		int indicePile = pile.size()-1;
		int indiceP1 = p1.size()-1;
		boolean fin = false;
		Object s = null;
		int premListe, premPile;
		Association assoc;
		while( !fin && indicePile != -1 && indiceP1 != -1 ){
			assoc = (Association)( p1.get(indiceP1) );
			premListe = assoc.getBloc();
			premPile = ( (Integer)(pile.get(indicePile)) ).intValue();
			if(premListe == premPile){
				s = assoc.getSymbole();
				fin = true;
			}else if( premListe < premPile)
				indiceP1--;
			else
				indicePile--;
		}
		return (Symbole)s;
	}
}
