public class ArbreAbstrait {
	
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}