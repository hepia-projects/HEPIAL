public class AnalyseurSemantique implements Visiteur {
	
	public Object visiter(Addition a) throws Exception {
		a.gauche().accepter(this);
		a.droit().accepter(this);
		
		Integer n = new Integer(5);
		
		String typeD = a.droit().type();
		String typeG = a.gauche().type();
		
		Object v1 = a.gauche().accepter(Evaluateur.getInstance());
		Object v2 = a.droit().accepter(Evaluateur.getInstance());
		
		//System.out.println("valeur de v1: "+v1+" valeur de v2: "+v2);
		
		if( v1 != null && v2 != null ){
			if( v1.getClass() != n.getClass() || v2.getClass() != n.getClass()){
				System.out.println("Erreur 1 visiter addition");
				throw(new Exception());
			}
		}else if(v1 != null && v2 == null){
			if( v1.getClass() != n.getClass() || !typeD.equals("entier") ){
				System.out.println("Erreur 2 visiter addition");
				throw(new Exception());
			}
		}else if(v1 == null && v2 != null){
			if( v2.getClass() != n.getClass() || !typeG.equals("entier") ){
				System.out.println("Erreur 3 visiter addition");
				throw(new Exception());
			}
		}else if(v1 == null && v2 == null){
			if( !typeG.equals("entier") || !typeD.equals("entier") ){
				System.out.println("Erreur 4 visiter addition");
				throw(new Exception());
			}
		}
		
		a.setType("entier");
		
		return null;
	}
	
	public Object visiter(Affectation a) throws Exception {
		a.destination().accepter(this);
		a.source().accepter(this);
		String typeDest = a.destination().type();
		
		//System.out.println("type de source: "+a.source().type());
		Object v = a.source().accepter(Evaluateur.getInstance());
		
		if( v!= null){
			if( !a.source().type().equals("booleen") ){
				a.setSource( new Nombre( (Integer)v ) );
				a.source().setType("entier");
			}else{
				System.out.println("source: "+a.source().type());
				a.setSource( new Booleen( (Boolean)v ) );
				a.source().setType("booleen");
			}
		}
		//a.source().accepter(this);
		String typeSource = a.source().type();
		//System.out.println("type source: "+typeSource+" type destination: "+typeDest);
		if( !typeSource.equals(typeDest) ){
			System.out.println("Erreur visiter affectation");
				throw(new Exception());
		}else
			a.setType(typeDest);
		return null;
	}
	
	public Object visiter(ArbreAbstrait a){return null;}
	
	public Object visiter(Arithmetique a){return null;}
	
	public Object visiter(Binaire b){return null;}
	
	public Object visiter(Bloc b){
		for(int i = 0; i < b.liste.size(); i++)
			b.liste.get(i).accepter(this);
		
		return null;
	}
	public Object visiter(Ecrire e){return null;}
	
	public Object visiter(Egal e) throws Exception {
		e.gauche().accepter(this);
		e.droit().accepter(this);
		
		Boolean b = true;
		Integer n = 5;
		
		String typeD = e.droit().type();
		String typeG = e.gauche().type();
		Object v1 = e.gauche().accepter(Evaluateur.getInstance());
		Object v2 = e.droit().accepter(Evaluateur.getInstance());
		
		if( v1 != null && v2 != null ){
			if(v1.getClass() != v2.getClass()){
				System.out.println("Erreur 1 visiter egal");
				throw(new Exception());
			}
		}else if(v1 != null && v2 == null){
			if(typeD.equals("booleen")){
				if(v1.getClass() != b.getClass()){
					System.out.println("Erreur 2a visiter egal");
					throw(new Exception());
				}
			}else{
				if(v1.getClass() != n.getClass()){
					System.out.println("Erreur 2b visiter egal");
					throw(new Exception());
				}
			}
		}else if(v1 == null && v2 != null){
			if(typeG.equals("booleen")){
				if(v2.getClass() != b.getClass()){
					System.out.println("Erreur 3a visiter egal");
					throw(new Exception());
				}
			}else{
				if(v2.getClass() != n.getClass()){
					System.out.println("Erreur 3b visiter egal");
					throw(new Exception());
				}
			}
		}else if(v1 == null && v2 == null){
			if( !typeG.equals(typeD) ){
				System.out.println("Erreur 4 visiter egal");
				throw(new Exception());
			}
		}
		
		e.setType("booleen");
		
		return null;	
	}
	
	public Object visiter(Expression e){return null;}
	
	public Object visiter(Identificateur i){
		return null;
	}
	
	public Object visiter(Instructions i){return null;}
	public Object visiter(Lire l){return null;}
	public Object visiter(Nombre n){
		n.setType("entier");
		return null;
	}
	
	public Object visiter(PlusGrandQue p) throws Exception {
		p.gauche().accepter(this);
		p.droit().accepter(this);
		
		Integer b = new Integer(5);
		
		String typeD = p.droit().type();
		String typeG = p.gauche().type();
		
		Object v1 = p.gauche().accepter(Evaluateur.getInstance());
		Object v2 = p.droit().accepter(Evaluateur.getInstance());
		
		//System.out.println("type de gauche: "+typeG+" type de droite: "+typeD);
		//System.out.println("type de gauche: "+v1+" type de droite: "+v2);

		if( v1 != null && v2 != null ){
			if( v1.getClass() != b.getClass() || v2.getClass() != b.getClass()){
				System.out.println("Erreur 1 visiter pgq");
				throw(new Exception());
			}
		}else if(v1 != null && v2 == null){
			if( v1.getClass() != b.getClass() || !typeD.equals("entier") ){
				System.out.println("Erreur 2 visiter pgq");
				throw(new Exception());
			}
		}else if(v1 == null && v2 != null){
			if( v2.getClass() != b.getClass() || !typeG.equals("entier") ){
				System.out.println("Erreur 3 visiter pgq");
				throw(new Exception());
			}
		}else if(v1 == null && v2 == null){
			if( !typeG.equals("entier") || !typeD.equals("entier") ){
				System.out.println("Erreur 4 visiter pgq");
				throw(new Exception());
			}
		}
		
		p.setType("booleen");
		
		return null;	
	}
		
	public Object visiter(Pour p) throws Exception {
		p.ident.accepter(this);
		p.de.accepter(this);
		p.a.accepter(this);
		
		String typeDe = p.de.type();
		String typeA = p.a.type();
		
		Integer n = new Integer(5);
		
		Object v1 = p.de.accepter(Evaluateur.getInstance());
		Object v2 = p.a.accepter(Evaluateur.getInstance());
		//System.out.println("classe v1: "+v1.getClass()+" classe v2: "+v2.getClass());
		
		if( !p.ident.type().equals("entier") ){
			System.out.println("Erreur 0 visiter pour");
			throw(new Exception());
		}
		if( v1 != null && v2 != null ){
			if( v1.getClass() != n.getClass() || v2.getClass() != n.getClass()){
				System.out.println("Erreur 1 visiter pour");
				throw(new Exception());
			}
		}else if(v1 != null && v2 == null){
			if( v1.getClass() != n.getClass() || !typeA.equals("entier") ){
				System.out.println("Erreur 2 visiter pour");
				throw(new Exception());
			}
		}else if(v1 == null && v2 != null){
			if( v2.getClass() != n.getClass() || !typeDe.equals("entier") ){
				System.out.println("Erreur 3 visiter pour");
				throw(new Exception());
			}
		}else if(v1 == null && v2 == null){
			if( !typeDe.equals("entier") || !typeA.equals("entier") ){
				System.out.println("Erreur 4 visiter pour");
				throw(new Exception());
			}
		}
		
		return null;
	}
	
	public Object visiter(Produit p) throws Exception {
		p.gauche().accepter(this);
		p.droit().accepter(this);
		
		Integer n = new Integer(5);
		
		String typeD = p.droit().type();
		String typeG = p.gauche().type();
		
		Object v1 = p.gauche().accepter(Evaluateur.getInstance());
		Object v2 = p.droit().accepter(Evaluateur.getInstance());
		
		if( v1 != null && v2 != null ){
			if( v1.getClass() != n.getClass() || v2.getClass() != n.getClass()){
				System.out.println("Erreur 1 visiter produit");
				throw(new Exception());
			}
		}else if(v1 != null && v2 == null){
			if( v1.getClass() != n.getClass() || !typeD.equals("entier") ){
				System.out.println("Erreur 2 visiter produit");
				throw(new Exception());
			}
		}else if(v1 == null && v2 != null){
			if( v2.getClass() != n.getClass() || !typeG.equals("entier") ){
				System.out.println("Erreur 3 visiter produit");
				throw(new Exception());
			}
		}else if(v1 == null && v2 == null){
			if( !typeG.equals("entier") || !typeD.equals("entier") ){
				System.out.println("Erreur 4 visiter produit");
				throw(new Exception());
			}
		}
		
		p.setType("entier");
		
		return null;
	}
	
	public Object visiter(Relation r){ return null; }	
	
	public Object visiter(Si s) throws Exception {
		s.condition.accepter(this);
		
		Boolean b = true;
		
		Object v = s.condition.accepter(Evaluateur.getInstance());
		
		String typeSi = s.condition.type();
		
		if(v == null){
			if(!typeSi.equals("booleen")){
				System.out.println("Erreur 1 visiter si");
				throw(new Exception());
			}
		}else{
			if(v.getClass() != b.getClass()){
				System.out.println("Erreur 2 visiter si");
				throw(new Exception());
			}
		}
		
		return null;
	}
	
	public Object visiter(TantQue t) throws Exception{
		t.tq.accepter(this);
		
		Boolean b = true;
		
		Object v = t.tq.accepter(Evaluateur.getInstance());
		
		String typeTq = t.tq.type();
		
		if(v == null){
			if(!typeTq.equals("booleen")){
				System.out.println("Erreur 1 visiter tantque");
				throw(new Exception());
			}
		}else{
			if(v.getClass() != b.getClass()){
				System.out.println("Erreur 2 visiter tantque");
				throw(new Exception());
			}
		}
		
		return null;
	}
	
	public Object visiter(Booleen b){
		b.setType("booleen");
		return null;
	}
	
}

