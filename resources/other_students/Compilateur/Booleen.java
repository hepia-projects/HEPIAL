public class Booleen extends Expression {
	boolean valeur;
	
	public Booleen(boolean n){
		valeur = n;
	}
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}