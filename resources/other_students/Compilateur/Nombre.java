public class Nombre extends Expression {
	int valeur;
	
	public Nombre(int n){
		valeur = n;
	}
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}