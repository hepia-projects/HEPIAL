public class Affectation extends Instructions{
	private Expression destination;
	private Expression source;
	private String type;
	
	public Affectation(Expression d, Expression s){
		destination = d;
		source = s;
	}
	public Expression destination(){
		return destination;
	}
	public Expression source(){
		return source;
	}
	public void setDestination(Expression e){
		destination = e;
	}
	public void setSource(Expression e){
		source = e;
	}
	public void setType(String s){
		type = s;
	}
	
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}
