import java_cup.runtime.*;
%%

/* options */
%class Lexer
%unicode
%line
%column
%cup

%{

public void yyerror() {
       System.out.println("error line " +yyline + " column " +yycolumn
       + " " +yytext());
}

%}

/* models */
ident = [a-zA-Z][a-zA-Z0-9]*
constanteEnt = [0-9]+
constanteCh = \"([^\"]|\"\")*\"
comment = \/\/[^\n]
opebin = \+|\-|\*|\/|==|<>|<|>|<=|>=|et|ou
opeun = \~|non
virgule = ,
pointvirgule = ;
parentouvre = \(
parentferme = \)
crochetouvre = \[
crochetferme = \]
affect = =


%%

/* rules */
programme       { return new Symbol(sym.programme); }
debutprg        { return new Symbol(sym.debutprg); }
finprg          { return new Symbol(sym.finprg); }
constante       { return new Symbol(sym.constante); }
entier          { return new Symbol(sym.entier); }
booleen         { return new Symbol(sym.booleen); }
lire            { return new Symbol(sym.lire); }
ecrire          { return new Symbol(sym.ecrire); }
retourne        { return new Symbol(sym.retourne); }
si              { return new Symbol(sym.si); }
alors           { return new Symbol(sym.alors); }
sinon           { return new Symbol(sym.sinon); }
finsi           { return new Symbol(sym.finsi); }
faire           { return new Symbol(sym.faire); }
pour            { return new Symbol(sym.pour); }
allantde        { return new Symbol(sym.allantde); }
a               { return new Symbol(sym.a); }
finpour         { return new Symbol(sym.finpour); }
tantque         { return new Symbol(sym.tantque); }
fintantque      { return new Symbol(sym.fintantque); }
vrai            { return new Symbol(sym.vrai); }
faux            { return new Symbol(sym.faux); }
{opebin}        { return new Symbol(sym.OPEBIN, new String(yytext())); }
{opeun}         { return new Symbol(sym.OPEUN, new String(yytext())); }
{virgule}       { return new Symbol(sym.virgule); }
{pointvirgule}  { return new Symbol(sym.pointvirgule); }
{parentouvre}   { return new Symbol(sym.parentouvre); }
{parentferme}   { return new Symbol(sym.parentferme); }
{constanteEnt}  { return new Symbol(sym.constanteEnt, new String(yytext())); }
{constanteCh}   { return new Symbol(sym.constanteChaine, new String(yytext())); }
{ident}     	{ return new Symbol(sym.ident, new String(yytext()+"-"+yyline)); }
{affect}    	{ return new Symbol(sym.affect); }
"$"             { return new Symbol(sym.end); }

/* Caracteres non pris en compte */
[\ |\t|\n|\r|\r\n] { }





