class Evaluateur implements Visiteur {
	
	private static Evaluateur eval = new Evaluateur();
	
	
	private Evaluateur(){ }
	
	public static Evaluateur getInstance(){
		return eval;
	}
	
	public Object visiter(Addition a)throws Exception{
		Object valG = a.gauche().accepter(this);
		Object valD = a.droit().accepter(this);
		if (valG == null || valD == null){
			return null;
		}
		
		int g = ((Integer)valG).intValue();
		int d = ((Integer)valD).intValue();
		return new Integer(g+d);
		
	}
	
	public Object visiter(Affectation a)throws Exception{
		
		Object dest = a.destination().accepter(this);
		Object src = a.source().accepter(this);
		
		if( (dest == null) && (src != null) )
			return src;
		return null;
		
	}
	
	public Object visiter(ArbreAbstrait a){return null;}
	public Object visiter(Arithmetique a){return null;}
	public Object visiter(Binaire b){return null;}
	public Object visiter(Bloc b){return null;}
	public Object visiter(Ecrire e){return null;}
	
	public Object visiter(Egal e)throws Exception{
		Object valG = e.gauche().accepter(this);
		Object valD = e.droit().accepter(this);
		if (valG == null || valD == null){
			return null;
		}
		try{
			int g = ((Integer)valG).intValue();
			int d = ((Integer)valD).intValue();
			return g==d;
		}catch(Exception ex){
			boolean g = ((Boolean)valG).booleanValue();
			boolean d = ((Boolean)valD).booleanValue();
			return g==d;
		}
	}
	
	public Object visiter(Expression e){return null;}
	
	public Object visiter(Identificateur i){
		return null;
	}
	
	public Object visiter(Instructions i){return null;}
	public Object visiter(Lire l){return null;}
	
	public Object visiter(Nombre n){
		return new Integer(n.valeur);
	}
	
	public Object visiter(PlusGrandQue p)throws Exception{
		Object valG = p.gauche().accepter(this);
		Object valD = p.droit().accepter(this);
		if (valG == null || valD == null){
			return null;
		}
		
		int g = ((Integer)valG).intValue();
		int d = ((Integer)valD).intValue();
		return g>d;
	}
	
	public Object visiter(Pour p)throws Exception{return null;}
	
	public Object visiter(Produit p)throws Exception{
		Object valG = p.gauche().accepter(this);
		Object valD = p.droit().accepter(this);
		if (valG == null || valD == null){
			return null;
		}
		
		int g = ((Integer)valG).intValue();
		int d = ((Integer)valD).intValue();
		return new Integer(g*d);
	}
	
	public Object visiter(Relation r){return null;}
	public Object visiter(Si s)throws Exception{return null;}
	public Object visiter(TantQue t)throws Exception{return null;}
	public Object visiter(Booleen b){return new Boolean(b.valeur);}
	
}