interface Visiteur {
	public Object visiter(Addition a)throws Exception;
	public Object visiter(Affectation a)throws Exception;
	public Object visiter(ArbreAbstrait a);
	public Object visiter(Arithmetique a);
	public Object visiter(Binaire b);
	public Object visiter(Bloc b);
	public Object visiter(Ecrire e);
	public Object visiter(Egal e)throws Exception;
	public Object visiter(Expression e);
	public Object visiter(Identificateur i);
	public Object visiter(Instructions i);
	public Object visiter(Lire l);
	public Object visiter(Nombre n);
	public Object visiter(PlusGrandQue p)throws Exception;
	public Object visiter(Pour p)throws Exception;
	public Object visiter(Produit p)throws Exception;
	public Object visiter(Relation r);
	public Object visiter(Si s)throws Exception;
	public Object visiter(TantQue t)throws Exception;
	public Object visiter(Booleen b);
}