public class Arithmetique extends Binaire {

	public Arithmetique(Expression g, Expression d){
		super(g,d);
	}
	public Object accepter(Visiteur v){
		return v.visiter(this);
	}
}