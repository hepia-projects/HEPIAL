
//----------------------------------------------------
// The following code was generated by CUP v0.11b 20140808 (SVN rev 54)
//----------------------------------------------------

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int debutprg = 3;
  public static final int bool = 9;
  public static final int fintantque = 52;
  public static final int different = 15;
  public static final int allantde = 47;
  public static final int finpour = 50;
  public static final int plus = 11;
  public static final int faux = 35;
  public static final int plusgrand = 20;
  public static final int pointvirgule = 38;
  public static final int doubleegal = 16;
  public static final int entier = 8;
  public static final int parentouvrante = 27;
  public static final int si = 42;
  public static final int ecrire = 24;
  public static final int finfonc = 6;
  public static final int programme = 2;
  public static final int crochetfermant = 30;
  public static final int debutfonc = 5;
  public static final int moins = 12;
  public static final int faire = 49;
  public static final int vrai = 34;
  public static final int EOF = 0;
  public static final int ident = 21;
  public static final int sinon = 44;
  public static final int alors = 43;
  public static final int retourne = 7;
  public static final int nombre = 22;
  public static final int error = 1;
  public static final int tantque = 51;
  public static final int ou = 26;
  public static final int affectation = 10;
  public static final int constanteent = 32;
  public static final int comment = 36;
  public static final int crochetouvrant = 29;
  public static final int pluspetit = 18;
  public static final int virgule = 37;
  public static final int mult = 13;
  public static final int pour = 46;
  public static final int pluspetitegal = 17;
  public static final int finprg = 4;
  public static final int finsi = 45;
  public static final int et = 25;
  public static final int plusgrandegal = 19;
  public static final int parentfermante = 28;
  public static final int constantechaine = 33;
  public static final int constante = 31;
  public static final int lire = 23;
  public static final int div = 14;
  public static final int no = 41;
  public static final int moinsunaire = 40;
  public static final int deuxpoints = 39;
  public static final int a = 48;
  public static final String[] terminalNames = new String[] {
  "EOF",
  "error",
  "programme",
  "debutprg",
  "finprg",
  "debutfonc",
  "finfonc",
  "retourne",
  "entier",
  "bool",
  "affectation",
  "plus",
  "moins",
  "mult",
  "div",
  "different",
  "doubleegal",
  "pluspetitegal",
  "pluspetit",
  "plusgrandegal",
  "plusgrand",
  "ident",
  "nombre",
  "lire",
  "ecrire",
  "et",
  "ou",
  "parentouvrante",
  "parentfermante",
  "crochetouvrant",
  "crochetfermant",
  "constante",
  "constanteent",
  "constantechaine",
  "vrai",
  "faux",
  "comment",
  "virgule",
  "pointvirgule",
  "deuxpoints",
  "moinsunaire",
  "no",
  "si",
  "alors",
  "sinon",
  "finsi",
  "pour",
  "allantde",
  "a",
  "faire",
  "finpour",
  "tantque",
  "fintantque"
  };
}

